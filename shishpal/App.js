import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import Boot from './app/boot';

export default class App extends React.Component {
  render() {
    return (
      <Boot />
    );
  }
}
