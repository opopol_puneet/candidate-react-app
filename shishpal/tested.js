static onEnter = () => {
  AsyncStorage.getItem('updateList').then((resp) => {
    if(resp){
      resp = JSON.parse(resp);
      if(resp.polled === 1){
        Actions.refresh({
          rowID:resp.rowID
        });
      }
    }
  })
}

componentWillReceiveProps (nextProps) {
  if (this.props.rowID !== nextProps.rowID) {
    AsyncStorage.getItem('updateList').then((resp){
      if(resp){
        var newArray = this.state.data.slice();
        var rowID = nextProps.rowID;
        newArray[rowID] = {
          key: newArray[rowID].rowID,
          rowID: newArray[rowID].rowID,
          id: newArray[rowID].id,
          village: newArray[rowID].village,
          booth: newArray[rowID].booth,
          voter_id: newArray[rowID].voter_id,
          name: newArray[rowID].name,
          patron: newArray[rowID].patron,
          ward_no: newArray[rowID].ward_no,
          age: newArray[rowID].age,
          caste: newArray[rowID].caste,
          gender: newArray[rowID].gender,
          occupation: newArray[rowID].occupation,
          mobile_no: newArray[rowID].mobile_no,
          relation: newArray[rowID].relation,
          polled: 1,
        };
        this.setState({
            dataSource: this.state.dataSource.cloneWithRows(newArray)
        });
        AsyncStorage.removeItem('updateList');
      }
    })
  }
}
