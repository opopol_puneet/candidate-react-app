import React from 'react';
import { StyleSheet, Text, View, Alert, ToastAndroid, AsyncStorage } from 'react-native';

import Main from '../app/screens/main';
import Video from '../app/screens/video';
import PlayVideo from '../app/screens/playVideo';
import News from '../app/screens/news/news';
import SingleNews from '../app/screens/news/singleNews';
import Profile from '../app/screens/profile';
import Events from '../app/screens/events/events.js';
import SingleEvent from '../app/screens/events/single-event.js';
import WriteMe from '../app/screens/writeMe.js';
import Interview from '../app/screens/interview.js';
import LiveResult from '../app/screens/liveresult.js';
import Team from '../app/screens/team.js';
import EditProfile from '../app/screens/editprofile.js';
import Notification from '../app/screens/notification.js';
import Login from '../app/screens/login.js';

import AgentHome from '../app/screens/agent/home.js';
import AgentProfile from '../app/screens/agent/profile.js';
import AgentNotification from '../app/screens/agent/notification.js';
import AgentDataList from '../app/screens/agent/list.js';
import SingleItem from '../app/screens/agent/singleItem.js';
import Manage from '../app/screens/agent/manage.js';

import EditVoter  from '../app/screens/agent/edit/edit-voter.js';
import EditSocial  from '../app/screens/agent/edit/edit-social.js';
import EditEffective  from '../app/screens/agent/edit/edit-effective.js';
import EditPerference  from '../app/screens/agent/edit/edit-preference.js';
import EditMapping  from '../app/screens/agent/edit/edit-mapping.js';

import myConstants from '../app/constant/constant';

import {Scene, Router, Actions, ActionConst, Reducer} from 'react-native-router-flux';

import Icon from 'react-native-vector-icons/FontAwesome';

const TabIcon = ({ title, focused }) => {
  switch (title) {
    case 'Home':
      return (
        <Icon name="home" size={20} color={focused ? myConstants.PURPLE : myConstants.GRAY}  />
      )
    case 'Profile':
      return (
        <Icon name="user" size={20} color={focused ? myConstants.PURPLE : myConstants.GRAY}  />
      )
    case 'Notification':
      return (
        <Icon name="bell" size={20} color={focused ? myConstants.PURPLE : myConstants.GRAY}  />
      )
    case 'List':
      return (
        <Icon name="list-ul" size={20} color={focused ? myConstants.PURPLE : myConstants.GRAY}  />
      )
    case 'Logout':
      return (
        <Icon name="sign-out" size={20} color={focused ? myConstants.PURPLE : myConstants.GRAY}  />
      )
  }
}

const reducerCreate = params => {
  const defaultReducer = new Reducer(params);
  return (state, action) => {
    console.warn('ACTION:', action, Actions.currentScene);
    if (action.type === 'Navigation/BACK') { console.warn(TAG, 'Back Pressed');
    if (Actions.currentScene === 'home') { BackHandler.exitApp(); } }
    if (action.type === 'Navigation/INIT') { Actions.auth(); }
    return defaultReducer(state, action);
  };
};



export default class Boot extends React.Component {

  routerBackAndroidHandler = () => {
    console.warn(Actions.currentScene)
    if(Actions.currentScene=='_agentprofile' || Actions.currentScene=='_agentdatalist' || Actions.currentScene=='_agentnotification')
    {
      Actions.agenthome();
      return true;
    }
    else if(Actions.currentScene==='_agenthome')
    {
      Actions.home();
      return true;
    }
    else {
      return true;
    }
    return true;
  }

  render() {
    return (
      <Router navigationBarStyle={{backgroundColor: myConstants.PURPLE, zIndex:0, elevation: 0, shadowOpacity: 0}} navBarButtonColor={myConstants.WHITE} titleStyle={{fontFamily:myConstants.FONTFAMILY,fontSize:15, fontWeight:'normal', color:myConstants.WHITE}}>
        <Scene key="root" hideNavBar={true} initial>
          <Scene key="home" component={Main}  hideNavBar={true}  title="Home" />
          <Scene key="news" component={News}  hideNavBar={true} title="News" />
          <Scene key="singlenews" component={SingleNews} hideNavBar={true} title="News" />
          <Scene key="profile"  component={Profile} hideNavBar={true} title="Profile" />
          <Scene key="events" component={Events}  title="Events" hideNavBar={true} />
          <Scene key="singleevent" component={SingleEvent} title="Event" hideNavBar={true} />
          <Scene key="writeme" component={WriteMe} hideNavBar={true}  title="Suggestion" />
          <Scene key="interview" component={Interview}  hideNavBar={true}  title="Interview" />
          <Scene key="liveresult" component={LiveResult} hideNavBar={true} title="MLA Results" />
          <Scene key="team" component={Team} hideNavBar={true} title="Team Join" />
          <Scene key="editprofile" component={EditProfile} title="Edit Profile" />
          <Scene key="notification" component={Notification} hideNavBar={true}  title="Notification" />
          <Scene key="login" component={Login}   title="Login" />

          <Scene key="singleitem" title="Voter View" component={SingleItem} hideNavBar={true} />
          <Scene key="manage" hideNavBar={true} title="Manage Voter" component={Manage} />

          <Scene key="tabs" tabs swipeEnabled={false}  tabBarStyle={{backgroundColor:myConstants.CONTAINER}} tabBarPosition="bottom" inactiveTintColor={myConstants.GRAY} activeTintColor={myConstants.PURPLE} labelStyle={{fontSize:11,marginBottom:4, marginTop:-7, fontFamily:myConstants.FONTFAMILY}}>
            <Scene key="agenthome" title="Home" component={AgentHome} icon={TabIcon} hideNavBar={true} />
            <Scene key="agentprofile" title="Profile" component={AgentProfile} icon={TabIcon} />
            <Scene key="agentdatalist" title="List" component={AgentDataList} hideNavBar={true} icon={TabIcon} />
            <Scene key="agentnotification" title="Notification" component={AgentNotification}  icon={TabIcon} />
            <Scene key="logout" title="Logout" component={AgentHome} icon={TabIcon} tabBarOnPress={this.logout.bind(this)} />
          </Scene>

          <Scene key="edit">
            <Scene key="editvoter" hideNavBar={true} component={EditVoter} />
            <Scene key="editsocial" hideNavBar={true} component={EditSocial} />
            <Scene key="editeffective" hideNavBar={true} component={EditEffective} />
            <Scene key="editperference" hideNavBar={true} component={EditPerference} />
            <Scene key="editmapping" hideNavBar={true} component={EditMapping} />
          </Scene>

        </Scene>
      </Router>


    );
  }

  logout(){
    Alert.alert(
      'Confirmation required',
      'Do you really want to logout?',
      [
         {text: 'OK', onPress: () => {
          let keys = ['softUser', 'token', 'role', 'authId'];
          AsyncStorage.multiRemove(keys, (data) => {
            Actions.home();
          })
          ToastAndroid.show('Successfully Logged Out', ToastAndroid.SHORT)
         }},
         {text: 'Cancel', onPress:() => {ToastAndroid.show('Logout Cancelled', ToastAndroid.SHORT)}}
      ]
    )
  }
}
