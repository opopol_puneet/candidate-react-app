import React from 'react';
import { StatusBar } from 'react-native';
import myConstants from '../constant/constant';
export default class TopStatusBar extends React.Component {
  render() {
    return (
      <StatusBar hidden={false} backgroundColor={myConstants.PURPLE} barStyle="light-content" />
    );
  }
}
