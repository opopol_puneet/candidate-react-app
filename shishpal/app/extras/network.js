import React from 'react';
import { StatusBar, NetInfo, View, Text } from 'react-native';
import myConstants from '../constant/constant';
import TopStatusBar from './statusBar';
var styles = require('../styles/mainStyle');
export default class Network extends React.Component {

  render() {
    return (
      <View style={styles.mainContainer}>
        <TopStatusBar/>
        <View style={{alignItems:'center', justifyContent:'center', marginTop:20}}>
          <Text style={{fontSize:18, fontFamily:myConstants.FONTFAMILY, color:myConstants.BLACK}}>No Internet Connection</Text>
        </View>
      </View>
    );
  }
}
