'use strict';

var React = require('react-native');

var {
  StyleSheet,
} = React;

import myConstants from '../constant/constant';

module.exports = StyleSheet.create({

  voterViewContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  listHeader:{
    backgroundColor:myConstants.PURPLE,
    alignItems:'center',
    justifyContent:'center',
    flexDirection:'row',
    height:56
  },
  listHeaderTitle:{
    flex:7,
    justifyContent:'flex-start',
    padding:15,
  },
  listHeaderTitleText:{
    color:myConstants.WHITE,
    fontSize:18,
    fontFamily:myConstants.FONTFAMILY
  },
  listHeaderButton:{
    padding:15,
    flex:1,
    flexDirection:'row',
    justifyContent:'flex-start',
    alignItems:'center',
  },
  listHeaderButtonIcon:{
    color:myConstants.WHITE,
    fontSize:17,
    alignSelf:'center',
  },
  voterViewRow:{
    flex:1,
    flexDirection:'row',
    borderBottomWidth:1,
    height:42,
    borderBottomColor:'#A9A9A9',
    backgroundColor:'#DCDCDC',
  },
  voterViewColumn:{
    flex:1,
    justifyContent:'center',
    paddingLeft:25
  },
  voterViewColumnText:{
    fontFamily:'WhitneyMedium',
    fontSize:12,
  },
  voterViewButtonRow:{
    flex:1,
    flexDirection:'row',
  },
  voterViewButton:{
    flex:1,
    backgroundColor:myConstants.PURPLE,
    borderRadius:5,
    justifyContent:'center',
    alignItems:'center',
    paddingTop:10,
    paddingBottom:13
  },
  voterViewButtonText:{
    fontFamily:'WhitneyMedium',
    fontSize:20,
    color:'white',
  },
  btn:{
    marginTop:20
  }
});
