'use strict';

var React = require('react-native');

var {
  StyleSheet,
} = React;

import myConstants from '../constant/constant';

module.exports = StyleSheet.create({

  mainContainer: {
    flex: 1,
    backgroundColor: myConstants.WHITE,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection:'column'
  },
  buttons:{
    borderWidth:2,
    borderColor:myConstants.FACEBOOK,
    width:200,
    height:50,
    justifyContent:'center',
    alignItems:'center',
    flexDirection:'row',
    marginTop:25
  },
  buttonsIcon:{
    color:myConstants.FACEBOOK,
    fontSize:myConstants.FONTSIZE16,
    paddingRight:10,
  },
  buttonsText:{
    color:myConstants.FACEBOOK,
    fontSize:myConstants.FONTSIZE16,
    fontFamily:myConstants.FONTFAMILY,
  },
  statusBar:{
    height:24,
    width:375,
    backgroundColor:myConstants.PURPLE
  },
  headerButtonsContainer:{
    flex:1,
    flexDirection:'row',
    backgroundColor:myConstants.PURPLE
  },
  headerButtonsRow:{
    flex:1,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'flex-end'
  },
  headerButtons:{
    paddingRight:15
  },
  headerButtonsIcon:{
    color:myConstants.WHITE,
    fontSize:myConstants.FONTSIZE16
  },
  homeHeaderButton:{
    flex:1,
    flexDirection:'row',
    justifyContent:'flex-end',
    paddingRight:15,
    alignItems:'center',

  },
  homeHeaderButtonIcon:{
    color:myConstants.WHITE,
    fontSize:18,
    alignSelf:'center',
    borderRadius:50,

  },
  profileMain:{
    flex:6,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection:'column',
  },
  imageBoxView:{
    zIndex:0,
    top:0,
    position:'absolute',
  },
  imageBox:{
    height:215,
    width:360,
    zIndex:1,
    opacity:0.8,

  },

  imageBoxContainer:{
    position: 'absolute',
    left: 0,
    top: 0,
    zIndex:0,
    flex:1,
    flexDirection:'column'
  },

  profileImageBoxContainer:{
    justifyContent:'center',
    alignItems:'center',
    flex:1,
    backgroundColor:'transparent'
  },
  profileImageBox:{
    height:100,
    width:100,
    borderRadius:50,
    backgroundColor:'white'
  },
  profileImageBoxImage:{
    height:96,
    width:96,
    top:2,
    left:2,
    borderRadius:50
  },
  profileImageBoxPartyImage:{
    position:'absolute',
    top:60,
    left:60,
    borderRadius:50,
    backgroundColor:'white'
  },
  profileImageBoxPartyImageImage:{
    width:37,
    height:37,
    borderRadius:50,
  },
  profileBoxContent:{
    alignItems:'center'
  },
  profileBoxCandidateName:{
    paddingTop:5,
    fontSize:14,
    fontWeight:'bold',
    fontFamily:'Kreon'
  },
  profileBoxAssemblyName:{
    fontStyle:'italic',
    fontFamily:'Kreon'
  },
  profileNavs:{
    flex:8,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    padding:8
  },
  variantsBoxes:{
    flex:1,
    flexDirection:'column',
  },
  box1:{
    backgroundColor:myConstants.COLOR1
  },
  box2:{
    backgroundColor:myConstants.COLOR2
  },
  box3:{
    backgroundColor:myConstants.COLOR3
  },
  box4:{
    backgroundColor:myConstants.COLOR4
  },
  box5:{
    backgroundColor:myConstants.COLOR5
  },
  box6:{
    backgroundColor:myConstants.COLOR6
  },
  box7:{
    backgroundColor:myConstants.COLOR7
  },
  box8:{
    backgroundColor:myConstants.COLOR8
  },
  box9:{
    backgroundColor:myConstants.COLOR9
  },
  variantsBoxRow:{
    margin:8,
    flex:1,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    borderRadius:5,
    borderWidth:1,
    borderColor:myConstants.NEWSBORDER,
    backgroundColor:myConstants.NEWSBACKGROUND
  },
  variantsBoxCaption:{
    alignItems:'center',
    justifyContent:'center',
    flex:1,
  },
  variantsBoxCaptionText:{
    fontSize:27,
    fontFamily:'WhitneyMedium',
    color:myConstants.GRAY,
  },








  listContainer: {
    flex: 1,
    backgroundColor: myConstants.WHITE,
    alignItems: 'center',
    justifyContent: 'center',
  },
  listHeader:{
    backgroundColor:myConstants.PURPLE,
    alignItems:'center',
    justifyContent:'center',
    flexDirection:'row',
    height:56
  },
  listHeaderTitle:{
    flex:1,
    justifyContent:'flex-start',
    paddingLeft:15
  },
  listHeaderTitleText:{
    color:myConstants.WHITE,
    fontSize:16,
    fontFamily:myConstants.FONTFAMILY
  },
  listHeaderButton:{
    flex:1,
    flexDirection:'row',
    justifyContent:'flex-end',
    paddingRight:15,
    alignItems:'center',
  },
  listHeaderButtonIcon:{
    color:myConstants.PURPLE,
    backgroundColor:myConstants.WHITE,
    fontSize:23,
    alignSelf:'center',
    paddingTop:9,
    paddingLeft:11,
    borderRadius:50,
    width:40,
    height:40
  },
  dataHeaders:{
    flexDirection:'row',
    height:40,
    margin:7
  },
  dataHeader:{
    height:38,

    justifyContent:'center',
    alignItems:'center',
    backgroundColor:'#A9A9A9'
  },
  dataHeaderCaption:{
    fontFamily:myConstants.FONTFAMILY,
    fontSize:11,
  },
  dataRow:{
    flexDirection:'row',
    height:38,
  },
  dataColumn:{
    height:38,
    borderBottomWidth:0.5,
    borderBottomColor:'#A9A9A9',
    backgroundColor:'#F5F5F5',
    justifyContent:'center',
    alignItems:'center',
  },
  dataColumnCaption:{
    fontFamily:myConstants.FONTFAMILY,
    fontSize:11,
  },






  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems:'center',
    backgroundColor: 'rgba(0,0,0, 0.5)',
  },
  innerContainer: {
    width:300,
    height:240,
    borderRadius:5,
    borderWidth:0.8,
    borderColor:myConstants.NEWSBORDER,
    backgroundColor:myConstants.WHITE
  },
  modalHeader:{
    height:35,
    paddingTop:13,
  },
  modalHeaderText:{
    fontFamily:myConstants.FONTFAMILY,
    color:myConstants.BLACK,
    fontSize:18,
    alignSelf:'center',
    paddingTop:2
  },
  modalHeaderSubTitle:{
    height:30,
    paddingTop:8
  },
  modalHeaderSubTitleText:{
    fontFamily:myConstants.FONTFAMILY,
    color:myConstants.BLACK,
    fontSize:14,
    alignSelf:'center'
  },
  modalBox:{
    paddingTop:20,
    paddingBottom:10,
    paddingLeft:30,
    paddingRight:30,
    flex:1
  },
  modalBoxInputBox:{

  },
  modalBoxInputBoxInput:{
    borderWidth:1,
    borderColor:myConstants.NEWSBORDER,
    height:40,
    borderRadius:5,
    fontFamily:myConstants.FONTFAMILY,
  },
  modalButtons:{
    flex:1,
    flexDirection:'row',
    paddingTop:30
  },
  modalButtonLeft:{
    flex:1,
    paddingRight:10,
  },
  modalButtonLeftButton:{
    height:40,
    borderRadius:5,
    justifyContent:'center',
    alignItems:'center',
    borderWidth:1,
    borderColor:myConstants.NEWSBORDER,
    backgroundColor:myConstants.NEWSBACKGROUND
  },
  modalButtonLeftButtonText:{
    fontFamily:myConstants.FONTFAMILY,
    color:myConstants.BLACK
  },
  modalButtonRight:{
    flex:1,
    paddingLeft:10
  },
  modalButtonRightButton:{
    height:40,
    borderRadius:5,
    justifyContent:'center',
    alignItems:'center',
    backgroundColor:myConstants.PURPLE
  },
  modalButtonRightButtonText:{
    fontFamily:myConstants.FONTFAMILY,
    color:myConstants.WHITE
  },




  manageContainer:{
    minHeight:600
  },
  manageHeader:{
    flex:.2, height:85, backgroundColor:myConstants.PURPLE
  },
  manageTitle:{
    flex:1, alignItems:'flex-start', justifyContent:'center'
  },
  manageTitleText:{
    fontFamily:myConstants.FONTFAMILY, fontSize:14, paddingLeft:10,color:myConstants.WHITE
  },
  manageSearch:{
    flex:1, justifyContent:'center', margin:9
  },
  manageSearchInput:{
    flex:1, borderWidth:1, borderColor:'white', paddingLeft:10, borderRadius:5,
    justifyContent:'center', fontFamily:myConstants.FONTFAMILY, fontSize:14, color:myConstants.WHITE
  },
  manageSection:{
    flex:1,
    paddingTop:10
  },
  manageSectionDefault:{
    flex:1,
    alignItems:'center',
  },
  manageSectionDefaultText:{
    fontFamily:myConstants.FONTFAMILY,
    fontSize:30,
    color:myConstants.GRAY
  }
});
