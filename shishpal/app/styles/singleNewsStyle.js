'use strict';

var React = require('react-native');

var {
  StyleSheet,
} = React;

import myConstants from '../constant/constant';

module.exports = StyleSheet.create({

  singleNewsContainer: {
    backgroundColor: myConstants.WHITE,
    flex:1,
    flexDirection:'column'
  },
  listHeader:{
    backgroundColor:myConstants.PURPLE,
    alignItems:'center',
    justifyContent:'center',
    flexDirection:'row',
    height:56
  },
  listHeaderTitle:{
    flex:7,
    justifyContent:'flex-start',
    padding:15,
  },
  listHeaderTitleText:{
    color:myConstants.WHITE,
    fontSize:18,
    fontFamily:myConstants.FONTFAMILY
  },
  listHeaderButton:{
    padding:15,
    flex:1,
    flexDirection:'row',
    justifyContent:'flex-start',
    alignItems:'center',
  },
  listHeaderButtonIcon:{
    color:myConstants.WHITE,
    fontSize:17,
    alignSelf:'center',
  },
  singleNews:{
    margin:10,
    backgroundColor: myConstants.WHITE,
    flexDirection: 'column',
    flex:1,
    borderWidth:0.5,
    borderColor:myConstants.NEWSBORDER,
  },
  singleNewsTitle:{
    flex:1,
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:myConstants.NEWSBACKGROUND,
    padding:10
  },
  singleNewsTitleText:{
    fontFamily:myConstants.FONTFAMILY,
  },
  singleNewsData:{
    flex:1,
    flexDirection:'row',
    borderTopWidth:0.7,
    borderTopColor:myConstants.NEWSBORDER
  },
  singleNewsDataTime:{
    flex:1,
    alignItems:'flex-start',
    flexDirection:'row',
    paddingTop:3,
    paddingBottom:3
  },
  singleNewsDataTimeBoxIcon:{
    flex:1,
    alignItems:'center',
    justifyContent:'center'
  },
  singleNewsDataTimeBoxData:{
    flex:6,
    paddingTop:2,
    paddingBottom:2,
  },
  singleNewsDataTimeBoxDataText:{
    fontFamily:myConstants.FONTFAMILY,
    fontSize:12
  },
  singleNewsDataView:{
    flex:1,
    alignItems:'flex-end',
    flexDirection:'row',
    paddingTop:3,
    paddingBottom:3,
    borderWidth:1
  },
  singleNewsDescription:{
    flex:1,
    alignItems:'center',
    justifyContent:'center',
    padding:10,

  },
  singleNewsDescriptionText:{
    fontFamily:myConstants.FONTFAMILY,
    fontSize:12,
    textAlign:'justify'
  },
  singleNewsMeta:{
    flex:1,
    alignItems:'center',
    justifyContent:'center',
    paddingBottom:10,
    marginTop:20
  },
  imageBox:{
    padding:5,
    width:320,
    height:320,
    borderWidth:0.8,
    borderColor:myConstants.NEWSBORDER,
    borderRadius:3
  },
  imageBoxImage:{
    width:300,
    height:250,
    borderRadius:3
  },
  fullScreenButton:{
    marginTop:10,
    alignItems:'center',
    justifyContent:'center',
  },
  fullScreenButtonText:{
    fontFamily:myConstants.FONTFAMILY,
    fontSize:15,
    paddingTop:7,
    paddingBottom:8,
    paddingLeft:15,
    paddingRight:15,
    borderRadius:3,
    backgroundColor:myConstants.PURPLE,
    color:myConstants.WHITE
  }
});
