'use strict';

var React = require('react-native');

var {
  StyleSheet,
} = React;

import myConstants from '../constant/constant';

module.exports = StyleSheet.create({

  profileContainer: {
    backgroundColor: myConstants.CONTAINER,
    flex:1,
    flexDirection:'column',
  },
  profileMain:{
    flex:1,
    height:200
  },
  listHeader:{
    backgroundColor:myConstants.PURPLE,
    alignItems:'center',
    justifyContent:'center',
    flexDirection:'row',
    height:56
  },
  listHeaderTitle:{
    flex:7,
    justifyContent:'flex-start',
    padding:15,
  },
  listHeaderTitleText:{
    color:myConstants.WHITE,
    fontSize:18,
    fontFamily:myConstants.FONTFAMILY
  },
  listHeaderButton:{
    padding:15,
    flex:1,
    flexDirection:'row',
    justifyContent:'flex-start',
    alignItems:'center',
  },
  listHeaderButtonIcon:{
    color:myConstants.WHITE,
    fontSize:17,
    alignSelf:'center',
  },
  imageBoxContainer:{
    position: 'absolute',
    left: 0,
    top: 0,
    opacity: 0.2,
    zIndex:0
  },
  imageBox:{
    height:190,
    width:375,
    position:'absolute',
    opacity:0.8,
    resizeMode:'stretch'
  },
  profileImageBoxContainer:{
    justifyContent:'center',
    alignItems:'center',
    flex:1,
    paddingTop:6,
    backgroundColor:'transparent'
  },
  profileImageBox:{
    height:100,
    width:100,
    borderRadius:50
  },
  profileImageBoxImage:{
    height:96,
    width:96,
    top:2,
    left:2,
    borderRadius:50
  },
  profileImageBoxPartyImage:{
    position:'absolute',
    top:60,
    left:60,
    borderRadius:50,
    backgroundColor:'white'
  },
  profileImageBoxPartyImageImage:{
    width:37,
    height:37,
    borderRadius:50,
  },
  profileBoxContent:{
    alignItems:'center'
  },
  profileBoxCandidateName:{
    paddingTop:12,
    fontSize:14,
    fontWeight:'bold',
    color:myConstants.WHITE,
    fontFamily:myConstants.FONTFAMILY
  },
  profileBoxAssemblyName:{
    fontStyle:'italic',
    color:myConstants.WHITE,
    fontFamily:myConstants.FONTFAMILY
  },
  profileContent:{
    paddingTop:7,
    paddingBottom:7,
    flex:1,
    flexDirection:'column',
  },
  profileContentBox:{
    backgroundColor:myConstants.WHITE,
    minHeight:45,
    flex:1,
    flexDirection:'row',
    borderBottomWidth:0.7,
    borderBottomColor:myConstants.NEWSBORDER
  },
  profileIcon:{
    flex:2,
    alignItems:'center',
    justifyContent:'center'
  },
  profileHeadName:{
    flex:6,
    justifyContent:'center',
    paddingLeft:0,
  },
  profileHeadNameText:{
    fontFamily:myConstants.FONTFAMILY,
    fontSize:14,
    color:myConstants.CONTAINER2,
  },
  adjustText:{
    paddingTop:5,
    paddingBottom:7,
    paddingRight:5
  },
  profileBodyName:{
    flex:13,
    alignItems:'flex-end',
    justifyContent:'center',
    paddingRight:5,
  },
  profileEditButtonBox:{
    paddingTop:5,
    paddingRight:5,
    paddingLeft:5
  },
  profileEditButton:{
    height:45,
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:myConstants.PURPLE
  },
  profileEditButtonText:{
    fontFamily:myConstants.FONTFAMILY,
    fontSize:18,
    color:myConstants.WHITE,
  }
});
