'use strict';

var React = require('react-native');

var {
  StyleSheet,
} = React;

import myConstants from '../constant/constant';

module.exports = StyleSheet.create({

  writeMeContainer: {
    backgroundColor: myConstants.WHITE,
    justifyContent:'center',
    flex:1,
    flexDirection:'column',
  },
  listHeader:{
    backgroundColor:myConstants.PURPLE,
    alignItems:'center',
    justifyContent:'center',
    flexDirection:'row',
    height:56
  },
  listHeaderTitle:{
    flex:7,
    justifyContent:'flex-start',
    padding:15,
  },
  listHeaderTitleText:{
    color:myConstants.WHITE,
    fontSize:18,
    fontFamily:myConstants.FONTFAMILY
  },
  listHeaderButton:{
    padding:15,
    flex:1,
    flexDirection:'row',
    justifyContent:'flex-start',
    alignItems:'center',
  },
  listHeaderButtonIcon:{
    color:myConstants.WHITE,
    fontSize:17,
    alignSelf:'center',
  },
  writeMeForm:{
    flex:1,
    justifyContent:'center',
    flexDirection:'column',
    paddingLeft:10,
    paddingRight:10,
    paddingTop:10
  },
  writeMeRow:{
    flex:1,
    justifyContent:'center',
    flexDirection:'column',
    marginBottom:15
  },
  writeMeLabel:{
    flex:1,
    justifyContent:'center',
    paddingBottom:4,
  },
  writeMeLabelText:{
    fontSize:myConstants.FONTSIZE14,
    fontFamily:myConstants.FONTFAMILY,
    color:myConstants.GRAY,
  },
  writeMeInput:{
    flex:1,
    flexDirection:'row',
  },
  writeMeInputBox:{
    flex:1,
    borderWidth:1,
    borderColor:myConstants.NEWSBORDER,
    height:40,
    borderRadius:5,
    fontFamily:myConstants.FONTFAMILY,
  },
  datePicker:{
    flex:1,
    borderWidth:1,
    borderColor:myConstants.NEWSBORDER,
    height:40,
    borderRadius:5,
    paddingLeft:10,
    justifyContent:'center'
  },
  datePickerCaption:{
    fontSize:myConstants.FONTSIZE14,
    fontFamily:myConstants.FONTFAMILY,
  },
  switchBox:{
    borderWidth:1,
    borderColor:myConstants.NEWSBORDER,
    flex:1,
    height:40,
    justifyContent:'center',
    alignItems:'center',
    flexDirection:'row',
    borderRadius:5
  },
  switchBoxLeft:{
    flex:2,
    alignItems:'flex-start',
    paddingLeft:15,
  },
  switchBoxRight:{
    flex:7,
    alignItems:'flex-start',
  },
  switchBoxRightText:{
    fontSize:myConstants.FONTSIZE14,
    fontFamily:myConstants.FONTFAMILY
  },
  pickerBox:{
    flex:1,
    borderRadius:5,
    borderWidth:1,
    borderColor:myConstants.NEWSBORDER,
    height:40,
    justifyContent:'center'
  },
  terms:{
    flex:1,
    flexDirection:'row'
  },
  termLeft:{
    flex:7,
  },
  termRight:{
    flex:22,
  },
  errors:{
    borderWidth:1,
    borderColor:'red'
  },
  termRightText:{
    color:'blue',
    fontFamily:myConstants.FONTFAMILY,
    fontSize:myConstants.FONTSIZE14,
    textDecorationLine:'underline'
  },
  submitWriteMe:{
    flex:1,
    alignItems:'center',
    padding:15,
    backgroundColor:myConstants.PURPLE,
    borderRadius:5,
  },
  submitWriteMeText:{
    fontSize:20,
    fontFamily:myConstants.FONTFAMILY,
    color:myConstants.WHITE,
  },








  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems:'center',
    backgroundColor: 'rgba(0,0,0, 0.5)',
  },
  innerContainer: {
    width:300,
    height:240,
    borderRadius:5,
    borderWidth:0.8,
    borderColor:myConstants.NEWSBORDER,
    backgroundColor:myConstants.WHITE
  },
  modalHeader:{
    height:35,
    paddingTop:13,
  },
  modalHeaderText:{
    fontFamily:myConstants.FONTFAMILY,
    color:myConstants.BLACK,
    fontSize:18,
    alignSelf:'center',
    paddingTop:2
  },
  modalHeaderSubTitle:{
    height:30,
    paddingTop:8
  },
  modalHeaderSubTitleText:{
    fontFamily:myConstants.FONTFAMILY,
    color:myConstants.BLACK,
    fontSize:14,
    alignSelf:'center'
  },
  modalBox:{
    paddingTop:20,
    paddingBottom:10,
    paddingLeft:30,
    paddingRight:30,
    flex:1
  },
  modalBoxInputBox:{

  },
  modalBoxInputBoxInput:{
    borderWidth:1,
    borderColor:myConstants.NEWSBORDER,
    height:40,
    borderRadius:5,
    fontFamily:myConstants.FONTFAMILY,
  },
  modalButtons:{
    flex:1,
    flexDirection:'row',
    paddingTop:30
  },
  modalButtonLeft:{
    flex:1,
    paddingRight:10,
  },
  modalButtonLeftButton:{
    height:40,
    borderRadius:5,
    justifyContent:'center',
    alignItems:'center',
    borderWidth:1,
    borderColor:myConstants.NEWSBORDER,
    backgroundColor:myConstants.NEWSBACKGROUND
  },
  modalButtonLeftButtonText:{
    fontFamily:myConstants.FONTFAMILY,
    color:myConstants.BLACK
  },
  modalButtonRight:{
    flex:1,
    paddingLeft:10
  },
  modalButtonRightButton:{
    height:40,
    borderRadius:5,
    justifyContent:'center',
    alignItems:'center',
    backgroundColor:myConstants.PURPLE
  },
  modalButtonRightButtonText:{
    fontFamily:myConstants.FONTFAMILY,
    color:myConstants.WHITE
  },


  termsModalCloseButton:{
    borderWidth:1,
    borderColor:myConstants.NEWSBORDER,
    backgroundColor:myConstants.NEWSBACKGROUND,
    width:30,
    height:30,
    position:'absolute',
    right:-1,
    top:-1,
    alignItems:'center',
    justifyContent:'center',
    borderRadius:5,
  }
});
