'use strict';

var React = require('react-native');

var {
  StyleSheet,
} = React;

import myConstants from '../constant/constant';

module.exports = StyleSheet.create({

  mainContainer: {
    flex: 1,
    backgroundColor: myConstants.WHITE,
    flexDirection:'column'
  },
  listHeader:{
    backgroundColor:myConstants.PURPLE,
    alignItems:'center',
    justifyContent:'center',
    flexDirection:'row',
    height:56
  },
  listHeaderTitle:{
    flex:1,
    justifyContent:'flex-start',
    paddingLeft:15
  },
  listHeaderTitleText:{
    color:myConstants.WHITE,
    fontSize:16,
    fontFamily:myConstants.FONTFAMILY
  },
  listHeaderButton:{
    flex:1,
    flexDirection:'row',
    justifyContent:'flex-end',
    paddingRight:15,
    alignItems:'center',

  },
  listHeaderButtonIcon:{
    color:myConstants.PURPLE,
    backgroundColor:myConstants.WHITE,
    alignSelf:'center',
    fontSize:25,
    paddingTop:8,
    paddingLeft:8,
    borderRadius:50,
    width:40,
    height:40
  },
  content:{
    paddingTop:15,
    paddingLeft:15,
    paddingRight:15,
    flex:1
  },
  column:{
    marginBottom:15,
    height:55
  },
  label:{
    fontSize:12,
    fontFamily:myConstants.FONTFAMILY,
    color:myConstants.GRAY,
  },
  input:{
    flex:1,
    borderWidth:1,
    borderColor:myConstants.NEWSBORDER,
    borderRadius:5,
    marginTop:3,
    fontFamily:myConstants.FONTFAMILY,
  },
  pickerBox:{
    flex:1, borderWidth:1, marginTop:3, height:30, borderRadius:5, borderColor:myConstants.NEWSBORDER
  },
  picker:{
    height:31
  },
  switchBox:{
    borderWidth:1,
    borderColor:myConstants.NEWSBORDER,
    flex:1,
    height:40,
    marginTop:3,
    justifyContent:'center',
    alignItems:'center',
    flexDirection:'row',
    borderRadius:5
  },
  switchBoxLeft:{
    flex:2,
    alignItems:'flex-start',
    paddingLeft:15,
  },
  switchBoxRight:{
    flex:7,
    alignItems:'flex-start',
  },
  switchBoxRightText:{
    fontSize:myConstants.FONTSIZE14,
    fontFamily:myConstants.FONTFAMILY
  },
  uploadAvatarButton:{
    borderWidth:1,
    marginTop:3,
    borderColor:myConstants.NEWSBORDER,
    height:195,
    borderRadius:5,
    alignItems:'center',
    justifyContent:'center'
  },
  uploadAvatarImage:{
    width:217,
    height:180,
    borderRadius:5
  },
  uploadAvatarButtonText:{
    fontFamily:myConstants.FONTFAMILY,
    fontSize:20,
    color:myConstants.GRAY
  },
  showPhotoBox:{
    flex:1, flexDirection:'row'
  },
  showPhotoColumn:{
    flex:2.5, alignItems:'center', justifyContent:'center'
  },
  removePhotoBox:{
    flex:1, alignItems:'center', justifyContent:'center'
  },
  removePhotoButton:{
    paddingTop:30, paddingBottom:30, paddingLeft:22, paddingRight:22, backgroundColor:myConstants.COLOR1, borderRadius:5
  },
  removePhotoButtonIcon:{
    fontSize:30, color:myConstants.WHITE
  },
  submit:{
    marginTop:8,
    alignItems:'center',
    padding:10,
    backgroundColor:myConstants.PURPLE,
    borderRadius:5,
  },
  submitText:{
    fontSize:20,
    fontFamily:myConstants.FONTFAMILY,
    color:myConstants.WHITE,
  },
  socialSubmit:{
    flex:1, flexDirection:'row'
  },
  socialSubmitLeftView:{
    flex:1, alignItems:'flex-start', justifyContent:'center'
  },
  socialSubmitButton:{
    flexDirection:'row',flex:1,width:110, height:40, backgroundColor:myConstants.PURPLE,
    alignItems:'center', justifyContent:'center', borderRadius:5
  },
  socialSubmitIcon:{
    borderWidth:0,flex:0.8, paddingLeft:17, fontSize:20, color:'white'
  },
  socialSubmitText:{
    borderWidth:0,flex:2, paddingLeft:10, fontSize:15, paddingBottom:0, color:'white', fontFamily:myConstants.FONTFAMILY
  },
  socialSubmitRightView:{
    flex:1, alignItems:'flex-end', justifyContent:'center'
  },
  socialSubmitRightIcon:{
    flex:1.8, borderWidth:0,paddingLeft:0, fontSize:20, color:'white'
  },
  socialSubmitRightText:{
    flex:2,borderWidth:0, paddingLeft:19, fontSize:15, paddingBottom:0, color:'white', fontFamily:myConstants.FONTFAMILY
  }
});
