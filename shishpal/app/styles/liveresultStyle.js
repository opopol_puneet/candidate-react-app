'use strict';

var React = require('react-native');

var {
  StyleSheet,
} = React;

import myConstants from '../constant/constant';

module.exports = StyleSheet.create({

  liveResultContainer: {
    backgroundColor: myConstants.WHITE,
    justifyContent:'center',
    flex:1,
    flexDirection:'column',
  },
  liveResultFull:{
    padding:7,
    flex:10,
    marginBottom:80
  },
  listHeader:{
    backgroundColor:myConstants.PURPLE,
    alignItems:'center',
    justifyContent:'center',
    flexDirection:'row',
    height:56
  },
  listHeaderTitle:{
    flex:7,
    justifyContent:'flex-start',
    padding:15,
  },
  listHeaderTitleText:{
    color:myConstants.WHITE,
    fontSize:18,
    fontFamily:myConstants.FONTFAMILY
  },
  listHeaderButton:{
    padding:15,
    flex:1,
    flexDirection:'row',
    justifyContent:'flex-start',
    alignItems:'center',
  },
  listHeaderButtonIcon:{
    color:myConstants.WHITE,
    fontSize:17,
    alignSelf:'center',
  },
  assemblyName:{
    flex:1,
    alignItems:'center',
    padding:10,
    marginTop:10
  },
  assemblyNameText:{
    fontFamily:myConstants.FONTFAMILY,
    fontSize:20,
    color:myConstants.BLACK
  },
  resultRow:{
    flex:1,
    borderWidth:1,
    flexDirection:'row',
    margin:10,
    padding:7,
    borderRadius:5,
    borderColor:myConstants.NEWSBORDER
  },
  candidateImageBox:{
    flex:1,
    borderColor:'red',
    alignItems:'center',
    justifyContent:'center'
  },
  resultDataBox:{
    flex:4,
  },
  resultName:{
    flex:100
  },
  resultNameText:{
    fontFamily:myConstants.FONTFAMILY,
    fontSize:14,
    color:myConstants.BLACK,
    alignSelf:'center'
  },
  percentage:{
    flex:100,
    paddingLeft:10,
    justifyContent:'flex-start'
  },
  progress:{
    flex:100,
    height:1,
    borderRadius:5,
    flexDirection:'row',
    paddingRight:5,
    backgroundColor:myConstants.PURPLERBGA2,
    marginLeft:10,
  },
  progressData:{

  },
  percentageText:{
    fontFamily:myConstants.FONTFAMILY,
    fontSize:14,
    color:myConstants.BLACK,
  },
  partyImageBox:{
    flex:1,
    borderWidth:0,
    paddingLeft:7,
    borderColor:'blue'
  },
  bottomBox:{
    flex:1,
    position:'absolute',
    bottom:0,
    height:80,
    width:360,
    flexDirection:'row',
    backgroundColor:myConstants.NEWSBACKGROUND,
    padding:5,
    borderTopColor:myConstants.NEWSBORDER,
    borderTopWidth:1
  },
  bottomBoxImageView:{
    flex:2, height:70, alignItems:'center', justifyContent:'center'
  },
  bottomBoxImage:{
    width: 90, height: 70, resizeMode:'contain'
  },
  bottomBoxTagView:{
    flex:5,
    height:70,
    padding:10
  },
  bottomBoxTagViewText:{
    fontFamily:myConstants.FONTFAMILY,
    fontSize:14,
    color:myConstants.BLACK,
    alignSelf:'center'
  }
});
