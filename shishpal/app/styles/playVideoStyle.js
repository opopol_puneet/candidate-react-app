'use strict';

var React = require('react-native');

var {
  StyleSheet,
} = React;

import myConstants from '../constant/constant';

module.exports = StyleSheet.create({
  container: {
    backgroundColor: 'white',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  buttonGroup: {
    flexDirection: 'row',
    alignSelf: 'center',
  },
  button: {
    paddingVertical: 4,
    paddingHorizontal: 8,
    alignSelf: 'center',
  },
  buttonText: {
    fontSize: 18,
    color: 'blue',
  },
  buttonTextSmall: {
    fontSize: 15,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
    fontFamily:'WhitneyMedium',
    fontSize:15
  },
  player: {
    alignSelf: 'stretch',
    marginVertical: 10,
  },
});
