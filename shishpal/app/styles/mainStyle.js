'use strict';

var React = require('react-native');

var {
  StyleSheet,
} = React;

import myConstants from '../constant/constant';

module.exports = StyleSheet.create({

  mainContainer: {
    flex: 1,
    backgroundColor: myConstants.WHITE,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection:'column'
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems:'center',
    backgroundColor: 'rgba(0,0,0, 0.5)',
  },
  innerNetworkModalContainer: {
    width:300,
    height:240,
    borderRadius:5,
    borderWidth:0.8,
    borderColor:myConstants.NEWSBORDER,
    backgroundColor:myConstants.WHITE
  },
  innerContainer: {
    width:300,
    height:370,
    justifyContent:'center',
    alignItems:'center',
    borderRadius:5,
    borderWidth:0.8,
    borderColor:myConstants.NEWSBORDER,
    backgroundColor:myConstants.WHITE
  },
  modalHeader:{
    height:35,
    paddingTop:13,
  },
  modalHeaderText:{
    fontFamily:myConstants.FONTFAMILY,
    color:myConstants.BLACK,
    fontSize:18,
    alignSelf:'center',
    paddingTop:2
  },
  modalHeaderSubTitle:{
    height:30,
    paddingTop:8
  },
  modalHeaderSubTitleText:{
    fontFamily:myConstants.FONTFAMILY,
    color:myConstants.BLACK,
    fontSize:14,
    alignSelf:'center'
  },
  modalBox:{
    paddingTop:20,
    paddingBottom:10,
    paddingLeft:30,
    paddingRight:30,
    flex:1,
  },
  modalBoxInputBox:{
    flex:1,
    alignItems:'center'
  },
  modalBoxInputBoxInput:{
    borderColor:myConstants.NEWSBORDER,
    height:40,
    borderRadius:5,
    fontFamily:myConstants.FONTFAMILY,
  },
  modalButtons:{
    flex:1,
    flexDirection:'row',
    paddingTop:30,
  },
  modalButtonLeft:{
    flex:1,
    paddingRight:10,
  },
  modalButtonLeftButton:{
    height:40,
    borderRadius:5,
    justifyContent:'center',
    alignItems:'center',
    borderWidth:1,
    borderColor:myConstants.NEWSBORDER,
    backgroundColor:myConstants.NEWSBACKGROUND
  },
  modalButtonLeftButtonText:{
    fontFamily:myConstants.FONTFAMILY,
    color:myConstants.BLACK
  },
  modalButtonRight:{
    flex:1,
    paddingLeft:10
  },
  modalButtonRightButton:{
    height:40,
    borderRadius:5,
    justifyContent:'center',
    alignItems:'center',
    backgroundColor:myConstants.PURPLE
  },
  modalButtonRightButtonText:{
    fontFamily:myConstants.FONTFAMILY,
    color:myConstants.WHITE
  },
  buttons:{
    borderWidth:2,
    borderColor:myConstants.FACEBOOK,
    width:200,
    height:50,
    justifyContent:'center',
    alignItems:'center',
    flexDirection:'row',
    marginTop:25
  },
  buttonsIcon:{
    color:myConstants.FACEBOOK,
    fontSize:myConstants.FONTSIZE16,
    paddingRight:10,
  },
  buttonsText:{
    color:myConstants.FACEBOOK,
    fontSize:myConstants.FONTSIZE16,
    fontFamily:myConstants.FONTFAMILY,
  },
  statusBar:{
    height:24,
    width:375,
    backgroundColor:myConstants.PURPLE
  },
  headerButtonsContainer:{
    flex:1,
    flexDirection:'row',
    backgroundColor:myConstants.PURPLE
  },
  headerButtonsRow:{
    flex:1,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'flex-end'
  },
  headerButtons:{
    paddingRight:15
  },
  headerButtonsIcon:{
    color:myConstants.WHITE,
    fontSize:myConstants.FONTSIZE16
  },
  profileMain:{
    flex:5,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection:'column',
  },
  imageBoxView:{
    zIndex:0,
    top:0,
    position:'absolute',
  },
  imageBox:{
    height:215,
    width:360,
    zIndex:1,
    opacity:0.8,

  },

  imageBoxContainer:{
    position: 'absolute',
    left: 0,
    top: 0,
    zIndex:0,
    flex:1,
    flexDirection:'column'
  },

  profileImageBoxContainer:{
    justifyContent:'center',
    alignItems:'center',
    flex:1,
    paddingTop:10,
    backgroundColor:'transparent'
  },
  profileImageBox:{
    height:100,
    width:100,
    borderRadius:50,
    backgroundColor:'white'
  },
  profileImageBoxImage:{
    height:96,
    width:96,
    top:2,
    left:2,
    borderRadius:50
  },
  profileImageBoxPartyImage:{
    position:'absolute',
    top:60,
    left:60,
    borderRadius:50,
    backgroundColor:'white'
  },
  profileImageBoxPartyImageImage:{
    width:37,
    height:37,
    borderRadius:50,
  },
  profileBoxContent:{
    alignItems:'center'
  },
  profileBoxCandidateName:{
    paddingTop:19,
    fontSize:14,
    color:myConstants.WHITE,
    fontWeight:'bold',
    fontFamily:'Kreon'
  },
  profileBoxAssemblyName:{
    fontStyle:'italic',
    fontFamily:'Kreon',
    color:myConstants.WHITE,
  },
  profileNavs:{
    flex:8,
    flexDirection:'column',
    alignItems:'center',
    justifyContent:'center',
    paddingTop:10,
    paddingLeft:5,
    paddingRight:5,
    paddingBottom:7
  },
  variantsBoxes:{
    flex:1,
    flexDirection:'row',
  },
  box1:{
    backgroundColor:myConstants.COLOR1
  },
  box2:{
    backgroundColor:myConstants.COLOR2
  },
  box3:{
    backgroundColor:myConstants.COLOR3
  },
  box4:{
    backgroundColor:myConstants.COLOR4
  },
  box5:{
    backgroundColor:myConstants.COLOR5
  },
  box6:{
    backgroundColor:myConstants.COLOR6
  },
  box7:{
    backgroundColor:myConstants.COLOR7
  },
  box8:{
    backgroundColor:myConstants.COLOR8
  },
  box9:{
    backgroundColor:myConstants.COLOR9
  },
  variantsBoxRow:{
    margin:5,
    flex:1,
    alignItems:'center',
    justifyContent:'center',
    borderRadius:5,
    borderWidth:1,
    borderColor:myConstants.NEWSBORDER
  },
  variantsBoxCaption:{
    color:myConstants.BLACK,
    fontFamily:'WhitneyMedium',
    marginTop:3,
    fontSize:12
  }
});
