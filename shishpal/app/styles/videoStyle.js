'use strict';

var React = require('react-native');

var {
  StyleSheet,
} = React;

import myConstants from '../constant/constant';

module.exports = StyleSheet.create({

  mainContainer: {
    flex: 1,
    backgroundColor: myConstants.CONTAINER,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection:'row'
  },
  videos:{
    flex:1,
    paddingTop:5
  },
  video:{
    height:50,
    justifyContent:'center',
    marginTop:5,
    marginBottom:5,
    backgroundColor:myConstants.WHITE
  },
  videoText:{
    paddingLeft:15,
    fontFamily:'WhitneyMedium',
  }
});
