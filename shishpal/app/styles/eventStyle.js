'use strict';

var React = require('react-native');

var {
  StyleSheet,
} = React;

import myConstants from '../constant/constant';

module.exports = StyleSheet.create({

  eventsContainer: {
    backgroundColor: myConstants.WHITE,
    justifyContent:'center',
    flex:1,
    flexDirection:'column',
  },
  event:{
    flex:1,
    borderWidth:0.8,
    borderRadius:3,
    borderColor:myConstants.NEWSBORDER,
    flexDirection:'row',
    marginTop:8,
    marginLeft:8,
    marginRight:8,
    padding:5
  },
  listHeader:{
    backgroundColor:myConstants.PURPLE,
    alignItems:'center',
    justifyContent:'center',
    flexDirection:'row',
    height:56
  },
  listHeaderTitle:{
    flex:7,
    justifyContent:'flex-start',
    padding:15,
  },
  listHeaderTitleText:{
    color:myConstants.WHITE,
    fontSize:18,
    fontFamily:myConstants.FONTFAMILY
  },
  listHeaderButton:{
    padding:15,
    flex:1,
    flexDirection:'row',
    justifyContent:'flex-start',
    alignItems:'center',
  },
  listHeaderButtonIcon:{
    color:myConstants.WHITE,
    fontSize:17,
    alignSelf:'center',
  },
  eventImageView:{
    flex:2,
    justifyContent:'center',
    alignItems:'center'
  },
  eventDataView:{
    flex:10,
  },
  eventTitle:{
    flex:1,
    alignItems:'center',
    flexDirection:'row',
    paddingLeft:10
  },
  eventTitleText:{
    fontFamily:myConstants.FONTFAMILY,
    color:myConstants.BLACK,
    fontSize:myConstants.FONTSIZE14
  },
  eventText:{
    fontFamily:myConstants.FONTFAMILY,
    fontSize:myConstants.FONTSIZE12,
    color:myConstants.PURPLE
  },
  eventDate:{
    flex:1,
    flexDirection:'row',
    justifyContent:'center',
  },
  eventIcon:{
    flex:1,
    flexDirection:'column',
    justifyContent:'center'
  },
  eventData:{
    flex:4,
    flexDirection:'column',
  },




  singleEventContainer:{
    backgroundColor: myConstants.WHITE,
    justifyContent:'center',
    flex:1,
    flexDirection:'column',
    paddingBottom:15,
  },

  singleEvent:{
    marginTop:8,
    marginLeft:8,
    marginRight:8,
    flex:1,
    borderWidth:1,
    borderColor:myConstants.NEWSBORDER,
  },
  singleEventHeader:{
    flex:1,
    backgroundColor:myConstants.NEWSBACKGROUND,
    justifyContent:'center',
    alignItems:'center',
    padding:10,
    borderBottomWidth:1,
    borderColor:myConstants.NEWSBORDER,
  },
  singleEventHeaderText:{
    fontSize:myConstants.FONTSIZE14,
    color:myConstants.BLACK,
    fontFamily:myConstants.FONTFAMILY
  },
  singleEventContent:{
    flex:10,
    alignItems:'center',
    justifyContent:'center',
    paddingTop:10,
    marginBottom:10
  },
  singleEventImage:{
    flex:1,
    padding:10,
    alignItems:'center',
    justifyContent:'center'
  },
  singleEventDescription:{
    flex:1,
    backgroundColor:"#fff",
    padding:10
  },
  singleEventDescriptionText:{
    fontSize:myConstants.FONTSIZE12,
    color:myConstants.BLACK,
    textAlign:'justify',
    fontFamily:myConstants.FONTFAMILY,
  }
});
