'use strict';

var React = require('react-native');

var {
  StyleSheet,
} = React;

import myConstants from '../constant/constant';

module.exports = StyleSheet.create({

  interviewContainer: {
    backgroundColor: myConstants.WHITE,
    justifyContent:'center',
    flex:1,
    flexDirection:'column',
  },
  interviewFull:{
    // padding:7,
    flex:1,
  },
  interview:{
    borderWidth:1,
    borderColor:myConstants.NEWSBORDER,
    backgroundColor:myConstants.NEWSBACKGROUND,
    borderRadius:5,
    margin:7,
    padding:5,
    flex:1,
    flexDirection:'column'
  },
  interviewThumbnail:{
    flex:1,
    height:300
  },
  listHeader:{
    backgroundColor:myConstants.PURPLE,
    alignItems:'center',
    justifyContent:'center',
    flexDirection:'row',
    height:56
  },
  listHeaderTitle:{
    flex:7,
    justifyContent:'flex-start',
    padding:15,
  },
  listHeaderTitleText:{
    color:myConstants.WHITE,
    fontSize:18,
    fontFamily:myConstants.FONTFAMILY
  },
  listHeaderButton:{
    padding:15,
    flex:1,
    flexDirection:'row',
    justifyContent:'flex-start',
    alignItems:'center',
  },
  listHeaderButtonIcon:{
    color:myConstants.WHITE,
    fontSize:17,
    alignSelf:'center',
  },
  interviewHeading:{
    flex:1,
    flexDirection:'row',
    marginTop:8
  },
  interviewHeadingTitle:{
    flex:4,
  },
  interviewHeadingTitleText:{
    fontSize:myConstants.FONTSIZE14,
    fontFamily:myConstants.FONTFAMILY
  },
  interviewPlay:{
    borderRadius:5,
    flex:1,
    justifyContent:'center',
    alignItems:'center',
    backgroundColor:myConstants.PURPLE,
  },
  interviewPlayIcon:{
    fontSize:30,
    color:myConstants.WHITE
  }
});
