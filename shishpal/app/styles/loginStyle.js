'use strict';

var React = require('react-native');

var {
  StyleSheet,
} = React;

import myConstants from '../constant/constant';

module.exports = StyleSheet.create({

  loginContainer: {
    backgroundColor: myConstants.WHITE,
    justifyContent:'center',
    alignItems:'center',
    flex:1,
    flexDirection:'column',
  },
  listHeader:{
    backgroundColor:myConstants.PURPLE,
    alignItems:'center',
    justifyContent:'center',
    flexDirection:'row',
    height:56
  },
  listHeaderTitle:{
    flex:7,
    justifyContent:'flex-start',
    padding:15,
  },
  listHeaderTitleText:{
    color:myConstants.WHITE,
    fontSize:18,
    fontFamily:myConstants.FONTFAMILY
  },
  listHeaderButton:{
    padding:15,
    flex:1,
    flexDirection:'row',
    justifyContent:'flex-start',
    alignItems:'center',
  },
  listHeaderButtonIcon:{
    color:myConstants.WHITE,
    fontSize:17,
    alignSelf:'center',
  },
  loginBox:{
    flexDirection:'column',
    padding:10,
    flex:1
  },
  loginHead:{
    flex:1,
    alignItems:'center',
    zIndex:10
  },
  loginContent:{
    flex:1,
    marginTop:-34,
    borderWidth:1,
    borderColor:myConstants.NEWSBORDER,
    borderRadius:3,
    paddingTop:46,
    paddingBottom:10,
    zIndex:0
  },
  loginContentRow:{
    flex:1,
    paddingLeft:15,
    paddingRight:15,
    margin:10,
  },
  loginContentSwitch:{
    flex:1,
    alignItems:'flex-start'
  },
  loginContentRowInput:{
    borderWidth:1,
    height:43,
    borderRadius:5,
    paddingLeft:7,
    alignItems:'center',
    borderColor:myConstants.NEWSBORDER,
    fontFamily:myConstants.FONTFAMILY
  },
  loginButton:{
    flex:1,
    flexDirection:'row',
    backgroundColor:myConstants.PURPLE,
    padding:5,
    borderRadius:5
  },
  loginButtonText:{
    fontSize:20,
    fontFamily:myConstants.FONTFAMILY,
    color:myConstants.WHITE,
    flex:1.6,
    paddingTop:4,
    textAlign:'right',
    paddingRight:10
  },
  loginButtonIcon:{
    fontSize:35,
    color:myConstants.WHITE,
    flex:1,
    textAlign:'right'
  },
  forgetPasswordBox:{
    flex:1,
    marginTop:15,
    alignItems:'center'
  },
  forgetPasswordButtonText:{
    fontSize:16,
    fontFamily:myConstants.FONTFAMILY,
    color:myConstants.GRAY,
  },




  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems:'center',
    backgroundColor: 'rgba(0,0,0, 0.5)',
  },
  innerContainer: {
    width:300,
    height:240,
    borderRadius:5,
    borderWidth:0.8,
    borderColor:myConstants.NEWSBORDER,
    backgroundColor:myConstants.WHITE
  },
  modalHeader:{
    height:35,
    paddingTop:13,
  },
  modalHeaderText:{
    fontFamily:myConstants.FONTFAMILY,
    color:myConstants.BLACK,
    fontSize:18,
    alignSelf:'center',
    paddingTop:2
  },
  modalHeaderSubTitle:{
    height:30,
    paddingTop:8
  },
  modalHeaderSubTitleText:{
    fontFamily:myConstants.FONTFAMILY,
    color:myConstants.BLACK,
    fontSize:14,
    alignSelf:'center'
  },
  modalBox:{
    paddingTop:20,
    paddingBottom:10,
    paddingLeft:30,
    paddingRight:30,
    flex:1
  },
  modalBoxInputBox:{

  },
  modalBoxInputBoxInput:{
    borderWidth:1,
    borderColor:myConstants.NEWSBORDER,
    height:40,
    borderRadius:5,
    fontFamily:myConstants.FONTFAMILY,
  },
  modalButtons:{
    flex:1,
    flexDirection:'row',
    paddingTop:30
  },
  modalButtonLeft:{
    flex:1,
    paddingRight:10,
  },
  modalButtonLeftButton:{
    height:40,
    borderRadius:5,
    justifyContent:'center',
    alignItems:'center',
    borderWidth:1,
    borderColor:myConstants.NEWSBORDER,
    backgroundColor:myConstants.NEWSBACKGROUND
  },
  modalButtonLeftButtonText:{
    fontFamily:myConstants.FONTFAMILY,
    color:myConstants.BLACK
  },
  modalButtonRight:{
    flex:1,
    paddingLeft:10
  },
  modalButtonRightButton:{
    height:40,
    borderRadius:5,
    justifyContent:'center',
    alignItems:'center',
    backgroundColor:myConstants.PURPLE
  },
  modalButtonRightButtonText:{
    fontFamily:myConstants.FONTFAMILY,
    color:myConstants.WHITE
  },
});
