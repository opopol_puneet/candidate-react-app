import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  Image,
  TouchableOpacity,
  Linking,
  ToastAndroid,
  Modal,
  AsyncStorage,
  BackHandler,
  NetInfo,
  TouchableHighlight
} from 'react-native';

var styles = require('../styles/mainStyle');


import TopStatusBar from '../extras/statusBar';

import Network from '../extras/network';

import Icon from 'react-native-vector-icons/FontAwesome';

import myConstants from '../constant/constant';

import Share, {ShareSheet, Button} from 'react-native-share';

import { Actions } from 'react-native-router-flux';

import RNExitApp from 'react-native-exit-app';

import SplashScreen from 'react-native-smart-splash-screen';

import AndroidOpenSettings from 'react-native-android-open-settings'

export default class Main extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      socialMediaModal : false,
      networkModal : false,
      exitAppModal : false
    }
  }

  componentDidMount(){
    fetch(myConstants.USER.API_URL+"/candidate/profile/"+myConstants.USER.USER_ID)
    .then((response) => response.json())
    .then((data) => {
      this.setState(data.profile);
      AsyncStorage.setItem('user', JSON.stringify(data.profile));
    })
    .catch((error) => {
    })
    SplashScreen.close({
        animationType: SplashScreen.animationType.scale,
        duration: 1500,
        delay: 500,
     })

    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick.bind(this));
    NetInfo.addEventListener('connectionChange',this.handleFirstConnectivityChange.bind(this));
  }

  handleBackButtonClick(){
      this.setState({exitAppModal:true});
      return true;
  }

  handleFirstConnectivityChange(connectionInfo) {
    connectionInfo.type==='none' ?  this.setState({networkModal:true}) :  this.setState({networkModal:false});
  }

  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick.bind(this));
    NetInfo.removeEventListener('connectionChange',this.handleFirstConnectivityChange.bind(this));
  }

  closeNetworkModal() {
    this.setState({networkModal:false});
  }

  closeExitAppModal() {
    this.setState({exitAppModal:false});
    return true;
  }

  openSettings(){
    AndroidOpenSettings.generalSettings()
  }

  exitApp(){
    RNExitApp.exitApp();
    return false;
  }

  render() {
    return (
      <View style={styles.mainContainer}>
        <TopStatusBar/>
        <Modal
              visible={this.state.exitAppModal}
              animationType={'slide'}
              transparent={true}
              onRequestClose={() => this.closeExitAppModal()}
          >
            <View style={styles.modalContainer}>
              <View style={styles.innerNetworkModalContainer}>
                <View style={styles.modalHeader}>
                  <Text style={styles.modalHeaderText}>Exit App</Text>
                </View>
                <View style={styles.modalHeaderSubTitle}>
                  <Text style={styles.modalHeaderSubTitleText}>Exit App</Text>
                </View>
                <View style={styles.modalBox}>
                  <View style={styles.modalBoxInputBox}>
                    <Icon name="window-close" style={{fontSize:60, color:myConstants.BLACK}} />
                  </View>
                  <View style={styles.modalButtons}>
                    <View style={styles.modalButtonLeft}>
                    <TouchableHighlight underlayColor='rgba(138, 71, 221, 0.8)' style={styles.modalButtonLeftButton} onPress={this.closeExitAppModal.bind(this)}>
                        <Text style={styles.modalButtonLeftButtonText}>No</Text>
                      </TouchableHighlight>
                    </View>
                    <View style={styles.modalButtonRight}>
                    <TouchableHighlight underlayColor='rgba(138, 71, 221, 0.8)' style={styles.modalButtonRightButton} onPress={this.exitApp.bind(this)}>
                        <Text style={styles.modalButtonRightButtonText}>Yes</Text>
                      </TouchableHighlight>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          </Modal>
        <Modal
              visible={this.state.networkModal}
              animationType={'slide'}
              transparent={true}
              onRequestClose={() => this.closeNetworkModal()}
          >
            <View style={styles.modalContainer}>
              <View style={styles.innerNetworkModalContainer}>
                <View style={styles.modalHeader}>
                  <Text style={styles.modalHeaderText}>Network Error</Text>
                </View>
                <View style={styles.modalHeaderSubTitle}>
                  <Text style={styles.modalHeaderSubTitleText}>No Internet Connection Found</Text>
                </View>
                <View style={styles.modalBox}>
                  <View style={styles.modalBoxInputBox}>
                    <Icon name="exclamation-triangle" style={{fontSize:60, color:myConstants.BLACK}} />
                  </View>
                  <View style={styles.modalButtons}>
                    <View style={styles.modalButtonLeft}>
                    <TouchableHighlight underlayColor='rgba(138, 71, 221, 0.8)' style={styles.modalButtonLeftButton} onPress={this.closeNetworkModal.bind(this)}>
                        <Text style={styles.modalButtonLeftButtonText}>OK</Text>
                      </TouchableHighlight>
                    </View>
                    <View style={styles.modalButtonRight}>
                    <TouchableHighlight underlayColor='rgba(138, 71, 221, 0.8)' style={styles.modalButtonRightButton} onPress={this.openSettings.bind(this)}>
                        <Text style={styles.modalButtonRightButtonText}>Settings</Text>
                      </TouchableHighlight>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          </Modal>
        <Modal
              visible={this.state.socialMediaModal}
              animationType={'slide'}
              transparent={true}
              onRequestClose={() => this.closeModal()}
          >
            <View style={styles.modalContainer}>
              <View style={styles.innerContainer}>
                <TouchableOpacity activeOpacity={0.9} style={styles.buttons} onPress={this.openFacebook.bind(this)}>
                  <Icon name="facebook" style={styles.buttonsIcon} />
                  <Text style={styles.buttonsText}>Facebook</Text>
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={0.9} style={[styles.buttons, {borderColor:myConstants.TWITTER}]} onPress={this.openTwitter.bind(this)}>
                  <Icon name="twitter" style={[styles.buttonsIcon, {color:myConstants.TWITTER}]} />
                  <Text style={[styles.buttonsText, {color:myConstants.TWITTER}]}>Twitter</Text>
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={0.9} style={[styles.buttons, {borderColor:myConstants.WHATSAPP}]} onPress={this.openWhatsapp.bind(this)}>
                  <Icon name="whatsapp" style={[styles.buttonsIcon, {color:myConstants.WHATSAPP}]} />
                  <Text style={[styles.buttonsText, {color:myConstants.WHATSAPP}]}>Whatsapp</Text>
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={0.9} style={[styles.buttons, {borderColor:myConstants.INSTAGRAM}]} onPress={this.openInstagram.bind(this)}>
                  <Icon name="instagram" style={[styles.buttonsIcon, {color:myConstants.INSTAGRAM}]} />
                  <Text style={[styles.buttonsText, {color:myConstants.INSTAGRAM}]}>Instagram</Text>
                </TouchableOpacity>
              </View>
            </View>
          </Modal>
        <View style={styles.headerButtonsContainer}>
          <View style={styles.headerButtonsRow}>
            <TouchableOpacity activeOpacity={0.9} style={styles.headerButtons} onPress={this.openNotificationScreen.bind(this)}>
              <Icon name="bell" style={styles.headerButtonsIcon} />
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.9} style={styles.headerButtons} onPress={this.openLoginScreen.bind(this)}>
              <Icon name="user" style={styles.headerButtonsIcon} />
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.profileMain}>
          <View style={styles.imageBoxView}>
            <Image source={myConstants.USER.PARTY_BANNER} style={styles.imageBox} />
          </View>
          <View style={styles.profileImageBoxContainer}>
              <View style={styles.profileImageBox}>
                <Image source={myConstants.USER.CANDIDATE_IMAGE} style={styles.profileImageBoxImage} />
                <View style={styles.profileImageBoxPartyImage}>
                  <Image source={myConstants.USER.PARTY_LOGO} style={styles.profileImageBoxPartyImageImage} />
                </View>
              </View>
              <View style={styles.profileBoxContent}>
                <Text style={styles.profileBoxCandidateName}>{myConstants.USER.CANDIDATE_NAME}</Text>
                <Text style={styles.profileBoxAssemblyName}>{myConstants.USER.ASSEMBLY}</Text>
              </View>
          </View>
        </View>
        <View style={styles.profileNavs}>
          <View style={styles.variantsBoxes}>
            <TouchableOpacity activeOpacity={0.9} style={styles.variantsBoxRow} onPress={this.openEventsScreen.bind(this)}>
              <Image source={myConstants.RESOURCES.EVENTS} style={{width: 70, height: 70}} />
              <Text style={styles.variantsBoxCaption}>Events</Text>
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.9} style={styles.variantsBoxRow} onPress={this.openNewsScreen.bind(this)}>
              <Image source={myConstants.RESOURCES.NEWS} style={{width: 70, height: 70}} />
              <Text style={styles.variantsBoxCaption}>News</Text>
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.9} style={styles.variantsBoxRow} onPress={this.openProfileScreen.bind(this)}>
              <Image source={myConstants.RESOURCES.PROFILE} style={{width: 70, height: 70}} />
              <Text style={styles.variantsBoxCaption}>Profile</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.variantsBoxes}>
          <TouchableOpacity activeOpacity={0.9} style={styles.variantsBoxRow} onPress={this.openSocialMediaModal.bind(this)}>
            <Image source={myConstants.RESOURCES.SOCIAL_MEDIA} style={{width: 70, height: 70}} />
            <Text style={styles.variantsBoxCaption}>Social Media</Text>
          </TouchableOpacity>
          <TouchableOpacity activeOpacity={0.9} style={styles.variantsBoxRow} onPress={this.openWriteMeScreen.bind(this)}>
            <Image source={myConstants.RESOURCES.WRITE_ME} style={{width: 70, height: 70}} />
            <Text style={styles.variantsBoxCaption}>Suggestion</Text>
          </TouchableOpacity>
          <TouchableOpacity activeOpacity={0.9} style={styles.variantsBoxRow} onPress={this.openInterviewScreen.bind(this)}>
            <Image source={myConstants.RESOURCES.CANDIDATE_INTERVIEW} style={{width: 70, height: 70}} />
            <Text style={styles.variantsBoxCaption}>Interview</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.variantsBoxes}>
          <TouchableOpacity activeOpacity={0.9} style={styles.variantsBoxRow} onPress={this.openLiveResultScreen.bind(this)}>
            <Image source={myConstants.RESOURCES.RAJNEETI_LIVE} style={{width: 70, height: 70}} />
            <Text style={styles.variantsBoxCaption}>Live Result</Text>
          </TouchableOpacity>
          <TouchableOpacity activeOpacity={0.9} style={styles.variantsBoxRow} onPress={this.shareProfile.bind(this)}>
            <Image source={myConstants.RESOURCES.SHARE_PROFILE} style={{width: 70, height: 70}} />
            <Text style={styles.variantsBoxCaption}>Share Profile</Text>
          </TouchableOpacity>
          <TouchableOpacity activeOpacity={0.9} style={styles.variantsBoxRow} onPress={this.openTeamJoinScreen.bind(this)}>
            <Image source={myConstants.RESOURCES.JOIN_TEAM} style={{width: 70, height: 70}} />
            <Text style={styles.variantsBoxCaption}>Join Team</Text>
          </TouchableOpacity>
        </View>
        </View>
      </View>
    );
  }

  openNotificationScreen(){
    Actions.notification();
  }

  openLoginScreen(){
    AsyncStorage.getItem('token').then((token) => {
      if(token){
        Actions.agenthome();
      }
      else{
        Actions.login();
      }
    })
  }

  openEventsScreen(){
    Actions.events();
  }

  openNewsScreen(){
    Actions.news();
  }

  openProfileScreen(){
    Actions.profile();
  }

  openSocialMediaModal(){
    this.setState({socialMediaModal:true});
  }

  closeModal() {
    this.setState({socialMediaModal:false});
  }

  openFacebook(){
    Linking.canOpenURL(myConstants.SOCIAL.IFFACEBOOKAPPAVAILABLE).then(supported => {
      if(supported){
        Linking.openURL("fb://facewebmodal/f?href="+this.state.facebookURL);
      }
      else {
        Linking.openURL(myConstants.SOCIAL.IFFACEBOOKAPPNOTAVAILABLE);
      }
    }).catch(err => {
    })
  }

  openTwitter(){
    Linking.canOpenURL(myConstants.SOCIAL.IFTWITTERAPPAVAILABLE).then(supported => {
      if(supported){
        Linking.openURL("twitter://user?screen_name="+this.state.twitter);
      }
      else {
        Linking.openURL(myConstants.SOCIAL.IFTWITTERAPPNOTAVAILABLE);
      }
    }).catch(err => {
    })
  }

  openWhatsapp(){
    Linking.canOpenURL(myConstants.SOCIAL.IFWHATSAPPAPPAVAILABLE).then(supported => {
      if(supported){
        Linking.openURL("whatsapp://send?text=Hello&phone=+91"+this.state.mobile);
      }
      else {
        Linking.openURL(myConstants.SOCIAL.IFWHATSAPPAPPNOTAVAILABLE);
      }
    }).catch(err => {
    })
  }

  openInstagram(){
    Linking.canOpenURL(myConstants.SOCIAL.IFINSTAGRAMAPPAVAILABLE).then(supported => {
      if(supported){
        // ToastAndroid.show('Navigating to Twitter!', ToastAndroid.SHORT);
        Linking.openURL("instagram://user?username="+this.state.instagram);
      }
      else {
        Linking.openURL(myConstants.SOCIAL.IFINSTAGRAMAPPNOTAVAILABLE);
      }
    }).catch(err => {
      alert(err);
    })
  }

  openWriteMeScreen(){
    Actions.writeme();
  }

  openInterviewScreen(){
    Actions.interview();
  }

  openLiveResultScreen(){
    Actions.liveresult();
  }

  shareProfile(){
    AsyncStorage.getItem('user').then((data) => {
      data = JSON.parse(data);
      let shareOptions = {
        title: data.name+" - Next MLA",
        message: "For vote and support to "+data.name+", MLA candidate of "+data.party+" from "+data.assembly+" assembly Constituency. To Vote and Support visit on link "+myConstants.USER.REFER_URL+"/profile/"+data.slug,
        url: '',
        subject: "For vote and support to "+data.name+", MLA candidate of "+data.party+" from "+data.assembly+" assembly Constituency. To Vote and Support visit on link "+myConstants.USER.REFER_URL+"/profile/"+data.slug,
      };
      Share.open(shareOptions);
    })
  }

  openTeamJoinScreen(){
    Actions.team();
  }
}
