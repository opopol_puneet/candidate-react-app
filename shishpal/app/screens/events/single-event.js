import React from 'react';
import { Text, View, StatusBar, Image, TouchableOpacity, ListView, ScrollView, ActivityIndicator, WebView } from 'react-native';

var styles = require('../../styles/eventStyle');

import TopStatusBar from '../../extras/statusBar';

import Icon from 'react-native-vector-icons/FontAwesome';

import myConstants from '../../constant/constant';

import { Actions } from 'react-native-router-flux';

export default class SingleEvent extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      loading:true,
      image:'',
      errorMsg:'',
      Height:0,
      Width:320
    };
  }

  componentDidMount(){
    fetch(myConstants.USER.API_URL+"/event/"+this.props.id)
    .then((response) => response.json())
    .then((data) => {
      this.setState(data.event);
      this.setState({image: {
        uri : myConstants.USER.IMAGE_URL+'/images/events_photo/'+data.event.photo
      }})
      this.setState({loading:false});
    })
    .catch((error) => {
      this.setState({errorMsg:'No Events'});
      this.setState({loading:false});
    })
  }
  onNavigationChange(event) {
        if (event.title) {
            const htmlHeight = Number(event.title) //convert to number
            this.setState({Height:htmlHeight});
        }

  }

  goToBack(){
    Actions.events();
  }


  render() {
    if(this.state.loading) {
      return <View style={{marginTop:20}}><ActivityIndicator size="large" color={myConstants.PURPLE} /></View>;
    }
    var style = "<style>body, html, #height-calculator {margin: 0;padding: 0;}#height-calculator {position: absolute;top: 0;left: 0;right: 0;}</style>";

    var script = '<script>window.location.hash = 1; var calculator = document.createElement("div");  calculator.id = "height-calculator"; while (document.body.firstChild) {calculator.appendChild(document.body.firstChild);document.body.appendChild(calculator);document.title = calculator.clientHeight;}</script>'

    var html = "<!DOCTYPE html><html><body><p style='text-align:justify;font-size:13px;line-height:17px;letter-spacing:0.5px;font-family: WhitneyMedium;'>" + this.state.description + "</p></body></html>";

    return (
      <View style={styles.singleEventContainer}>
        <TopStatusBar/>
        <View style={styles.listHeader}>
          <TouchableOpacity style={styles.listHeaderButton} activeOpacity={0.9} onPress={this.goToBack.bind(this)}>
            <Icon name="chevron-left" style={styles.listHeaderButtonIcon} />
          </TouchableOpacity>
          <View style={styles.listHeaderTitle}>
            <Text style={styles.listHeaderTitleText}>{this.state.event_name.length<30 ? this.state.event_name : this.state.event_name.substring(0,25)+'...'}</Text>
          </View>
        </View>
        <View style={{flex:1, flexDirection:'row'}}>
          <ScrollView>
            <View style={styles.singleEvent}>
              <View style={styles.singleEventHeader}>
                <Text style={styles.singleEventHeaderText}>{this.state.event_name}</Text>
              </View>
              <View style={styles.singleEventContent}>
                <View style={styles.eventTitle}>
                  <View style={[styles.eventDate, {flex:8, paddingLeft:2}]}>
                    <View style={[styles.eventIcon, {flex:.5}]}>
                      <Icon name="calendar" size={14} color={myConstants.PURPLE} />
                    </View>
                    <View style={styles.eventData}>
                      <Text style={styles.eventText}>{this.state.date} {this.state.time}</Text>
                    </View>
                  </View>
                  <View style={[styles.eventDate, {flex:5, }]}>
                    <View style={[styles.eventIcon, {flex:1.2}]}>
                      <Icon name="map-marker" size={14} color={myConstants.PURPLE} />
                    </View>
                    <View style={styles.eventData}>
                      <Text style={styles.eventText}>{this.state.location}</Text>
                    </View>
                  </View>
                  <View style={[styles.eventDate, {flex:2}]}>
                    <View style={[styles.eventIcon, {flex:3}]}>
                      <Icon name="eye" size={14} color={myConstants.PURPLE} />
                    </View>
                    <View style={styles.eventData}>
                      <Text style={styles.eventText}>{this.state.view}</Text>
                    </View>
                  </View>
                </View>
                {
                  this.state.photo
                  ?
                  <View style={styles.singleEventImage}>
                    <Image source={this.state.image} style={{width:320, height:400, resizeMode:'contain', borderWidth:1, flex:1, borderRadius:5}} />
                  </View>
                  :
                  null
                }
                <View style={styles.singleEventDescription}>
                  <Text style={styles.singleEventDescriptionText}>{this.state.description}</Text>
                </View>
              </View>
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }
}
