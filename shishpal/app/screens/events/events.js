import React from 'react';
import { Text, View, StatusBar, Image, TouchableOpacity, ListView, ScrollView, ActivityIndicator, NetInfo, BackHandler } from 'react-native';

var styles = require('../../styles/eventStyle');

import TopStatusBar from '../../extras/statusBar';

import Icon from 'react-native-vector-icons/FontAwesome';

import myConstants from '../../constant/constant';

import { Actions } from 'react-native-router-flux';

import Network from '../../extras/network';

const ds = new ListView.DataSource({rowHasChanged:(row1, row2) => row1 != row2});

export default class Events extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      dataSource: ds.cloneWithRows(['row 1', 'row 2']),
      loading:true,
      errorMsg:'',
      network:true
    };
  }

  componentDidMount(){
    fetch(myConstants.USER.API_URL+"/events/user/"+myConstants.USER.USER_ID)
    .then((response) => response.json())
    .then((data) => {
      this.setState({dataSource:this.state.dataSource.cloneWithRows(data.events)});
      this.setState({loading:false});
    })
    .catch((error) => {
      this.setState({errorMsg:'No Events'});
      this.setState({loading:false});
    })

    NetInfo.addEventListener('connectionChange',this.handleFirstConnectivityChange.bind(this));
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick.bind(this));
  }

  handleFirstConnectivityChange(connectionInfo) {
    connectionInfo.type==='none' ?  this.setState({network:false}) :  this.setState({network:true});
  }

  componentWillUnmount(){
    NetInfo.removeEventListener('connectionChange',this.handleFirstConnectivityChange.bind(this));
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick.bind(this));
  }

  handleBackButtonClick(){
    // console.warn(Actions.currentScene);
      Actions.home();
      return true;
  }

  imageOnLoad = () => {
    return <View style={{ backgroundColor: 'red', position: 'absolute', borderRadius: 50, height: 100, width: 100 }} />
  }

  _renderRow(rowData){
    let image;
    rowData.photo
    ?
    image = {
        uri : myConstants.USER.IMAGE_URL+'/images/events_photo/'+rowData.photo
    }
    :
    image = require('../../../resources/RAJNEETI_LIVE.png')

      return (
        <TouchableOpacity activeOpacity={0.9} style={styles.event} onPress={this.openSinglEventScreen.bind(this, rowData.id)}>
          <View style={styles.eventImageView}>
            <Image source={image} style={{width: 50, height: 50}} />
          </View>
          <View style={styles.eventDataView}>
            <View style={styles.eventTitle}>
              <Text style={styles.eventTitleText}>{rowData.event_name}</Text>
            </View>
            <View style={styles.eventTitle}>
              <View style={[styles.eventDate, {flex:5}]}>
                <View style={[styles.eventIcon, {flex:.5}]}>
                  <Icon name="calendar" size={14} color={myConstants.PURPLE} />
                </View>
                <View style={styles.eventData}>
                  <Text style={styles.eventText}>{rowData.date} {rowData.time}</Text>
                </View>
              </View>
              <View style={[styles.eventDate, {flex:2}]}>
                <View style={[styles.eventIcon, {flex:2}]}>
                  <Icon name="eye" size={14} color={myConstants.PURPLE} />
                </View>
                <View style={styles.eventData}>
                  <Text style={styles.eventText}>{rowData.view}</Text>
                </View>
              </View>
            </View>
          </View>
        </TouchableOpacity>
      )
  }

  goToBack(){
    Actions.home();
  }

  render() {
    return (
      <View style={styles.eventsContainer}>
        <TopStatusBar/>
        <View style={styles.listHeader}>
          <TouchableOpacity style={styles.listHeaderButton} activeOpacity={0.9} onPress={this.goToBack.bind(this)}>
            <Icon name="chevron-left" style={styles.listHeaderButtonIcon} />
          </TouchableOpacity>
          <View style={styles.listHeaderTitle}>
            <Text style={styles.listHeaderTitleText}>Events</Text>
          </View>
        </View>
        <View style={{flex:1, flexDirection:'row', justifyContent:'center'}}>
          {
            this.state.loading
            ?
            <View style={{marginTop:20}}><ActivityIndicator size="large" color={myConstants.PURPLE} /></View>
            :
            (
              this.state.errorMsg!=''
              ?
              <View style={{alignItems:'center', justifyContent:'center', marginTop:20}}><Text style={{fontSize:myConstants.FONTSIZE18, fontFamily:myConstants.FONTFAMILY, color:myConstants.BLACK}}>{this.state.errorMsg}</Text></View>
              :
              <View style={{flex:1, flexDirection:'row'}}>
                <ScrollView>
                  <ListView dataSource={this.state.dataSource} renderRow={this._renderRow.bind(this)} />
                </ScrollView>
              </View>
            )
          }
        </View>
      </View>
    );
  }

  openSinglEventScreen(id){
    Actions.singleevent({id:id});
  }
}
