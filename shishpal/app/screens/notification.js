import React from 'react';
import { Text, View, StatusBar, BackHandler, TouchableOpacity, ListView, ScrollView, ActivityIndicator, WebView } from 'react-native';

var styles = require('../styles/notificationStyle');
import TopStatusBar from '../extras/statusBar';

import Icon from 'react-native-vector-icons/FontAwesome';

import myConstants from '../constant/constant';

import { Actions } from 'react-native-router-flux';

import Thumbnail from 'react-native-thumbnail-video';

export default class Interview extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      loading:true,
      errorMsg:'',
    }
  }

  componentDidMount(){
    let that = this;
    setTimeout(() => {
      that.setState({loading:false});
      that.setState({errorMsg:'No New Notifications!!!'})
    }, 1000)

    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick.bind(this));
  }

  handleBackButtonClick(){
    Actions.home();
    return true;
  }

  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick.bind(this));
  }

  goToBack(){
    Actions.home();
  }

  render() {
    return (
      <View style={styles.notificationContainer}>
        <TopStatusBar/>
        <View style={styles.listHeader}>
          <TouchableOpacity style={styles.listHeaderButton} activeOpacity={0.9} onPress={this.goToBack.bind(this)}>
            <Icon name="chevron-left" style={styles.listHeaderButtonIcon} />
          </TouchableOpacity>
          <View style={styles.listHeaderTitle}>
            <Text style={styles.listHeaderTitleText}>Notification</Text>
          </View>
        </View>
        <View style={{flex:1, flexDirection:'row', justifyContent:'center'}}>
          <ScrollView>
            <View style={styles.notificationRow}>
              {
                this.state.loading
                ?
                <View style={{marginTop:20}}><ActivityIndicator size="large" color={myConstants.PURPLE} /></View>
                :
                (
                  this.state.errorMsg!=''
                  ?
                  <View style={{alignItems:'center', justifyContent:'center', marginTop:20}}><Text style={{fontSize:myConstants.FONTSIZE18, fontFamily:myConstants.FONTFAMILY, color:myConstants.BLACK}}>{this.state.errorMsg}</Text></View>
                  :
                  null
                )
              }
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }

}
