import React from 'react';
import { Text, View, StatusBar, Image, TouchableOpacity, ListView, ScrollView, ActivityIndicator, BackHandler } from 'react-native';

var styles = require('../../styles/newsStyle');
import TopStatusBar from '../../extras/statusBar';

import Icon from 'react-native-vector-icons/FontAwesome';

import myConstants from '../../constant/constant';

import { Actions } from 'react-native-router-flux';

import Thumbnail from 'react-native-thumbnail-video';

const ds = new ListView.DataSource({rowHasChanged:(row1, row2) => row1 != row2});

export default class News extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      dataSource: ds.cloneWithRows(['row 1', 'row 2']),
      loading:true,
      errorMsg:''
    }
  }

  componentDidMount(){
    fetch(myConstants.USER.API_URL+"/posts/user/"+myConstants.USER.USER_ID)
    .then((response) => response.json())
    .then((data) => {
      this.setState({dataSource:this.state.dataSource.cloneWithRows(data.data)});
      this.setState({loading:false});
    })
    .catch((error) => {
      // console.warn(error);
      this.setState({errorMsg:'No News'});
      this.setState({loading:false});
    })

    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick.bind(this));
  }

  handleBackButtonClick(){
      console.warn(Actions.currentScene);
  }

  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick.bind(this));
  }

  handleBackButtonClick(){
    Actions.home();
    return true;
  }

  _renderRow(rowData){
    let image;
    rowData.newsImage
    ?
    image = {
        uri : myConstants.USER.IMAGE_URL+'/news_photo/'+rowData.newsImage
    }
    :
    image = require('../../../resources/RAJNEETI_LIVE.png')

      return (
        <TouchableOpacity activeOpacity={0.9} style={styles.event} onPress={this.openSinglNewsScreen.bind(this, rowData.news_id)}>
          <View style={styles.eventImageView}>
            {
              rowData.video
              ?
              <Thumbnail url={rowData.video} style={{width: 80, height: 100}} />
              :
              <Image source={image} style={{width: 80, height: 100}} defaultSource={require('../../../resources/RAJNEETI_LIVE.png')}/>
            }

          </View>
          <View style={styles.eventDataView}>
            <View style={styles.eventTitle}>
              <Text style={styles.eventTitleText}>{rowData.title.length>25 ? rowData.title.slice(0,25)+' ...' : rowData.title}</Text>
            </View>
            <View style={styles.eventTitle}>
              <View style={[styles.eventDate, {flex:5}]}>
                <View style={[styles.eventIcon, {flex:.5}]}>
                  <Icon name="calendar" style={styles.buttonsIcon} />
                </View>
                <View style={styles.eventData}>
                  <Text style={styles.eventText}>{rowData.created}</Text>
                </View>
              </View>
            </View>
            <View style={styles.newsDescription}>
              <Text style={styles.newsDescriptionText}>{rowData.news.length>110 ? rowData.news.slice(0,110)+' ...' : rowData.news}</Text>
            </View>
          </View>

        </TouchableOpacity>
      )
  }

  goToBack(){
    Actions.home();
  }

  render() {
    return (
      <View style={styles.eventsContainer}>
        <TopStatusBar/>
        <View style={styles.listHeader}>
          <TouchableOpacity style={styles.listHeaderButton} activeOpacity={0.9} onPress={this.goToBack.bind(this)}>
            <Icon name="chevron-left" style={styles.listHeaderButtonIcon} />
          </TouchableOpacity>
          <View style={styles.listHeaderTitle}>
            <Text style={styles.listHeaderTitleText}>News</Text>
          </View>
        </View>
        <View style={{flex:1, flexDirection:'row', justifyContent:'center'}}>
          {
            this.state.loading
            ?
            <View style={{marginTop:20}}><ActivityIndicator size="large" color={myConstants.PURPLE} /></View>
            :
            (
              this.state.errorMsg!=''
              ?
              <View style={{alignItems:'center', justifyContent:'center', marginTop:20}}><Text style={{fontSize:myConstants.FONTSIZE18, fontFamily:myConstants.FONTFAMILY, color:myConstants.BLACK}}>{this.state.errorMsg}</Text></View>
              :
              <View style={{flex:1, flexDirection:'row'}}>
                <ScrollView>
                  <ListView dataSource={this.state.dataSource} renderRow={this._renderRow.bind(this)} />
                </ScrollView>
              </View>
            )
          }
        </View>
      </View>
    );
  }

  openSinglNewsScreen(id){
    Actions.singlenews({id:id});
  }

}
