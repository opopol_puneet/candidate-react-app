import React from 'react';
import {
  Text,
  View,
  StatusBar,
  Image,
  TouchableOpacity,
  ScrollView,
  ToastAndroid,
  Dimensions,
  WebView,
  Platform,
  ActivityIndicator
} from 'react-native';

var styles = require('../../styles/singleNewsStyle');
import TopStatusBar from '../../extras/statusBar';

import Icon from 'react-native-vector-icons/FontAwesome';

import myConstants from '../../constant/constant';

import { Actions } from 'react-native-router-flux';

export default class singleNews extends React.Component {

  state = {
    loading:true,
    isReady: false,
    status: null,
    quality: null,
    error: null,
    isPlaying: true,
    isLooping: false,
    duration: 0,
    currentTime: 0,
    fullscreen: false,
    containerMounted: false,
    containerWidth: null,
  };

  componentDidMount(){
    fetch(myConstants.USER.API_URL+"/single/news/user/"+this.props.id)
    .then((response) => response.json())
    .then((data) => {
      this.setState(data.data);
      this.setState({loading:false});
    })
    .catch((error) => {
      this.setState({errorMsg:'No News'});
      this.setState({loading:false});
    })
  }

  loadingTillVideoLoad(){
    return (
        <ActivityIndicator
           animating = {this.state.visible}
           color = {myConstants.PURPLE}
           size = "large"
           style = {{flex:1}}
           hidesWhenStopped={true}
        />
    );
  }

  goToBack(){
    Actions.news();
  }

  render() {
    let uri = myConstants.USER.IMAGE_URL+'/news_photo/default.jpg';
    let url = myConstants.USER.IMAGE_URL+'/news_photo';
    if(this.state.newsImage)
    {
      uri = myConstants.USER.IMAGE_URL+'/news_photo/'+this.state.newsImage;
    }
    if(this.state.loading) {
      return <View style={{marginTop:20}}><ActivityIndicator size="large" color={myConstants.PURPLE} /></View>;
    }

    return (
      <View style={styles.singleNewsContainer}>
        <TopStatusBar/>
        <View style={styles.listHeader}>
          <TouchableOpacity style={styles.listHeaderButton} activeOpacity={0.9} onPress={this.goToBack.bind(this)}>
            <Icon name="chevron-left" style={styles.listHeaderButtonIcon} />
          </TouchableOpacity>
          <View style={styles.listHeaderTitle}>
            <Text style={styles.listHeaderTitleText}>{this.state.title.length<30 ? this.state.title : this.state.title.substring(0,25)+'...'}</Text>
          </View>
        </View>
        <View style={{flex:1, flexDirection:'row'}}>
          <ScrollView>
            <View style={styles.singleNews}>
              <View style={styles.singleNewsTitle}>
                <Text style={styles.singleNewsTitleText}>{this.state.title}</Text>
              </View>
              <View style={styles.singleNewsDescription}>
                <Text style={styles.singleNewsDescriptionText}>{this.state.news}</Text>
              </View>
              <View style={styles.singleNewsMeta}>
                {
                  this.state.video_id
                  ?
                  <View style={{flex:1, height:300, flexDirection:'row'}}>
                    <WebView startInLoadingState={true} renderLoading={this.loadingTillVideoLoad.bind(this)} javaScriptEnabled={true} domStorageEnabled={true} source={{uri : this.state.video_id}} />
                  </View>
                  :
                  (
                    this.state.newsImage
                    ?
                    <View style={styles.imageBox}>
                      <Image source={{uri : uri}} style={{width:310, height:310, resizeMode:'contain', flex:1, borderRadius:5}} />
                    </View>
                    :
                    null
                  )
                }
              </View>
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }
}
