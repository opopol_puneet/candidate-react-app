import React from 'react';
import {
  Text,
  View,
  StatusBar,
  Image,
  TouchableOpacity,
  ToastAndroid,
  ScrollView,
  ActivityIndicator,
  Switch,
  TextInput,
  Modal,
  AsyncStorage,
  TouchableHighlight,
  NetInfo,
  BackHandler
} from 'react-native';

var styles = require('../styles/loginStyle');
import TopStatusBar from '../extras/statusBar';

import Icon from 'react-native-vector-icons/FontAwesome';

import myConstants from '../constant/constant';

import { Actions, ActionConst } from 'react-native-router-flux';

import Spinner from 'react-native-loading-spinner-overlay';

export default class Interview extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      loading:false,
      errorMsg:'',
      remember:true,
      forgetPasswordModal:false,
      mobilePattern:/^[6789]\d{9}$/,
      forgerPasswordSubmit:true,
      isUsernameOk:false,
      isPasswordOk:false,
      btnDisable:true,
      username:'',
      password:''
    }
  }

  componentDidMount(){
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick.bind(this));
  }

  handleBackButtonClick(){
      Actions.home();
      return true;
  }

  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick.bind(this));
  }

  closeModal(){
    this.setState({forgetPasswordModal:false});
  }

  submitModal(){
    <View style={styles.forgetPasswordBox}>
      <TouchableOpacity activeOpacity={0.9} style={styles.forgetPasswordButton} onPress={this.openForgetPasswordModal.bind(this)}>
        <Text style={styles.forgetPasswordButtonText}>Forget Password</Text>
      </TouchableOpacity>
    </View>
  }

  forgetMobileChange(mobile){
    this.setState({forgetMobile:mobile});
    if(this.state.mobilePattern.test(mobile)){
      this.setState({forgerPasswordSubmit:false});
    }
    else {
      this.setState({forgerPasswordSubmit:true});
    }
  }

  goToBack(){
    Actions.home();
  }

  render() {
    return (
      <View style={styles.loginContainer}>
        <TopStatusBar/>
        <Spinner visible={this.state.loading} color={myConstants.PURPLE} overlayColor={myConstants.PURPLERBGA} />
        <Modal
          onShow={() => { this.textInput.focus() }}
          style={{ zIndex: -10 }}
          visible={this.state.forgetPasswordModal}
          animationType={'slide'}
          transparent={true}
          onRequestClose={() => this.closeModal()}
        >
          <View style={styles.modalContainer}>
            <View style={styles.innerContainer}>
              <View style={styles.modalHeader}>
                <Text style={styles.modalHeaderText}>Forget Password</Text>
              </View>
              <View style={styles.modalHeaderSubTitle}>
                <Text style={styles.modalHeaderSubTitleText}>Enter Registered Mobile Number</Text>
              </View>
              <View style={styles.modalBox}>
                <View style={styles.modalBoxInputBox}>
                  <TextInput underlineColorAndroid='transparent'
                    keyboardType = 'numeric'
                    onChangeText={this.forgetMobileChange.bind(this)}
                    ref={(input) => { this.textInput = input }}
                    style={styles.modalBoxInputBoxInput}
                    value={this.state.forgetMobile}
                    maxLength={10}
                    placeholder='Registered Mobile Number'
                  />
                </View>
                <View style={styles.modalButtons}>
                  <View style={styles.modalButtonLeft}>
                  <TouchableHighlight underlayColor='rgba(107, 147, 75, 0.8)' style={styles.modalButtonLeftButton} onPress={this.closeModal.bind(this)}>
                      <Text style={styles.modalButtonLeftButtonText}>Cancel</Text>
                    </TouchableHighlight>
                  </View>
                  <View style={styles.modalButtonRight}>
                  <TouchableHighlight underlayColor='rgba(107, 147, 75, 0.8)' style={[styles.modalButtonRightButton, {opacity:this.state.forgerPasswordSubmit?0.4:1}]} disabled={this.state.forgerPasswordSubmit}  onPress={this.submitModal.bind(this)}>
                      <Text style={styles.modalButtonRightButtonText}>Submit</Text>
                    </TouchableHighlight>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </Modal>
        <View style={styles.listHeader}>
          <TouchableOpacity style={styles.listHeaderButton} activeOpacity={0.9} onPress={this.goToBack.bind(this)}>
            <Icon name="chevron-left" style={styles.listHeaderButtonIcon} />
          </TouchableOpacity>
          <View style={styles.listHeaderTitle}>
            <Text style={styles.listHeaderTitleText}>Login</Text>
          </View>
        </View>
        <View style={{flex:1, flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
          <ScrollView>
            <View style={styles.loginBox}>
              <View style={styles.loginHead}>
                <Image source={myConstants.RESOURCES.RAJNEETI_LIVE} style={{width: 70, height: 70}} />
              </View>
              <View style={styles.loginContent}>
                <View style={styles.loginContentRow}>
                  <TextInput underlineColorAndroid='transparent'
                    keyboardType = 'numeric'
                    style={styles.loginContentRowInput}
                    value={this.state.username}
                    maxLength={10}
                    onSubmitEditing={(event) => { this.refs.password.focus()}}
                    onChangeText={this.usernameChange.bind(this)}
                    placeholder={'Mobile Number'}
                  />
                </View>
                <View style={styles.loginContentRow}>
                  <TextInput underlineColorAndroid='transparent'
                    ref='password'
                    style={styles.loginContentRowInput}
                    value={this.state.password}
                    placeholder={'Password'}
                    onChangeText={this.passwordChange.bind(this)}
                    secureTextEntry={true}
                  />
                </View>
                <View style={styles.loginContentRow}>
                  <View style={styles.loginContentSwitch}>
                    <Switch
                        style={{transform: [{scaleX: 1.5}, {scaleY: 1.5}]}}
                        onTintColor={myConstants.PURPLE}
                        thumbTintColor="white"
                        onValueChange={(value) => this.setState({remember:value})}
                        tintColor={myConstants.LIGHTGRAY}
                        value={this.state.remember}
                    />
                  </View>
                </View>
                <View style={styles.loginContentRow}>
                  <TouchableHighlight underlayColor='rgba(138, 71, 221, 0.8)' style={[styles.loginButton, {opacity:this.state.btnDisable?0.4:1}]} onPress={this.doLogin.bind(this)} disabled={this.state.btnDisable}>
                    <View style={{flex:1, flexDirection:"row"}}>
                      <Text style={styles.loginButtonText}>Login</Text>
                      <Icon name="long-arrow-right" style={styles.loginButtonIcon} />
                    </View>
                  </TouchableHighlight>
                </View>
              </View>

            </View>
          </ScrollView>
        </View>
      </View>
    );
  }

  openForgetPasswordModal(){
    this.setState({forgetPasswordModal:true});
  }

  usernameChange(value){
    this.setState({username:value});
    if(this.state.mobilePattern.test(value)){
      this.setState({isUsernameOk:true});
      if(this.state.isPasswordOk){
        this.setState({btnDisable:false})
      }
      else{
        this.setState({btnDisable:true})
      }
    }
    else{
      this.setState({isUsernameOk:false});
      this.setState({btnDisable:true})
    }
  }

  passwordChange(value){
    this.setState({password:value});
    if(value.length>2){
      this.setState({isPasswordOk:true});
      if(this.state.isUsernameOk){
        this.setState({btnDisable:false})
      }
      else{
        this.setState({btnDisable:true})
      }
    }
    else{
      this.setState({isPasswordOk:false});
      this.setState({btnDisable:true})
    }
  }

  doLogin(){
    NetInfo.isConnected.fetch().then(isConnected => {
      if(!isConnected){
        ToastAndroid.show('No Internet Connection', ToastAndroid.SHORT);
        return false;
      }
    });
    this.setState({loading:true});
    fetch(myConstants.USER.SOFT_API_URL+"/jwtLogin", {
      method : 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body:JSON.stringify({
        username : this.state.username,
        password : this.state.password
      })
    })
    .then((response) => response.json())
    .then((responseData) => {
      this.setState({loading:false});
      if(responseData.error){
        ToastAndroid.show(responseData.errorMsg, ToastAndroid.SHORT);
      }
      else {
        AsyncStorage.setItem('softUser', JSON.stringify(responseData.data.user));
        AsyncStorage.setItem('token', JSON.stringify(responseData.data.token));
        AsyncStorage.setItem('role', JSON.stringify(responseData.data.role));
        AsyncStorage.setItem('authId', JSON.stringify(responseData.data.authId));
        Actions.tabs();
      }
    })
    .catch((error) => {
      this.setState({loading:false});
    })
  }
}
