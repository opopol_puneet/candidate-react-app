import React from 'react';
import { Text, View, StatusBar, Image, TouchableOpacity, ListView, ScrollView, TextInput, TouchableHighlight, AsyncStorage, Modal, ToastAndroid, NetInfo, BackHandler } from 'react-native';

var styles = require('../styles/writeMeStyle');
import TopStatusBar from '../extras/statusBar';

import Icon from 'react-native-vector-icons/FontAwesome';

import Spinner from 'react-native-loading-spinner-overlay';

import myConstants from '../constant/constant';

import { Actions } from 'react-native-router-flux';

import MyText from 'react-native-letter-spacing';

export default class WriteMe extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      mobilePattern:/^[6789]\d{9}$/,
      isNameVerified:true,
      isMobileVerified:false,
      isMessageVerified:true,
      btnDisable:true,
      otpBtnDisable:true,
      otpModal:false,
      id:'',
      loading:false,
      token_id:'',
      token:'',
      name:'',
      mobile:'',
      message:'',
      mobiles:[]
    }
  }

  componentDidMount(){
    AsyncStorage.getItem('mobiles').then((data) => {
    })

    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick.bind(this));
  }

  handleBackButtonClick(){
      Actions.home();
      return true;
  }

  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick.bind(this));
  }

  goToBack(){
    Actions.home();
  }


  render() {
    return (
      <View style={styles.writeMeContainer}>
        <TopStatusBar/>
        <Spinner visible={this.state.loading} color='#6B934B' overlayColor='rgba(107, 147, 75, 0.5)' />
        <Modal
          onShow={() => { this.textInput.focus() }}
          style={{ zIndex: -10 }}
          visible={this.state.otpModal}
          animationType={'slide'}
          transparent={true}
          onRequestClose={() => this.closeModal()}
        >
          <View style={styles.modalContainer}>
            <View style={styles.innerContainer}>
              <View style={styles.modalHeader}>
                <Text style={styles.modalHeaderText}>Enter OTP</Text>
              </View>
              <View style={styles.modalHeaderSubTitle}>
                <Text style={styles.modalHeaderSubTitleText}>6 digits OTP received on your mobile</Text>
              </View>
              <View style={styles.modalBox}>
                <View style={styles.modalBoxInputBox}>
                  <TextInput underlineColorAndroid='transparent'
                    keyboardType = 'numeric'
                    ref={(input) => { this.textInput = input }}
                    onChangeText={this.fillEnteredToken.bind(this)}
                    style={styles.modalBoxInputBoxInput}
                    value={this.state.enteredToken}
                    maxLength={6}
                  />
                </View>
                <View style={styles.modalButtons}>
                  <View style={styles.modalButtonLeft}>
                  <TouchableHighlight underlayColor='rgba(138, 71, 221, 0.8)' style={styles.modalButtonLeftButton} onPress={this.cancelModal.bind(this)}>
                      <Text style={styles.modalButtonLeftButtonText}>Cancel</Text>
                    </TouchableHighlight>
                  </View>
                  <View style={styles.modalButtonRight}>
                  <TouchableHighlight underlayColor='rgba(138, 71, 221, 0.8)' style={[styles.modalButtonRightButton, {opacity:this.state.otpBtnDisable?0.4:1}]} disabled={this.state.otpBtnDisable}  onPress={this.submitModal.bind(this)}>
                      <Text style={styles.modalButtonRightButtonText}>Submit</Text>
                    </TouchableHighlight>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </Modal>
        <View style={styles.listHeader}>
          <TouchableOpacity style={styles.listHeaderButton} activeOpacity={0.9} onPress={this.goToBack.bind(this)}>
            <Icon name="chevron-left" style={styles.listHeaderButtonIcon} />
          </TouchableOpacity>
          <View style={styles.listHeaderTitle}>
            <Text style={styles.listHeaderTitleText}>Suggestion</Text>
          </View>
        </View>
        <View style={{flex:1, flexDirection:'row', justifyContent:'center',paddingLeft:10, paddingRight:10, paddingTop:10}}>
          <ScrollView>
            <View style={styles.writeMeForm}>
              <View style={styles.writeMeRow}>
                <View style={styles.writeMeLabel}>
                  <Text style={styles.writeMeLabelText}>Name</Text>
                </View>
                <View style={styles.writeMeInput}>
                  <TextInput underlineColorAndroid='transparent'
                    onChangeText={this.nameChange.bind(this)}
                    onSubmitEditing={(event) => { this.refs.mobile.focus()}}
                    style={styles.writeMeInputBox}
                    value={this.state.name}
                    maxLength={40}
                  />
                </View>
              </View>
              <View style={styles.writeMeRow}>
                <View style={styles.writeMeLabel}>
                  <Text style={styles.writeMeLabelText}>Mobile</Text>
                </View>
                <View style={styles.writeMeInput}>
                  <TextInput underlineColorAndroid='transparent'
                    ref='mobile'
                    keyboardType = 'numeric'
                    onChangeText={this.mobileChange.bind(this)}
                    onSubmitEditing={(event) => { this.refs.message.focus()}}
                    style={styles.writeMeInputBox}
                    value={this.state.mobile}
                    maxLength={10}
                  />
                </View>
              </View>
              <View style={styles.writeMeRow}>
                <View style={styles.writeMeLabel}>
                  <Text style={styles.writeMeLabelText}>Message</Text>
                </View>
                <View style={styles.writeMeInput}>
                  <TextInput underlineColorAndroid='transparent'
                    ref='message'
                    multiline={true}
                    numberOfLines={10}
                    onChangeText={this.messageChange.bind(this)}
                    style={[styles.writeMeInputBox, {height:120, textAlignVertical: "top"}]}
                    value={this.state.message}
                    maxLength={500}
                  />
                </View>
              </View>
              <View style={styles.writeMeRow}>
                <TouchableHighlight underlayColor='rgba(138, 71, 221, 0.8)' style={[styles.submitWriteMe, {opacity:this.state.btnDisable?0.4:1}]} disabled={this.state.btnDisable} onPress={this.postWriteMe.bind(this)}>
                  <Text style={styles.submitWriteMeText}>Submit</Text>
                </TouchableHighlight>
              </View>
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }

  fillEnteredToken(val){
    this.setState({enteredToken:val});
    if(isNaN(val)===false && val.length===6){
      this.setState({otpBtnDisable:false})
    }
    else {
      this.setState({otpBtnDisable:true})
    }
  }

  closeModal(){
    return false;
  }

  cancelModal(){
    this.setState({otpModal:false});
  }

  nameChange(name){
    this.setState({name:name});
    if(name.length>2){
      this.setState({isNameVerified:true});
      if(this.state.isMobileVerified && this.state.isMessageVerified){
        this.setState({btnDisable:false});
      }
      else {
        this.setState({btnDisable:true});
      }
    }
    else {
      this.setState({isNameVerified:false});
      this.setState({btnDisable:true});
    }


  }

  mobileChange(mobile){
    this.setState({mobile:mobile});
    if(this.state.mobilePattern.test(mobile)){
      this.setState({isMobileVerified:true});
      if(this.state.isNameVerified && this.state.isMessageVerified){
        this.setState({btnDisable:false});
      }
      else {
        this.setState({btnDisable:true});
      }
    }
    else {
      this.setState({isMobileVerified:false});
      this.setState({btnDisable:true});
    }
  }

  messageChange(message){
    this.setState({message:message});
    if(message.length>9)
    {
      this.setState({isMessageVerified:true});
      if(this.state.isNameVerified && this.state.isMobileVerified){
        this.setState({btnDisable:false});
      }
      else {
        this.setState({btnDisable:true});
      }
    }
    else {
      this.setState({isMessageVerified:false});
      this.setState({btnDisable:true});
    }
  }

  postWriteMe(){
    var sendToken=1;
    NetInfo.isConnected.fetch().then(isConnected => {
      if(!isConnected){
        ToastAndroid.show('No Internet Connection', ToastAndroid.SHORT);
        return false;
      }
    });
    this.setState({loading:true});
    AsyncStorage.getItem('user').then((data) => {
      data = JSON.parse(data);
      AsyncStorage.getItem('mobiles').then((mobiles) => {
        if(mobiles)
        {
          mobiles = JSON.parse(mobiles);
          if(mobiles.indexOf(this.state.mobile) > -1){
            sendToken=0
          }
        }
        fetch(myConstants.USER.API_URL+"/write/"+data.user_id, {
          method : 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
          body:JSON.stringify({
            name : this.state.name,
            mobile : this.state.mobile,
            msg : this.state.message,
            assembly : data.assemblyID,
            sendToken : sendToken
          })
        })
        .then((response) => response.json())
        .then((responseData) => {
          this.setState({loading:false});
          if(responseData.sendToken===0)
          {
            this.refreshStates();
            this.setState({mobile:''})
            ToastAndroid.show(responseData.data, ToastAndroid.SHORT);
          }
          else {
            this.setState({id:responseData.id});
            this.setState({token_id:responseData.token_id});
            this.setState({token:responseData.token});
            this.setState({otpModal:true});
          }
        })
        .catch((error) => {
          this.setState({loading:false});
        })
      })
    })
  }

  submitModal(val){
    this.setState({otpModal:false});
    this.setState({loading:true});
    if(this.state.enteredToken==this.state.token){
      fetch(myConstants.USER.API_URL+"/writeme/verify", {
        method : 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body:JSON.stringify({
          id : this.state.id,
          token_id : this.state.token_id,
          token : this.state.token,
        })
      })
      .then((response) => response.json())
      .then((responseData) => {
        this.refreshStates();
        AsyncStorage.getItem('mobiles').then((data) => {
          if(data){
            data = JSON.parse(data);
            if(data.indexOf(this.state.mobile) === -1){
              data.push(this.state.mobile);
              AsyncStorage.setItem('mobiles', JSON.stringify(data));
            }
            this.setState({mobiles:data});
            this.setState({mobile:''})
          }
          else {
            var arr = [this.state.mobile]
            AsyncStorage.setItem('mobiles', JSON.stringify(arr)).then(() => {
              this.setState({mobiles:arr});
              this.setState({mobile:''});
            })
          }
        })
        ToastAndroid.show(responseData.data, ToastAndroid.SHORT);
        Actions.refresh();
      })
      .catch((error) => {
        this.setState({otpModal:true});
        this.setState({loading:false});
      })
    }
    else {
      this.setState({loading:false, otpModal:true});
      ToastAndroid.show('OTP not matched!', ToastAndroid.SHORT);
    }
  }
  refreshStates(){
    this.setState({loading:false, btnDisable:true});
    this.setState({otpModal:false});
    this.setState({name:''});
    this.setState({message:''});
    this.setState({enteredToken:''});
    this.setState({id:''});
    this.setState({token_id:''});
    this.setState({token:''});
  }
}
