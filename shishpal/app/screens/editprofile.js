import React from 'react';
import {
  Text,
  View,
  StatusBar,
  Image,
  TouchableOpacity,
  ListView,
  ScrollView,
  TextInput,
  TouchableHighlight,
  AsyncStorage,
  Modal,
  ToastAndroid,
  DatePickerAndroid,
  Switch,
  Picker
} from 'react-native';

var styles = require('../styles/editProfileStyle');
import TopStatusBar from '../extras/statusBar';

import Icon from 'react-native-vector-icons/FontAwesome';

import Spinner from 'react-native-loading-spinner-overlay';

import myConstants from '../constant/constant';

import { Actions } from 'react-native-router-flux';

export default class EditProfile extends React.Component {

  constructor(props){
    super(props);
    this.state = {
    }
  }

  componentDidMount(){
    fetch(myConstants.USER.API_URL+"/assembly?state="+myConstants.USER.STATE_ID+"&parliamentary=", {
      method:'GET'
    }).then((response) => response.json())
    .then((responseData) =>   {
      this.setState({assemblyData:JSON.stringify(responseData.assembly)});
    })
    .catch((error) => {
      alert(error)
    })
  }

  render() {
    return (
      <View style={styles.writeMeContainer}>
          <TopStatusBar/>
        <Spinner visible={this.state.loading} color='#6B934B' overlayColor='rgba(107, 147, 75, 0.5)' />
        <ScrollView>
          <View style={styles.writeMeForm}>
            <View style={styles.writeMeRow}>
              <View style={styles.writeMeLabel}>
                <Text style={styles.writeMeLabelText}>Name</Text>
              </View>
              <View style={styles.writeMeInput}>
                <TextInput underlineColorAndroid='transparent'
                  onChangeText={this.nameChange.bind(this)}
                  onSubmitEditing={(event) => { this.refs.mobile.focus()}}
                  style={styles.writeMeInputBox}
                  value={this.state.name}
                  maxLength={40}
                />
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }

}
