import React from 'react';
import { Text, View, StatusBar, Image, TouchableOpacity, ListView, ScrollView, ActivityIndicator, WebView, BackHandler } from 'react-native';

var styles = require('../styles/interviewStyle');
import TopStatusBar from '../extras/statusBar';

import Icon from 'react-native-vector-icons/FontAwesome';

import myConstants from '../constant/constant';

import { Actions } from 'react-native-router-flux';

import Thumbnail from 'react-native-thumbnail-video';

const ds = new ListView.DataSource({rowHasChanged:(row1, row2) => row1 != row2});

export default class Interview extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      dataSource: ds.cloneWithRows(['row 1', 'row 2']),
      loading:true,
      errorMsg:'',
      isReady: false,
      status: null,
      quality: null,
      error: null,
      isPlaying: false,
      isLooping: false,
      duration: 0,
      currentTime: 0,
      fullscreen: false,
      containerMounted: false,
      containerWidth: null,
    }
  }

  componentDidMount(){
    fetch(myConstants.USER.API_URL+"/candidate/interview/"+myConstants.USER.USER_ID)
    .then((response) => response.json())
    .then((data) => {
      this.setState({video:data.data[0].video})
      this.setState({dataSource:this.state.dataSource.cloneWithRows(data.data)});
      this.setState({loading:false});
    })
    .catch((error) => {
      this.setState({errorMsg:'No Interviews'});
      this.setState({loading:false});
    })

    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick.bind(this));
  }

  handleBackButtonClick(){
      Actions.home();
      return true;
  }

  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick.bind(this));
  }

  loadingTillVideoLoad(){
    return (
        <ActivityIndicator
           animating = {this.state.visible}
           color = {myConstants.PURPLE}
           size = "large"
           style = {{flex:1}}
           hidesWhenStopped={true}
        />
    );
  }

  _renderRow(rowData){
      return (

          <View style={styles.interview}>
            <View style={styles.interviewThumbnail}>
              <WebView  startInLoadingState={true} renderLoading={this.loadingTillVideoLoad.bind(this)} javaScriptEnabled={true} domStorageEnabled={true} source={{uri: rowData.video_id }} />
            </View>
            <View style={styles.interviewHeading}>
              <View style={styles.interviewHeadingTitle}>
                <Text style={styles.interviewHeadingTitleText}>{rowData.title}</Text>
              </View>
            </View>
          </View>

      )
  }

  goToBack(){
    Actions.home();
  }

  render() {

    return (
      <View style={styles.interviewContainer}>
        <TopStatusBar/>
        <View style={styles.listHeader}>
          <TouchableOpacity style={styles.listHeaderButton} activeOpacity={0.9} onPress={this.goToBack.bind(this)}>
            <Icon name="chevron-left" style={styles.listHeaderButtonIcon} />
          </TouchableOpacity>
          <View style={styles.listHeaderTitle}>
            <Text style={styles.listHeaderTitleText}>Interview</Text>
          </View>
        </View>
        <View style={{flex:1, flexDirection:'row', justifyContent:'center'}}>
          {
            this.state.loading
            ?
            <View style={{marginTop:20}}><ActivityIndicator size="large" color={myConstants.PURPLE} /></View>
            :
            (
              this.state.errorMsg!=''
              ?
              <View style={{marginTop:20}}><Text style={{fontSize:myConstants.FONTSIZE18, fontFamily:myConstants.FONTFAMILY, color:myConstants.BLACK}}>{this.state.errorMsg}</Text></View>
              :
              <View style={{flex:1, flexDirection:'row'}}>
                <ScrollView>
                  <View style={styles.interviewFull}>
                    <ListView dataSource={this.state.dataSource} renderRow={this._renderRow.bind(this)} />
                  </View>
                </ScrollView>
              </View>
            )
          }
        </View>
      </View>
    );
  }

}
