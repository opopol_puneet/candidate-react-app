import React from 'react';
import { Text, View, StatusBar, Image, TouchableOpacity, ScrollView, Linking, AsyncStorage, BackHandler } from 'react-native';

var styles = require('../styles/profileStyle');
import TopStatusBar from '../extras/statusBar';

import Icon from 'react-native-vector-icons/FontAwesome';

import myConstants from '../constant/constant';

import { Actions } from 'react-native-router-flux';

import MyText from 'react-native-letter-spacing';

export default class Profile extends React.Component {

  constructor(props){
    super(props);
    this.state = {
    }
  }

  componentDidMount(){
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick.bind(this));
  }

  componentWillMount(){
    AsyncStorage.getItem('user').then((data) => {
      if(data){
        data = JSON.parse(data);
        this.setState(data);
      }
      else {
        fetch(myConstants.USER.API_URL+"/candidate/profile/"+myConstants.USER.USER_ID)
        .then((response) => response.json())
        .then((data) => {
          this.setState(data.profile);
        })
      }
    })
  }

  handleBackButtonClick(){
    // console.warn(Actions.currentScene);
      Actions.home();
      return true;
  }

  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick.bind(this));
  }

  formatDate(){
      var date = new Date(this.state.dob)
      var month = date.getMonth()+1;
      var year = date.getFullYear();
      var day = date.getDate();
      return (day<10?"0"+day:day)+"/"+(month<10?"0"+month:month)+"/"+year;
  }

  goToBack(){
    Actions.home();
  }

  render() {
    return (
      <View style={styles.profileContainer}>
        <TopStatusBar/>
        <View style={styles.listHeader}>
          <TouchableOpacity style={styles.listHeaderButton} activeOpacity={0.9} onPress={this.goToBack.bind(this)}>
            <Icon name="chevron-left" style={styles.listHeaderButtonIcon} />
          </TouchableOpacity>
          <View style={styles.listHeaderTitle}>
            <Text style={styles.listHeaderTitleText}>{myConstants.USER.CANDIDATE_NAME}</Text>
          </View>
        </View>
        <View style={{flex:1, flexDirection:'row', justifyContent:'center'}}>
          <ScrollView>
            <View style={styles.profileMain}>
              <View style={styles.imageBoxView}>
                <Image source={myConstants.USER.PARTY_BANNER} style={styles.imageBox} />
              </View>
              <View style={styles.profileImageBoxContainer}>
                  <View style={styles.profileImageBox}>
                    <Image source={myConstants.USER.CANDIDATE_IMAGE} style={styles.profileImageBoxImage} />
                    <View style={styles.profileImageBoxPartyImage}>
                      <Image source={myConstants.USER.PARTY_LOGO} style={styles.profileImageBoxPartyImageImage} />
                    </View>
                  </View>
                  <View style={styles.profileBoxContent}>
                    <Text style={styles.profileBoxCandidateName}>{myConstants.USER.CANDIDATE_NAME}</Text>
                    <Text style={styles.profileBoxAssemblyName}>{myConstants.USER.ASSEMBLY}</Text>
                  </View>
              </View>
            </View>
            <View style={styles.profileContent}>
              <TouchableOpacity style={styles.profileContentBox} activeOpacity={0.9} onPress={this.openDialer.bind(this)}>
                <View style={styles.profileIcon}>
                  <View style={styles.profileIconBox}>
                    <Icon name="mobile" color={myConstants.PURPLE} size={18}></Icon>
                  </View>
                </View>
                <View style={styles.profileHeadName}>
                  <Text style={styles.profileHeadNameText}>Contact</Text>
                </View>
                <View style={styles.profileBodyName}>
                  <Text style={styles.profileHeadNameText}>{this.state.mobile ? this.state.mobile : 'N/A'}</Text>
                </View>
              </TouchableOpacity>
              <View style={styles.profileContentBox}>
                <View style={styles.profileIcon}>
                  <View style={styles.profileIconBox}>
                    <Icon name="envelope" color={myConstants.ORANGE} size={myConstants.PROFILEICONSIZE2}></Icon>
                  </View>
                </View>
                <View style={styles.profileHeadName}>
                  <Text style={styles.profileHeadNameText}>Email</Text>
                </View>
                <View style={styles.profileBodyName}>
                  <Text style={styles.profileHeadNameText}>{this.state.email ? this.state.email : 'N/A'}</Text>
                </View>
              </View>
              <View style={styles.profileContentBox}>
                <View style={styles.profileIcon}>
                  <Icon name="calendar" color={myConstants.COLOR1} size={myConstants.PROFILEICONSIZE2}></Icon>
                </View>
                <View style={styles.profileHeadName}>
                  <Text style={styles.profileHeadNameText}>DOB</Text>
                </View>
                <View style={styles.profileBodyName}>
                  <Text style={styles.profileHeadNameText}>{this.state.dob ? this.formatDate() : 'N/A'}</Text>
                </View>
              </View>
              <TouchableOpacity activeOpacity={0.9} style={styles.profileContentBox} onPress={this.openWebsite.bind(this)}>
                <View style={styles.profileIcon}>
                  <Icon name="globe" color={myConstants.COLOR2} size={myConstants.PROFILEICONSIZE}></Icon>
                </View>
                <View style={styles.profileHeadName}>
                  <Text style={styles.profileHeadNameText}>Website</Text>
                </View>
                <View style={styles.profileBodyName}>
                  <Text style={styles.profileHeadNameText}>{this.state.website ? this.state.website : 'N/A'}</Text>
                </View>
              </TouchableOpacity>
              <View style={styles.profileContentBox}>
                <View style={styles.profileIcon}>
                  <Icon name="cube" color={myConstants.COLOR3} size={myConstants.PROFILEICONSIZE2}></Icon>
                </View>
                <View style={styles.profileHeadName}>
                  <Text style={styles.profileHeadNameText}>Service Career</Text>
                </View>
                <View style={styles.profileBodyName}>
                  <Text style={[styles.profileHeadNameText, styles.adjustText]}>{this.state.services ? this.state.services : 'N/A'}</Text>
                </View>
              </View>
              <View style={styles.profileContentBox}>
                <View style={styles.profileIcon}>
                  <Icon name="life-saver" color={myConstants.COLOR7} size={myConstants.PROFILEICONSIZE2}></Icon>
                </View>
                <View style={styles.profileHeadName}>
                  <Text style={styles.profileHeadNameText}>Political Career</Text>
                </View>
                <View style={styles.profileBodyName}>
                  <Text style={[styles.profileHeadNameText, styles.adjustText]}>{this.state.political ? this.state.political : 'N/A'}</Text>
                </View>
              </View>
              <View style={styles.profileContentBox}>
                <View style={styles.profileIcon}>
                  <Icon name="cog" color={myConstants.COLOR6} size={myConstants.PROFILEICONSIZE}></Icon>
                </View>
                <View style={styles.profileHeadName}>
                  <Text style={styles.profileHeadNameText}>Present Post</Text>
                </View>
                <View style={styles.profileBodyName}>
                  <Text style={styles.profileHeadNameText}>{this.state.posts ? this.state.posts : 'N/A'}</Text>
                </View>
              </View>
              <TouchableOpacity activeOpacity={0.9} style={styles.profileContentBox} onPress={this.openFacebook.bind(this)}>
                <View style={styles.profileIcon}>
                  <Icon name="facebook" color={myConstants.COLOR4} size={myConstants.PROFILEICONSIZE}></Icon>
                </View>
                <View style={styles.profileHeadName}>
                  <Text style={styles.profileHeadNameText}>Facebook</Text>
                </View>
                <View style={styles.profileBodyName}>
                  <Text style={styles.profileHeadNameText}>{this.state.facebook ? this.state.facebook : 'N/A'}</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={styles.profileContentBox} activeOpacity={0.9} onPress={this.openTwitter.bind(this)}>
                <View style={styles.profileIcon}>
                  <Icon name="twitter" color={myConstants.COLOR5} size={myConstants.PROFILEICONSIZE}></Icon>
                </View>
                <View style={styles.profileHeadName}>
                  <Text style={styles.profileHeadNameText}>Twitter</Text>
                </View>
                <View style={styles.profileBodyName}>
                  <Text style={styles.profileHeadNameText}>{this.state.twitter ? this.state.twitter : 'N/A'}</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={styles.profileContentBox} activeOpacity={0.9} onPress={this.openInstagram.bind(this)}>
                <View style={styles.profileIcon}>
                  <Icon name="instagram" color={myConstants.INSTAGRAM} size={myConstants.PROFILEICONSIZE}></Icon>
                </View>
                <View style={styles.profileHeadName}>
                  <Text style={styles.profileHeadNameText}>Instagram</Text>
                </View>
                <View style={styles.profileBodyName}>
                  <Text style={styles.profileHeadNameText}>{this.state.instagram ? this.state.instagram : 'N/A'}</Text>
                </View>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }

  openFacebook(){
    Linking.canOpenURL(myConstants.SOCIAL.IFFACEBOOKAPPAVAILABLE).then(supported => {
      if(supported){
        // ToastAndroid.show('Navigating to Facebook!', ToastAndroid.SHORT);
        Linking.openURL("fb://facewebmodal/f?href="+this.state.facebookURL);
      }
      else {
        Linking.openURL(myConstants.SOCIAL.IFFACEBOOKAPPNOTAVAILABLE);
      }
    }).catch(err => {
      alert(err);
    })
  }

  openTwitter(){
    Linking.canOpenURL(myConstants.SOCIAL.IFTWITTERAPPAVAILABLE).then(supported => {
      if(supported){
        Linking.openURL("twitter://user?screen_name="+this.state.twitter);
      }
      else {
        Linking.openURL(myConstants.SOCIAL.IFTWITTERAPPNOTAVAILABLE);
      }
    }).catch(err => {
      alert(err);
    })
  }

  openInstagram(){
    Linking.canOpenURL(myConstants.SOCIAL.IFINSTAGRAMAPPAVAILABLE).then(supported => {
      if(supported){
        // ToastAndroid.show('Navigating to Twitter!', ToastAndroid.SHORT);
        Linking.openURL("instagram://user?username="+this.state.instagram);
      }
      else {
        Linking.openURL(myConstants.SOCIAL.IFINSTAGRAMAPPNOTAVAILABLE);
      }
    }).catch(err => {
      alert(err);
    })
  }

  openWebsite(){
    if(this.state.website)
    {
      Linking.canOpenURL(this.state.website).then(supported => {
        if (supported) {
          Linking.openURL(this.state.website);
        } else {
          ToastAndroid.show('URL is not well formatted', ToastAndroid.SHORT);
        }
      });
    }
  }

  openDialer(){
    Linking.openURL('tel:+91'+this.state.mobile);
  }

}
