import React from 'react';
import { Text, View, StatusBar, Image, TouchableOpacity, ListView, ScrollView, AsyncStorage } from 'react-native';

var styles = require('../../styles/profileStyle');
import TopStatusBar from '../../extras/statusBar';

import Icon from 'react-native-vector-icons/FontAwesome';

import myConstants from '../../constant/constant';

import { Actions } from 'react-native-router-flux';

import MyText from 'react-native-letter-spacing';

export default class AgentProfile extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      assembly:'Gurgaon'
    }
  }

  componentWillMount(){
    AsyncStorage.getItem('softUser').then((data) => {
      data = JSON.parse(data);
      this.setState(data);
    })
  }


  render() {
    return (
      <View style={styles.profileContainer}>
        <TopStatusBar/>
        <ScrollView>
          <View style={styles.profileMain}>
            <View style={styles.imageBoxView}>
              <Image source={myConstants.USER.PARTY_BANNER} style={styles.imageBox} />
            </View>
            <View style={styles.profileImageBoxContainer}>
                <View style={styles.profileImageBox}>
                <Image source={myConstants.USER.BOOTH_AGENT_IMAGE} style={styles.profileImageBoxImage} />
                <View style={styles.profileImageBoxPartyImage}>
                  <Image source={myConstants.USER.PARTY_LOGO} style={styles.profileImageBoxPartyImageImage} />
                </View>
                </View>
                <View style={styles.profileBoxContent}>
                  <Text style={styles.profileBoxCandidateName}>
                    {this.state.name ? this.state.name : myConstants.USER.CANDIDATE_NAME}
                  </Text>
                  <Text style={styles.profileBoxAssemblyName}>
                    {myConstants.USER.ASSEMBLY}
                  </Text>
                </View>
              </View>
          </View>
          <View style={styles.profileContent}>
            <View style={styles.profileContentBox}>
              <View style={styles.profileIcon}>
                <View style={styles.profileIconBox}>
                  <Icon name="mobile" color={myConstants.PURPLE} size={18}></Icon>
                </View>
              </View>
              <View style={styles.profileHeadName}>
                <Text style={styles.profileHeadNameText}>Contact</Text>
              </View>
              <View style={styles.profileBodyName}>
                <Text style={styles.profileHeadNameText}>{this.state.mobile}</Text>
              </View>
            </View>
            <View style={styles.profileContentBox}>
              <View style={styles.profileIcon}>
                <View style={styles.profileIconBox}>
                  <Icon name="envelope" color={myConstants.ORANGE} size={myConstants.PROFILEICONSIZE2}></Icon>
                </View>
              </View>
              <View style={styles.profileHeadName}>
                <Text style={styles.profileHeadNameText}>Address</Text>
              </View>
              <View style={styles.profileBodyName}>
                <Text style={styles.profileHeadNameText}>{this.state.address}</Text>
              </View>
            </View>
            <View style={styles.profileContentBox}>
              <View style={styles.profileIcon}>
                <Icon name="globe" color={myConstants.COLOR2} size={myConstants.PROFILEICONSIZE}></Icon>
              </View>
              <View style={styles.profileHeadName}>
                <Text style={styles.profileHeadNameText}>Zone</Text>
              </View>
              <View style={styles.profileBodyName}>
                <Text style={styles.profileHeadNameText}>{this.state.zone}</Text>
              </View>
            </View>
            <View style={styles.profileContentBox}>
              <View style={styles.profileIcon}>
                <Icon name="cube" color={myConstants.COLOR3} size={myConstants.PROFILEICONSIZE2}></Icon>
              </View>
              <View style={styles.profileHeadName}>
                <Text style={styles.profileHeadNameText}>Block</Text>
              </View>
              <View style={styles.profileBodyName}>
                <Text style={styles.profileHeadNameText}>{this.state.block}</Text>
              </View>
            </View>
            <View style={styles.profileContentBox}>
              <View style={styles.profileIcon}>
                <Icon name="life-saver" color={myConstants.COLOR7} size={myConstants.PROFILEICONSIZE2}></Icon>
              </View>
              <View style={styles.profileHeadName}>
                <Text style={styles.profileHeadNameText}>Village/Town</Text>
              </View>
              <View style={styles.profileBodyName}>
                <Text style={styles.profileHeadNameText}>{this.state.villName ? this.state.villName : 'Gurgaon'}</Text>
              </View>
            </View>
            <View style={styles.profileContentBox}>
              <View style={styles.profileIcon}>
                <Icon name="cog" color={myConstants.COLOR6} size={myConstants.PROFILEICONSIZE}></Icon>
              </View>
              <View style={styles.profileHeadName}>
                <Text style={styles.profileHeadNameText}>Booth No</Text>
              </View>
              <View style={styles.profileBodyName}>
                <Text style={styles.profileHeadNameText}>{this.state.booth}</Text>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }

}
