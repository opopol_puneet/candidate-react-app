import React from 'react';
import {
  Text,
  View,
  StatusBar,
  Image,
  TouchableOpacity,
  ToastAndroid,
  ScrollView,
  ActivityIndicator,
  Switch,
  TextInput,
  Modal,
  TouchableHighlight,
  ListView,
  AsyncStorage,
  Alert
} from 'react-native';

import TopStatusBar from '../../extras/statusBar';

import Icon from 'react-native-vector-icons/FontAwesome';

import { Actions } from 'react-native-router-flux';

import myConstants from '../../constant/constant';

import styles from '../../styles/agentStyle';

import { ButtonGroup } from 'react-native-elements';

import Spinner from 'react-native-loading-spinner-overlay';

var ds = new ListView.DataSource({
    rowHasChanged: (row1, row2) => row1 !== row2,
});

export default class AgentManageVoter extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      loading:false,
      fetchLimit:3,
      data:[],
      default:'Data goes here',
      dataSource: ds.cloneWithRows([]),
    }
  }

  componentDidMount(){
    AsyncStorage.getItem('token').then((val) => {
      this.setState({token:val});
    })
  }

  startSearch(value){
    this.setState({search:value});
    if(value.length>=3){
      this.setState({loading : true,  default:'', data: []});
      fetch(myConstants.USER.SOFT_API_URL+"/emsapi/custom/search", {
        method : 'POST',
        headers: {
          'Accept' : 'application/json',
          'Content-Type' : 'application/json',
          'Authorization' : 'Bearer '+this.state.token
        },
        body:JSON.stringify({
          search : value
        })
      })
      .then((response) => response.json())
      .then((response) => {
        this.setState({loading:false});
        if(response.data.length)
        {
          this.setState({dataSource:this.state.dataSource.cloneWithRows(response.data)});
          this.setState({data:response.data});
        }
        else
        {
          this.setState({data:[], default:'No Related Data Found'});
        }
      })
      .catch((error) => {
        console.warn(error);
      })
    }
    else {
      if(this.state.data.length)
      {
          this.setState({loading:false, default:''});
      }
      else {
        this.setState({default:'Data goes here'})
      }
    }
  }

  renderHeader(){
    return (
      <View style={styles.manageHeader}>
        <View style={styles.manageTitle}>
          <Text style={styles.manageTitleText}>Manage Voter</Text>
        </View>
        <View style={styles.manageSearch}>
          <TextInput underlineColorAndroid='transparent'
            style={styles.manageSearchInput}
            value={this.state.search}
            placeholder={'Enter Mininum 3 characters to search'}
            autoCorrect={false}
            placeholderTextColor={'white'}
            onChangeText={this.startSearch.bind(this)}
          />
        </View>
      </View>
    )
  }


  _renderRow(rowData){
    return (
      <View style={styles.dataRow}>
        <TouchableHighlight style={[styles.dataColumn, {width:115}]}>
          <Text style={[styles.dataColumnCaption, {color:'blue', textDecorationLine:'underline'}]}>{rowData.voter_id}</Text>
        </TouchableHighlight>
        <View style={[styles.dataColumn, {width:115}]}>
          <Text style={styles.dataColumnCaption}>{rowData.name}</Text>
        </View>
        <View style={[styles.dataColumn, {width:115}]}>
          <Text style={styles.dataColumnCaption}>{rowData.patron}</Text>
        </View>
      </View>
    )
  }


  render() {
    if(this.state.default)
    {
      return (
        <View style={styles.manageContainer}>
          <TopStatusBar/>
          {this.renderHeader()}
          <View style={styles.manageSection}>
            <View style={styles.manageSectionDefault}>
              <Text style={styles.manageSectionDefaultText}>{this.state.default}</Text>
            </View>
          </View>
        </View>
      );
    }
    else if (this.state.data.length) {
      return (
        <View style={styles.manageContainer}>
          <TopStatusBar/>
          {this.renderHeader()}
          <View style={styles.manageSection}>
            <View style={styles.manageSectionDefault}>
              <View style={styles.dataHeaders}>
                <View style={[styles.dataHeader, {width:115}]}>
                  <Text style={styles.dataHeaderCaption}>Voter Id</Text>
                </View>
                <View style={[styles.dataHeader, {width:115}]}>
                  <Text style={styles.dataHeaderCaption}>Name</Text>
                </View>
                <View style={[styles.dataHeader, {width:115}]}>
                  <Text style={styles.dataHeaderCaption}>Father Name</Text>
                </View>
              </View>
              <View style={{flex: 1, flexDirection:'row', marginTop:-9}}>
                  <View style={{flex: 1}}>
                    <ListView dataSource={this.state.dataSource} enableEmptySections={true}
                      renderRow={(rowData, rowID) => {
                        return (
                          <View style={{flex: 1, flexDirection:'row', height:38, paddingLeft:7, paddingRight:7, paddingBottom:7}}>
                            <TouchableHighlight style={[styles.dataColumn, {width:115}]} onPress={this.singleItem.bind(this, rowID, rowData.id)}>
                              <Text style={[styles.dataColumnCaption, {color:'blue', textDecorationLine:'underline'}]}>{rowData.voter_id}</Text>
                            </TouchableHighlight>
                            <View style={[styles.dataColumn, {width:115}]}>
                              <Text style={styles.dataColumnCaption}>{rowData.name}</Text>
                            </View>
                            <View style={[styles.dataColumn, {width:115}]}>
                              <Text style={styles.dataColumnCaption}>{rowData.patron}</Text>
                            </View>
                          </View>
                        )
                      }}
                     />
                  </View>
              </View>
            </View>
          </View>
        </View>
      )
    }
    else if (this.state.loading) {
      return (
        <View style={styles.manageContainer}>
          <TopStatusBar/>
          {this.renderHeader()}
          <View style={styles.manageSection}>
            <View style={styles.manageSectionDefault}>
              <ActivityIndicator size="large" color={myConstants.PURPLE} />
            </View>
          </View>
        </View>
      )
    }

    else {
        return (
        <View style={styles.manageContainer}>
          <TopStatusBar/>
          {this.renderHeader()}
          <View style={styles.manageSection}>
            <View style={styles.manageSectionDefault}>
              <Text style={styles.manageSectionDefaultText}>{this.state.default}</Text>
            </View>
          </View>
        </View>
      );
    }
  }

  singleItem(rowId, id){
    Actions.singleitem({rowID:rowId, id:id});
  }
}
