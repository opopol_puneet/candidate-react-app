import React from 'react';
import {
  Text,
  View,
  StatusBar,
  Image,
  TouchableOpacity,
  ToastAndroid,
  ScrollView,
  ActivityIndicator,
  Switch,
  TextInput,
  Modal,
  TouchableHighlight,
  AsyncStorage,
  Alert,
  BackHandler
} from 'react-native';

import TopStatusBar from '../../extras/statusBar';

import Icon from 'react-native-vector-icons/FontAwesome';

import { Actions } from 'react-native-router-flux';

import myConstants from '../../constant/constant';

import styles from '../../styles/singleItemStyle';

import PopoverTooltip from 'react-native-popover-tooltip';

import Spinner from 'react-native-loading-spinner-overlay';

const isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
  const paddingToBottom = 20;
  return layoutMeasurement.height + contentOffset.y >=
    contentSize.height - paddingToBottom;
};

export default class SingleItem extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      loading:true,
      errorMsg:'',
      poleLoading:false
    }
  }


  componentDidMount(){
    const {state} = this.props.navigation;
    AsyncStorage.getItem('token').then((val) => {
      val = JSON.parse(val)
      this.setState({token:val});
      fetch(myConstants.USER.SOFT_API_URL+"/emsapi/get/view/voter/"+state.params.id, {headers:{Authorization:'Bearer '+this.state.token}})
      .then((res) => res.json())
      .then((response) => {
        // console.warn(response);
        this.setState(response.data);
        this.setState({loading:false});
      })
      .catch(error => {
        // console.warn(error);
      })
    })

    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick.bind(this));
  }

  handleBackButtonClick(){
      Actions.agentdatalist()
      return true;
  }
  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick.bind(this));
  }

  goToBack(){
    Actions.agentdatalist();
  }

  render(){
    if(this.state.loading) {
      return <View style={{marginTop:20}}><ActivityIndicator size="large" color={myConstants.PURPLE} /></View>;
    }
    if(this.state.errorMsg!='') {
      return <View style={{alignItems:'center', justifyContent:'center', marginTop:20}}><Text style={{fontSize:myConstants.FONTSIZE18, fontFamily:myConstants.FONTFAMILY, color:myConstants.BLACK}}>{this.state.errorMsg}</Text></View>;
    }
    return (

      <View style={styles.voterViewContainer}>
        <Spinner visible={this.state.poleLoading} color={myConstants.PURPLE} overlayColor={myConstants.PURPLERBGA} />
        <TopStatusBar/>
        <View style={styles.listHeader}>
          <TouchableOpacity style={styles.listHeaderButton} activeOpacity={0.9} onPress={this.goToBack.bind(this)}>
            <Icon name="chevron-left" style={styles.listHeaderButtonIcon} />
          </TouchableOpacity>
          <View style={styles.listHeaderTitle}>
            <Text style={styles.listHeaderTitleText}>View Voter</Text>
          </View>
        </View>
        <View style={{flex:1, flexDirection:'row', padding:20}}>
          <ScrollView>
          <View style={styles.voterViewRow}>
            <View style={styles.voterViewColumn}>
              <Text style={styles.voterViewColumnText}>Voter Id</Text>
            </View>
            <View style={styles.voterViewColumn}>
              <Text style={styles.voterViewColumnText}>{this.state.voter_id}</Text>
            </View>
          </View>
          <View style={styles.voterViewRow}>
            <View style={styles.voterViewColumn}>
              <Text style={styles.voterViewColumnText}>Name</Text>
            </View>
            <View style={styles.voterViewColumn}>
              <Text style={styles.voterViewColumnText}>{this.state.name}</Text>
            </View>
          </View>
          <View style={styles.voterViewRow}>
            <View style={styles.voterViewColumn}>
              <Text style={styles.voterViewColumnText}>F/H Name</Text>
            </View>
            <View style={styles.voterViewColumn}>
              <Text style={styles.voterViewColumnText}>{this.state.patron}</Text>
            </View>
          </View>
          <View style={styles.voterViewRow}>
            <View style={styles.voterViewColumn}>
              <Text style={styles.voterViewColumnText}>Ward No</Text>
            </View>
            <View style={styles.voterViewColumn}>
              <Text style={styles.voterViewColumnText}>{this.state.ward}</Text>
            </View>
          </View>
          <View style={styles.voterViewRow}>
            <View style={styles.voterViewColumn}>
              <Text style={styles.voterViewColumnText}>Age</Text>
            </View>
            <View style={styles.voterViewColumn}>
              <Text style={styles.voterViewColumnText}>{this.state.age}</Text>
            </View>
          </View>
          <View style={styles.voterViewRow}>
            <View style={styles.voterViewColumn}>
              <Text style={styles.voterViewColumnText}>Gender</Text>
            </View>
            <View style={styles.voterViewColumn}>
              <Text style={styles.voterViewColumnText}>{this.state.gender}</Text>
            </View>
          </View>
          <View style={styles.voterViewRow}>
            <View style={styles.voterViewColumn}>
              <Text style={styles.voterViewColumnText}>Polled</Text>
            </View>
            <View style={[styles.voterViewColumn, {backgroundColor:this.state.polled==1?"#3c763d":"#a94442"}]}>
              {
                this.state.polled==1
                ?
                <Text style={[styles.voterViewColumnText,{fontWeight: "bold",color:'white'}]}>Yes</Text>
                :
                <TouchableHighlight underlayColor='rgba(169,68,66, 0.8)' onPress={this.poleIt.bind(this)}>
                  <Text>No</Text>
                </TouchableHighlight>
              }

            </View>
          </View>
          <View style={[styles.voterViewButtonRow, styles.btn]}>
            <TouchableHighlight underlayColor={myConstants.PURPLERBGA} style={styles.voterViewButton} disabled={this.state.btnStatus} onPress={this.edit.bind(this)}>
              <Text style={styles.voterViewButtonText}>Update Voter Information</Text>
            </TouchableHighlight>
          </View>
          </ScrollView>
        </View>

      </View>
    )
  }

  edit(){
    AsyncStorage.removeItem('editVoter').then((data) => {
      Actions.editvoter({rowID:this.props.rowID,id:this.props.id});
    });
  }

  poleIt(){
    Alert.alert(
      'Alert',
      'Are you want to pole this vote?',
      [
        {text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        {text: 'Yes', onPress: this.confirmPole.bind(this)},
      ],
      { cancelable: false }
    )
  }
  confirmPole(){
    var id = this.props.id;
    var rowID = this.props.rowID;
    this.setState({poleLoading:true});
    fetch(myConstants.USER.SOFT_API_URL+"/emsapi/pole/vote/"+id, {headers:{Authorization:'Bearer '+this.state.token}})
    .then((res) => res.json())
    .then((response) => {
      this.setState({poleLoading:false, polled:1});
      var data = {
        rowID:rowID,
        polled:1
      }
      ToastAndroid.show(response.data, ToastAndroid.SHORT);
      AsyncStorage.setItem('updateList', JSON.stringify(data));
      AsyncStorage.getItem('counter').then((data) => {
        data = JSON.parse(data);
        data.unpoled = data.unpoled-1;
        data.poled = data.poled+1;
        AsyncStorage.setItem('counter', JSON.stringify(data));
      });
    })
    .catch(error => {
      this.setState({poleLoading:false});
    })
  }

}
