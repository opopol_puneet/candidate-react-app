import React from 'react';
import {
  Text,
  View,
  StatusBar,
  TouchableOpacity,
  ToastAndroid,
  ScrollView,
  ActivityIndicator,
  Switch,
  TextInput,
  TouchableHighlight,
  BackHandler,
  AsyncStorage
} from 'react-native';

import TopStatusBar from '../../../extras/statusBar';

import Icon from 'react-native-vector-icons/FontAwesome';

import { Actions } from 'react-native-router-flux';

import myConstants from '../../../constant/constant';

import styles from '../../../styles/editAgentStyle';

import Spinner from 'react-native-loading-spinner-overlay';

export default class EditSocial extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      loading:false
    }
  }

  componentDidMount(){
    AsyncStorage.getItem('voter').then((data) => {
      this.setState(JSON.parse(data));
    })
    AsyncStorage.getItem('token').then((data) => {
      this.setState({token:data});
    })
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick.bind(this));
  }

  handleBackButtonClick(){
      Actions.pop()
      return true;
  }
  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick.bind(this));
  }

  render() {

    return (
      <View style={styles.mainContainer}>
        <TopStatusBar/>
        <Spinner visible={this.state.loading} color={myConstants.PURPLE} overlayColor={myConstants.PURPLERBGA} />
        <View style={styles.listHeader}>
          <View style={styles.listHeaderTitle}>
            <Text style={styles.listHeaderTitleText}>Edit Mapping</Text>
          </View>
        </View>
        <ScrollView>
          <View style={styles.content}>
            <View style={[styles.column, {height:70}]}>
              <Text style={styles.label}>Head of Family</Text>
              <View style={styles.switchBox}>
                <View style={styles.switchBoxLeft}>
                  <Switch
                      onValueChange={(value) => this.setState({isHead:value})}
                      onTintColor="rgba(138, 71, 221, 0.8)"
                      thumbTintColor="white"
                      tintColor="rgba(138, 71, 221, 0.2)"
                      value={this.state.isHead}
                      style={{ transform: [{ scaleX: 1.2 }, { scaleY: 1.2 }] }}
                  />
                </View>
                <View style={styles.switchBoxRight}>
                  <Text style={styles.switchBoxRightText}>{this.state.isHead ? 'Yes' : 'No'}</Text>
                </View>
              </View>
            </View>
            <View style={[styles.column, {height:70}]}>
              <Text style={styles.label}>Approachable</Text>
              <View style={styles.switchBox}>
                <View style={styles.switchBoxLeft}>
                  <Switch
                      onValueChange={(value) => this.setState({isApproach:value})}
                      onTintColor="rgba(138, 71, 221, 0.8)"
                      thumbTintColor="white"
                      tintColor="rgba(138, 71, 221, 0.2)"
                      value={this.state.isApproach}
                      style={{ transform: [{ scaleX: 1.2 }, { scaleY: 1.2 }] }}
                  />
                </View>
                <View style={styles.switchBoxRight}>
                  <Text style={styles.switchBoxRightText}>{this.state.isApproach ? 'Yes' : 'No'}</Text>
                </View>
              </View>
            </View>
            <View style={styles.column}>
              <View style={styles.socialSubmit}>
                <View style={styles.socialSubmitLeftView}>
                  <TouchableOpacity style={styles.socialSubmitButton} activeOpacity={0.9}  onPress={this.goToBack.bind(this)}>
                      <Icon name="long-arrow-left" style={styles.socialSubmitIcon} />
                      <Text style={styles.socialSubmitText}>Back</Text>
                  </TouchableOpacity>
                </View>
                <View style={styles.socialSubmitRightView}>
                  <TouchableOpacity style={styles.socialSubmitButton} activeOpacity={0.9}  onPress={this.goToNext.bind(this)}>
                      <Text style={[styles.socialSubmitRightText, {paddingLeft:10, flex:2}]}>Submit</Text>
                      <Icon name="long-arrow-right" style={[styles.socialSubmitRightIcon, {paddingLeft:12, flex:1.2}]} />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
  goToBack(){
    Actions.editperference({rowID:this.props.rowID, id:this.props.id});
  }
  goToNext(){
    this.setState({loading:true})
    AsyncStorage.getItem('voter').then((data) => {
      data = JSON.parse(data);
      data.approach = this.state.isApproach ? 1 : 0,
      data.head = this.state.isHead ? 1 : 0

      AsyncStorage.removeItem('voter').then(() => {
        AsyncStorage.setItem('voter', JSON.stringify(data)).then(() => {
          fetch(myConstants.USER.SOFT_API_URL+"/emsapi/update/booth/voter/data", {
            method : 'POST',
            headers: {
              'Accept' : 'application/json',
              'Content-Type' : 'application/json',
              'Authorization' : 'Bearer '+this.state.token
            },
            body:JSON.stringify(data)
          })
          .then((res) => res.json())
          .then((response) => {
            ToastAndroid.show(response.data, ToastAndroid.SHORT);
            this.setState({loading:false})
            Actions.singleitem({rowID:this.props.rowID, id:this.props.id});
          })
          .catch((error) => {
            this.setState({loading:false})
            console.warn(error);
          })
        })
      })
    })
  }
}
