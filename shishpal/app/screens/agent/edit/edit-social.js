import React from 'react';
import {
  Text,
  View,
  StatusBar,
  Image,
  TouchableOpacity,
  ToastAndroid,
  ScrollView,
  ActivityIndicator,
  Switch,
  TextInput,
  Picker,
  TouchableHighlight,
  BackHandler,
  AsyncStorage
} from 'react-native';

import TopStatusBar from '../../../extras/statusBar';

import Icon from 'react-native-vector-icons/FontAwesome';

import { Actions, ActionConst } from 'react-native-router-flux';

import myConstants from '../../../constant/constant';

import styles from '../../../styles/editAgentStyle';

var ImagePicker = require('react-native-image-picker');

export default class EditSocial extends React.Component {

  constructor(props){
    super(props);
    this.state = {
    }
  }

  componentDidMount(){
    AsyncStorage.getItem('voter').then((data) => {
      data = JSON.parse(data)
      this.setState(data);

      if(data.subcast!=0){
        fetch(myConstants.USER.SOFT_API_URL+"/get/subcastes/"+data.caste, {
          method:'GET'
        }).then((response) => response.json())
        .then((responseData) =>   {
          this.setState({subcastesData:JSON.stringify(responseData.data)});
        })
        .catch((error) => {
        })
      }
    })

    fetch(myConstants.USER.SOFT_API_URL+"/get/qualifications", {
      method:'GET'
    }).then((response) => response.json())
    .then((responseData) => {
      this.setState({qualificationsData:JSON.stringify(responseData.data)});
    })
    .catch((error) => {
    })

    fetch(myConstants.USER.SOFT_API_URL+"/get/occupations", {
      method:'GET'
    }).then((response) => response.json())
    .then((responseData) =>   {
      this.setState({occupationsData:JSON.stringify(responseData.data)});
    })
    .catch((error) => {
    })

    fetch(myConstants.USER.SOFT_API_URL+"/get/castes", {
      method:'GET'
    }).then((response) => response.json())
    .then((responseData) =>   {
      this.setState({castesData:JSON.stringify(responseData.data)});
    })
    .catch((error) => {
    })

    fetch(myConstants.USER.SOFT_API_URL+"/get/religions", {
      method:'GET'
    }).then((response) => response.json())
    .then((responseData) =>   {
      this.setState({religionsData:JSON.stringify(responseData.data)});
    })
    .catch((error) => {
    })

    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick.bind(this));
  }

  handleBackButtonClick(){
      Actions.editvoter({rowID:this.props.rowID,id:this.props.id});
      return true;
  }
  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick.bind(this));
  }

  onCasteChange(value){
    if(value!=='0')
    {
      this.setState({caste:value});
      fetch(myConstants.USER.SOFT_API_URL+"/get/subcastes/"+value, {
        method:'GET'
      }).then((response) => response.json())
      .then((responseData) =>   {
        this.setState({subcastesData:JSON.stringify(responseData.data)});
      })
      .catch((error) => {
      })
    }
  }

  render() {
    let qualificationsItems = <Picker.Item key='0' value='0' label='Data Loading...' />;
    if(this.state.qualificationsData){
      qualificationsItems = JSON.parse(this.state.qualificationsData).map(function(item){
          return <Picker.Item key={item.id} value={item.id} label={item.hindi_value} />
      })
      qualificationsItems.unshift(<Picker.Item key='0' value='0' label='Select Qualification' />)
    }

    let occupationsItems = <Picker.Item key='0' value='0' label='Data Loading...' />;
    if(this.state.occupationsData){
      occupationsItems = JSON.parse(this.state.occupationsData).map(function(item){
          return <Picker.Item key={item.id} value={item.id} label={item.hindi_value} />
      });
      occupationsItems.unshift(<Picker.Item key='0' value='0' label='Select Occupation' />)
    }

    let castesItems = <Picker.Item key='0' value='0' label='Data Loading...' />;
    if(this.state.castesData){
      castesItems = JSON.parse(this.state.castesData).map(function(item){
          return <Picker.Item key={item.id} value={item.id} label={item.caste_category} />
      });
      castesItems.unshift(<Picker.Item key='0' value='0' label='Select Caste' />)
    }

    let religionsItems = <Picker.Item key='0' value='0' label='Data Loading...' />;
    if(this.state.religionsData){
      religionsItems = JSON.parse(this.state.religionsData).map(function(item){
          return <Picker.Item key={item.id} value={item.id} label={item.hindi_value} />
      });
      religionsItems.unshift(<Picker.Item key='0' value='0' label='Select Religion' />)
    }

    let subcastesItems = <Picker.Item key='0' value='0' label='Data Loading...' />;
    if(this.state.subcastesData){
      subcastesItems = JSON.parse(this.state.subcastesData).map(function(item){
          return <Picker.Item key={item.id} value={item.id} label={item.subcategory} />
      });
      subcastesItems.unshift(<Picker.Item key='0' value='0' label='Select SubCaste' />)
    }

    return (
      <View style={styles.mainContainer}>
        <TopStatusBar/>
        <View style={styles.listHeader}>
          <View style={styles.listHeaderTitle}>
            <Text style={styles.listHeaderTitleText}>Edit Social</Text>
          </View>
        </View>
        <ScrollView>
          <View style={styles.content}>
            <View style={styles.column}>
              <Text style={styles.label}>Qualification</Text>
              <View style={styles.pickerBox}>
                <Picker style={styles.picker} selectedValue={this.state.qualification} onValueChange={(qualification) => this.setState({qualification:qualification})}>
                  {qualificationsItems}
                </Picker>
              </View>
            </View>
            <View style={styles.column}>
              <Text style={styles.label}>Occupation</Text>
              <View style={styles.pickerBox}>
                <Picker style={styles.picker} selectedValue={this.state.occupation} onValueChange={(occupation) => this.setState({occupation:occupation})} >
                  {occupationsItems}
                </Picker>
              </View>
            </View>
            <View style={styles.column}>
              <Text style={styles.label}>Religion</Text>
              <View style={styles.pickerBox}>
                <Picker style={styles.picker} selectedValue={this.state.religion_id} onValueChange={(religion_id) => this.setState({religion_id:religion_id})} >
                  {religionsItems}
                </Picker>
              </View>
            </View>
            <View style={styles.column}>
              <Text style={styles.label}>Caste</Text>
              <View style={styles.pickerBox}>
                <Picker style={styles.picker} selectedValue={this.state.caste} onValueChange={this.onCasteChange.bind(this)} >
                  {castesItems}
                </Picker>
              </View>
            </View>
            {
              this.state.subcastesData
              ?
              <View style={styles.column}>
                <Text style={styles.label}>SubCaste</Text>
                <View style={styles.pickerBox}>
                  <Picker style={styles.picker} selectedValue={this.state.subcast} onValueChange={(subcast) => this.setState({subcast:subcast})} >
                    {subcastesItems}
                  </Picker>
                </View>
              </View>
              :
              null
            }
            <View style={styles.column}>
              <Text style={styles.label}>Mobile</Text>
              <TextInput ref='mobile' style={styles.input} underlineColorAndroid='transparent'
                value={this.state.mobile}
                keyboardType = 'numeric'
                maxLength={10}
                onSubmitEditing={(event) => { this.refs.temp_address.focus()}}
                onChangeText={(value) => {this.setState({mobile:value})}}
               />
            </View>
            <View style={styles.column}>
              <Text style={styles.label}>Temporary Address</Text>
              <TextInput ref='temp_address' style={styles.input} underlineColorAndroid='transparent'
                value={this.state.temp_address}
                onChangeText={(value) => {this.setState({temp_address:value})}}
              />
            </View>
            <View style={styles.column}>
              <View style={styles.socialSubmit}>
                <View style={styles.socialSubmitLeftView}>
                  <TouchableOpacity style={styles.socialSubmitButton} activeOpacity={0.9}  onPress={this.goToBack.bind(this)}>
                      <Icon name="long-arrow-left" style={styles.socialSubmitIcon} />
                      <Text style={styles.socialSubmitText}>Back</Text>
                  </TouchableOpacity>
                </View>
                <View style={styles.socialSubmitRightView}>
                  <TouchableOpacity style={styles.socialSubmitButton} activeOpacity={0.9}  onPress={this.goToNext.bind(this)}>
                      <Text style={styles.socialSubmitRightText}>Next</Text>
                      <Icon name="long-arrow-right" style={styles.socialSubmitRightIcon} />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
  goToBack(){
    Actions.editvoter({rowID:this.props.rowID, id:this.props.id});
  }
  goToNext(){
    AsyncStorage.getItem('voter').then((data) => {
      data = JSON.parse(data);
      data.qualification = this.state.qualification,
      data.occupation = this.state.occupation,
      data.religion_id = this.state.religion_id,
      data.caste = this.state.caste,
      data.subcaste_id = this.state.subcast,
      data.mobile = this.state.mobile,
      data.temp_address = this.state.temp_address
      AsyncStorage.removeItem('voter').then(() => {
        AsyncStorage.setItem('voter', JSON.stringify(data)).then(() => {
          Actions.editeffective({rowID:this.props.rowID, id:this.props.id});
        })
      })
    })
  }
}
