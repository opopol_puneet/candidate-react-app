import React from 'react';
import {
  Text,
  View,
  StatusBar,
  Image,
  TouchableOpacity,
  ToastAndroid,
  ScrollView,
  ActivityIndicator,
  Switch,
  TextInput,
  Picker,
  TouchableHighlight,
  BackHandler,
  AsyncStorage
} from 'react-native';

import TopStatusBar from '../../../extras/statusBar';

import Icon from 'react-native-vector-icons/FontAwesome';

import { Actions } from 'react-native-router-flux';

import myConstants from '../../../constant/constant';

import styles from '../../../styles/editAgentStyle';

var ImagePicker = require('react-native-image-picker');

export default class EditSocial extends React.Component {

  constructor(props){
    super(props);
    this.state = {
    }
  }

  componentDidMount(){
    AsyncStorage.getItem('voter').then((data) => {
      this.setState(JSON.parse(data));
    })
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick.bind(this));
  }

  handleBackButtonClick(){
      Actions.editsocial({rowID:this.props.rowID, id:this.props.id});
      return true;
  }
  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick.bind(this));
  }

  render() {

    return (
      <View style={styles.mainContainer}>
        <TopStatusBar/>
        <View style={styles.listHeader}>
          <View style={styles.listHeaderTitle}>
            <Text style={styles.listHeaderTitleText}>Edit Effective</Text>
          </View>
        </View>
        <ScrollView>
          <View style={styles.content}>
            <View style={styles.column}>
              <Text style={styles.label}>Name</Text>
              <TextInput ref='mobile' style={styles.input} underlineColorAndroid='transparent'
                value={this.state.relative_name}
                onChangeText={(value) => {this.setState({relative_name:value})}}
                onSubmitEditing={(event) => { this.refs.mobile.focus()}}
               />
            </View>
            <View style={styles.column}>
              <Text style={styles.label}>Mobile</Text>
              <TextInput ref='mobile' style={styles.input} underlineColorAndroid='transparent'
                value={this.state.relative_mobile}
                keyboardType = 'numeric'
                maxLength={10}
                onChangeText={(value) => {this.setState({relative_mobile:value})}}
                onSubmitEditing={(event) => { this.refs.temp_address.focus()}}
               />
            </View>
            <View style={[styles.column, {height:120}]}>
              <Text style={styles.label}>Temporary Address</Text>
              <TextInput ref='temp_address' style={[styles.input, {height:100, textAlignVertical: "top"}]} underlineColorAndroid='transparent'
                value={this.state.relative_address}
                multiline={true}
                numberOfLines={10}
                onChangeText={(value) => {this.setState({relative_address:value})}}
                onSubmitEditing={(event) => { this.refs.remarks.focus()}}
              />
            </View>
            <View style={[styles.column, {height:120}]}>
              <Text style={styles.label}>Remarks</Text>
              <TextInput ref='remarks' style={[styles.input, {height:100, textAlignVertical: "top"}]} underlineColorAndroid='transparent'
                value={this.state.remarks}
                multiline={true}
                numberOfLines={10}
                onChangeText={(value) => {this.setState({remarks:value})}}
              />
            </View>
            <View style={styles.column}>
              <View style={styles.socialSubmit}>
                <View style={styles.socialSubmitLeftView}>
                  <TouchableOpacity style={styles.socialSubmitButton} activeOpacity={0.9}  onPress={this.goToBack.bind(this)}>
                      <Icon name="long-arrow-left" style={styles.socialSubmitIcon} />
                      <Text style={styles.socialSubmitText}>Back</Text>
                  </TouchableOpacity>
                </View>
                <View style={styles.socialSubmitRightView}>
                  <TouchableOpacity style={styles.socialSubmitButton} activeOpacity={0.9} onPress={this.goToNext.bind(this)}>
                      <Text style={styles.socialSubmitRightText}>Next</Text>
                      <Icon name="long-arrow-right" style={styles.socialSubmitRightIcon} />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
  goToBack(){
    Actions.editsocial({rowID:this.props.rowID, id:this.props.id});
  }
  goToNext(){
    AsyncStorage.getItem('voter').then((data) => {
      data = JSON.parse(data);
      data.relative_name = this.state.relative_name,
      data.relative_mobile = this.state.relative_mobile,
      data.relative_address = this.state.relative_address,
      data.remarks = this.state.remarks

      AsyncStorage.removeItem('voter').then(() => {
        AsyncStorage.setItem('voter', JSON.stringify(data)).then(() => {
          Actions.editperference({rowID:this.props.rowID, id:this.props.id});
        })
      })
    })
  }
}
