import React from 'react';
import {
  Text,
  View,
  StatusBar,
  Image,
  TouchableOpacity,
  ToastAndroid,
  ScrollView,
  ActivityIndicator,
  Switch,
  TextInput,
  Modal,
  TouchableHighlight,
  BackHandler,
  AsyncStorage
} from 'react-native';

import TopStatusBar from '../../../extras/statusBar';

import Icon from 'react-native-vector-icons/FontAwesome';

import { Actions } from 'react-native-router-flux';

import myConstants from '../../../constant/constant';

import styles from '../../../styles/editAgentStyle';

var ImagePicker = require('react-native-image-picker');

export default class EditVoter extends React.Component {

  constructor(props){
    super(props);
    this.state = {
    }
  }

  componentDidMount(){
    const {state} = this.props.navigation;

    AsyncStorage.getItem('token').then((val) => {
      if(val){
        val = JSON.parse(val);
        fetch(myConstants.USER.SOFT_API_URL+"/emsapi/get/view/voter/"+this.props.id, {headers:{Authorization:'Bearer '+val}})
        .then((response) => response.json())
        .then((responseData) =>   {
          responseData.data.ward = responseData.data.ward!=0 ? responseData.data.ward : '';
          responseData.data.genders==1 ? this.setState({isGender:true}) : this.setState({isGender:false});
          this.setState(responseData.data);
          AsyncStorage.setItem('voter', JSON.stringify(responseData.data));
        })
        .catch((error) => {
          // console.warn(error)
        })
      }
    })

    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick.bind(this));
  }

  handleBackButtonClick(){
    Actions.singleitem({rowID:this.props.rowID, id:this.props.id})
    return true;
  }
  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick.bind(this));
  }

  render() {
    return (
      <View style={styles.mainContainer}>
        <TopStatusBar/>
        <View style={styles.listHeader}>
          <View style={styles.listHeaderTitle}>
            <Text style={styles.listHeaderTitleText}>Edit Voter</Text>
          </View>
          <TouchableOpacity style={styles.listHeaderButton} activeOpacity={0.9} onPress={this.goToList.bind(this)}>
            <Icon name="list-alt" style={styles.listHeaderButtonIcon} />
          </TouchableOpacity>
        </View>
        <ScrollView>
          <View style={styles.content}>
            <View style={styles.column}>
              <Text style={styles.label}>Ward</Text>
              <TextInput style={styles.input}
                underlineColorAndroid='transparent'
                value={this.state.ward}
                onSubmitEditing={(event) => { this.refs.serial.focus()}}
                onChangeText={(value) => {this.setState({ward:value})}}
               />
            </View>
            <View style={styles.column}>
              <Text style={styles.label}>Serial No</Text>
              <TextInput ref='serial' style={styles.input} underlineColorAndroid='transparent'
                value={this.state.sr_no}
                onSubmitEditing={(event) => { this.refs.voter_id.focus()}}
                onChangeText={(value) => {this.setState({serial:value})}}
               />
            </View>
            <View style={styles.column}>
              <Text style={styles.label}>Voter ID</Text>
              <TextInput ref='voter_id' style={styles.input} underlineColorAndroid='transparent'
                value={this.state.voter_id}
                onSubmitEditing={(event) => { this.refs.name.focus()}}
                onChangeText={(value) => {this.setState({voter_id:value})}}
               />
            </View>
            <View style={styles.column}>
              <Text style={styles.label}>Name</Text>
              <TextInput ref='name' style={styles.input} underlineColorAndroid='transparent'
                value={this.state.name}
                onSubmitEditing={(event) => { this.refs.father_name.focus()}}
                onChangeText={(value) => {this.setState({name:value})}}
              />
            </View>
            <View style={styles.column}>
              <Text style={styles.label}>Father Name</Text>
              <TextInput ref='father_name' style={styles.input} underlineColorAndroid='transparent'
                value={this.state.patron}
                onSubmitEditing={(event) => { this.refs.house_no.focus()}}
                onChangeText={(value) => {this.setState({father_name:value})}}
               />
            </View>
            <View style={styles.column}>
              <Text style={styles.label}>House No</Text>
              <TextInput ref='house_no' style={styles.input} underlineColorAndroid='transparent'
                value={this.state.house_no}
                onSubmitEditing={(event) => { this.refs.age.focus()}}
                onChangeText={(value) => {this.setState({house_no:value})}}
               />
            </View>
            <View style={styles.column}>
              <Text style={styles.label}>Age</Text>
              <TextInput ref='age' style={styles.input} underlineColorAndroid='transparent'
                value={this.state.age}
                onChangeText={(value) => {this.setState({age:value})}}
               />
            </View>
            <View style={styles.column}>
              <Text style={styles.label}>Gender</Text>
              <View style={styles.switchBox}>
                <View style={styles.switchBoxLeft}>
                  <Switch
                      onValueChange={(value) => this.setState({isGender:value})}
                      onTintColor="rgba(138, 71, 221, 0.8)"
                      thumbTintColor="white"
                      tintColor="rgba(138, 71, 221, 0.2)"
                      value={this.state.isGender}
                  />
                </View>
                <View style={styles.switchBoxRight}>
                  <Text style={styles.switchBoxRightText}>{this.state.isGender ? 'Female' : 'Male'}</Text>
                </View>
              </View>
            </View>
            <View style={styles.column}>
              <TouchableHighlight underlayColor='rgba(138, 71, 221, 0.8)' style={styles.submit} onPress={this.goToNext.bind(this)}>
                <Text style={styles.submitText}>Next</Text>
              </TouchableHighlight>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
  uploadImage()
  {
    var options = {
      title: 'Select Avatar',
      storageOptions: {
        skipBackup: true,
        path: 'images'
      }
    };
    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response.uri };

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          photo: source,
          photoByteCode : 'data:image/jpeg;base64,' + response.data
        });
      }
    });
  }
  removeImage(){
    this.setState({photo:''});
  }
  goToNext(){
    AsyncStorage.getItem('voter').then((data) => {
      data = JSON.parse(data);
      data.id = this.props.id,
      data.ward = this.state.ward,
      data.sr_no = this.state.sr_no,
      data.voter_id = this.state.voter_id,
      data.name = this.state.name,
      data.patron = this.state.patron,
      data.house_no = this.state.house_no,
      data.age = this.state.age,
      data.gender = this.state.isGender ? 'महिला' : 'पुरुष',
      data.photo = this.state.photo ? this.state.photoByteCode : ''

      AsyncStorage.removeItem('voter').then(() => {
        AsyncStorage.setItem('voter', JSON.stringify(data)).then(() => {
          Actions.editsocial({rowID:this.props.rowID, id:this.props.id});
        })
      })
    })

  }
  goToList(){
    Actions.agentdatalist();
  }
}
