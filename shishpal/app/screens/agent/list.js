import React from 'react';
import {
  Text,
  View,
  StatusBar,
  Image,
  TouchableOpacity,
  ToastAndroid,
  ScrollView,
  ActivityIndicator,
  Switch,
  TextInput,
  Modal,
  TouchableHighlight,
  ListView,
  AsyncStorage,
  Alert
} from 'react-native';

import TopStatusBar from '../../extras/statusBar';

import Icon from 'react-native-vector-icons/FontAwesome';

import { Actions } from 'react-native-router-flux';

import myConstants from '../../constant/constant';

import styles from '../../styles/agentStyle';

import { ButtonGroup } from 'react-native-elements';

import Spinner from 'react-native-loading-spinner-overlay';

const isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
  const paddingToBottom = 20;
  return layoutMeasurement.height + contentOffset.y >=
    contentSize.height - paddingToBottom;
};

export default class AgentDataList extends React.Component {

  constructor(props){
    var ds = new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
    });
    super(props);
    this.state = {
      loading:true,
      errorMsg:'',
      page:1,
      data:[],
      searching:false,
      filterModal:false,
      dataSource: ds.cloneWithRows([]),
      selectedIndex: 0,
      poleLoading:false,
      filter:0
    }
  }

  static onEnter = () => {
    AsyncStorage.getItem('updateList').then((response) => {
        if(response)
        {
          response = JSON.parse(response);
          Actions.refresh({
            rowID : response.rowID
          });
        }
    })

    AsyncStorage.getItem('selectedIndex').then((data) => {
      if(data)
      {
        Actions.refresh({
          selectedIndex : parseInt(data)
        });
      }
    })
  }

  // componentWillReceiveProps (nextProps) {
  //   if (this.props.rowID !== nextProps.rowID) {
  //     var rowID = nextProps.rowID;
  //     var newArray = this.state.data.slice();
  //     console.warn(newArray);
  //     newArray[rowID] = {
  //       key: rowID,
  //       id: newArray[rowID].id,
  //       village: newArray[rowID].village,
  //       booth: newArray[rowID].booth,
  //       voter_id: newArray[rowID].voter_id,
  //       name: newArray[rowID].name,
  //       patron: newArray[rowID].patron,
  //       ward_no: newArray[rowID].ward_no,
  //       age: newArray[rowID].age,
  //       caste: newArray[rowID].caste,
  //       gender: newArray[rowID].gender,
  //       occupation: newArray[rowID].occupation,
  //       mobile_no: newArray[rowID].mobile_no,
  //       relation: newArray[rowID].relation,
  //       polled: 1,
  //     };
  //     this.setState({
  //         dataSource: this.state.dataSource.cloneWithRows(newArray),
  //         data : newArray
  //     });
  //     AsyncStorage.removeItem('updateList');
  //   }
  //   if(this.props.selectedIndex !== nextProps.selectedIndex){
  //     var ds = new ListView.DataSource({
  //         rowHasChanged: (row1, row2) => row1 !== row2,
  //     });
  //     this.setState({selectedIndex:nextProps.selectedIndex, filter:nextProps.selectedIndex, page:1, filterModal:false, data:[], dataSource: ds.cloneWithRows([])});
  //     AsyncStorage.removeItem('selectedIndex');
  //     this.getVotersData(nextProps.selectedIndex, 1);
  //   }
  // }

  componentDidMount(){
    this._isMounted = true;
    AsyncStorage.getItem('token').then((val) => {
      this.setState({token:JSON.parse(val)});
      this.getVotersData(this.state.selectedIndex, this.state.page)
    })
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  getVotersData(selectedIndex, page){
    this.setState({loading:true});
    this.setState({searching:true});
    fetch(myConstants.USER.SOFT_API_URL+"/emsapi/get/all/raw/data?filter="+selectedIndex+"&page="+page, {headers:{Authorization:'Bearer '+this.state.token}})
    .then((response) => response.json())
    .then((data) => {

      var arr = this.state.data.slice();

      data.data.data.map((item) => {
        arr.push(item);
      })
      this.setState({data:arr});
      this.setState({dataSource:this.state.dataSource.cloneWithRows(this.state.data)});

      this.setState({loading:false});
      this.setState({searching:false});
      this.setState({page:this.state.page+1});
    })
    .catch((error) => {
      this.setState({errorMsg:'No Data'});
      this.setState({searching:false});
      this.setState({loading:false});
    })
  }

  handleEnd = () => {
    if(this.state.searching===false){
      this.setState(state => ({page:this.state.page+1}), () => this.getVotersData(this.state.selectedIndex, this.state.page));
    }
  }



  openFilterModal(){
    this.setState({filterModal:true});
  }

  closeModal(){
    this.setState({filterModal:false});
  }

  submitModal(){
    ToastAndroid.show('Submit Clicked', ToastAndroid.SHORT);
    this.setState({filterModal:false});
  }

  updateIndex(selectedIndex){
    if(this.state.selectedIndex !== selectedIndex)
    {
      var ds = new ListView.DataSource({
          rowHasChanged: (row1, row2) => row1 !== row2,
      });
      this.setState({selectedIndex:selectedIndex, filter:selectedIndex, page:1, filterModal:false, data:[], dataSource: ds.cloneWithRows([])});
      this.getVotersData(selectedIndex, 1);
    }
  }

  render() {
    const {loading, selectedIndex} = this.state;
    const buttons = ['All', 'Poled', 'Unpoled']
    return (
      <View style={styles.listContainer}>
        <TopStatusBar/>
        <Spinner visible={this.state.poleLoading} color={myConstants.PURPLE} overlayColor={myConstants.PURPLERBGA} />
        <Modal
          visible={this.state.filterModal}
          animationType={'slide'}
          transparent={true}
          onRequestClose={() => this.closeModal()}
        >
          <View style={styles.modalContainer}>
            <View style={styles.innerContainer}>
              <View style={styles.modalHeader}>
                <Text style={styles.modalHeaderText}>Filter Data</Text>
              </View>
              <View style={styles.modalHeaderSubTitle}>
                <Text style={styles.modalHeaderSubTitleText}>Filter Data</Text>
              </View>
              <View style={styles.modalBox}>
                <View style={styles.modalBoxInputBox}>
                <ButtonGroup
                  onPress={this.updateIndex.bind(this)}
                  selectedIndex={selectedIndex}
                  buttons={buttons}
                  containerStyle={{height: 50}}
                  textStyle={{fontSize:12, fontFamily:myConstants.FONTFAMILY}}
                  selectedButtonStyle={{backgroundColor:myConstants.PURPLE}}
                />
                </View>
              </View>
            </View>
          </View>
        </Modal>
        <View style={styles.listHeader}>
          <View style={styles.listHeaderTitle}>
            <Text style={styles.listHeaderTitleText}>List</Text>
          </View>
          <TouchableOpacity style={styles.listHeaderButton} activeOpacity={0.9} onPress={this.openFilterModal.bind(this)}>
            <Icon name="filter" style={styles.listHeaderButtonIcon} />
          </TouchableOpacity>
        </View>

        <View style={styles.dataHeaders}>
          <View style={[styles.dataHeader, {width:100}]}>
            <Text style={styles.dataHeaderCaption}>Voter Id</Text>
          </View>
          <View style={[styles.dataHeader, {width:100}]}>
            <Text style={styles.dataHeaderCaption}>Name</Text>
          </View>
          <View style={[styles.dataHeader, {width:100}]}>
            <Text style={styles.dataHeaderCaption}>Father Name</Text>
          </View>
          <View style={[styles.dataHeader, {width:50}]}>
            <Text style={styles.dataHeaderCaption}>Status</Text>
          </View>
        </View>
        <View style={{flex: 1, flexDirection:'row', marginTop:-9}}>
          <ScrollView onScroll={({nativeEvent}) => {
            if (isCloseToBottom(nativeEvent)) {
              if(this.state.searching===false){
                this.getVotersData(this.state.selectedIndex, this.state.page)
              }
            }
          }}>
            <View style={{flex: 1, paddingLeft:7, paddingRight:7, paddingBottom:7}}>
              <ListView dataSource={this.state.dataSource} renderRow={this._renderRow.bind(this)} enableEmptySections={true} />
              {loading && (<ActivityIndicator style={{ height: 40, marginTop:7 }} color={myConstants.PURPLE} size="large" />)}
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }

  _renderRow(rowData, sectionID, rowID){
    return (
      <View style={styles.dataRow} key={rowID}>
        <TouchableHighlight style={[styles.dataColumn, {width:100}]} onPress={this.singleItem.bind(this, rowID, rowData.id)}>
          <Text style={[styles.dataColumnCaption, {color:'blue', textDecorationLine:'underline'}]}>{rowData.voter_id}</Text>
        </TouchableHighlight>
        <View style={[styles.dataColumn, {width:100}]}>
          <Text style={styles.dataColumnCaption}>{rowData.name}</Text>
        </View>
        <View style={[styles.dataColumn, {width:100}]}>
          <Text style={styles.dataColumnCaption}>{rowData.patron}</Text>
        </View>
        <View style={[styles.dataColumn, {width:50}]}>
          {
            rowData.polled==1
          ?
            <Text style={[styles.dataColumnCaption, {color:"#3c763d",fontWeight: "bold"}]}>Yes</Text>
          :
            <TouchableHighlight underlayColor='rgba(169,68,66, 0.8)' onPress={this.poleIt.bind(this, rowID, rowData.id)}>
              <Text style={{color:"#a94442", fontSize:10}}>No</Text>
            </TouchableHighlight>
          }
        </View>
      </View>
    )
  }

  singleItem(rowId, id){
    Actions.singleitem({rowID:rowId, id:id});
  }

  poleIt(rowId, id){
    Alert.alert(
      'Alert',
      'Are you want to pole this vote?',
      [
        {text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        {text: 'Yes', onPress: this.confirmPole.bind(this, rowId, id)},
      ],
      { cancelable: false }
    )
  }

  confirmPole(rowID, id){
    this.setState({poleLoading:true});
    fetch(myConstants.USER.SOFT_API_URL+"/emsapi/pole/vote/"+id, {headers:{Authorization:'Bearer '+this.state.token}})
    .then((res) => res.json())
    .then((response) => {
      this.setState({poleLoading:false});
      ToastAndroid.show(response.data, ToastAndroid.SHORT);
      var newArray = this.state.data.slice();
      newArray[rowID] = {
        key: newArray[rowID].rowID,
        rowID: newArray[rowID].rowID,
        id: newArray[rowID].id,
        village: newArray[rowID].village,
        booth: newArray[rowID].booth,
        voter_id: newArray[rowID].voter_id,
        name: newArray[rowID].name,
        patron: newArray[rowID].patron,
        ward_no: newArray[rowID].ward_no,
        age: newArray[rowID].age,
        caste: newArray[rowID].caste,
        gender: newArray[rowID].gender,
        occupation: newArray[rowID].occupation,
        mobile_no: newArray[rowID].mobile_no,
        relation: newArray[rowID].relation,
        polled: 1,
      };
      this.setState({
          dataSource: this.state.dataSource.cloneWithRows(newArray),
          data : newArray
      });
      AsyncStorage.getItem('counter').then((data) => {
        data = JSON.parse(data);
        data.unpoled = data.unpoled-1;
        data.poled = data.poled+1;
        AsyncStorage.setItem('counter', JSON.stringify(data));
      });
    })
    .catch(error => {
      this.setState({poleLoading:false});
        console.warn(error);
    })
  }
}
