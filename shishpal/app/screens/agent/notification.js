import React from 'react';
import {
  Text,
  View,
  StatusBar,
  Image,
  TouchableOpacity,
  ToastAndroid,
  ScrollView,
  ActivityIndicator,
  Switch,
  TextInput,
  Modal,
  TouchableHighlight
} from 'react-native';

import TopStatusBar from '../../extras/statusBar';

import Icon from 'react-native-vector-icons/FontAwesome';

import { Actions } from 'react-native-router-flux';

import myConstants from '../../constant/constant';

export default class AgentNotification extends React.Component {
  constructor(props){
    super(props);
    this.state = {
    }
  }

  render() {
    return (
      <View style={{flex:1, justifyContent:'center', alignItems:'center',backgroundColor:myConstants.COLOR8}}>
        <TopStatusBar/>
        <Text style={{fontSize:22, fontFamily:myConstants.FONTFAMILY, color:myConstants.WHITE}}>Agent Notification Screen</Text>
      </View>
    );
  }
}
