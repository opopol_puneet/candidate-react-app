import React from 'react';
import {
  Text,
  View,
  StatusBar,
  Image,
  TouchableOpacity,
  ToastAndroid,
  ScrollView,
  ActivityIndicator,
  Switch,
  TextInput,
  Modal,
  TouchableHighlight,
  BackHandler,
  AsyncStorage
} from 'react-native';

import TopStatusBar from '../../extras/statusBar';

import Icon from 'react-native-vector-icons/FontAwesome';

import { Actions } from 'react-native-router-flux';

import myConstants from '../../constant/constant';

import styles from '../../styles/agentStyle';

export default class AgentHome extends React.Component {

  constructor(props){
    super(props);
    this.state = {
    }
  }

  static onEnter = () => {
    AsyncStorage.getItem('counter').then((response) => {
        if(response)
        {
          response = JSON.parse(response);
          Actions.refresh({
            total:response.total, unpoled:response.unpoled, poled:response.poled
          });
        }
      })
  }

  componentWillReceiveProps (nextProps) {
    if (this.props.poled !== nextProps.poled) {
      this.setState({poled: nextProps.poled, unpoled:nextProps.unpoled, total:nextProps.total});
    }
  }


  _handle(){
    AsyncStorage.getItem('token').then((token) => {
      token = JSON.parse(token);
      fetch(myConstants.USER.SOFT_API_URL+"/emsapi/get/vote/status/counter", {headers:{Authorization:'Bearer '+token}})
      .then((res) => res.json())
      .then((response) => {
        // console.warn(response)
        if(response.error==='token_expired'){
          AsyncStorage.removeItem('token');
          AsyncStorage.removeItem('role');
          AsyncStorage.removeItem('authId');
          AsyncStorage.removeItem('softUser').then((val) => {
              ToastAndroid.show('Token Expired, Please Re-Login', ToastAndroid.SHORT);
              Actions.login();
          });
        }
        AsyncStorage.setItem('counter', JSON.stringify(response));
        this.setState({total:response.total, unpoled:response.unpoled, poled:response.poled});
      })
      .catch(error => {
        // console.warn(error);
      })
    })
  }

  componentWillMount(){
    this._handle();
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick.bind(this));
  }

  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick.bind(this));
  }
  handleBackButtonClick(){
    if(Actions.currentScene==='_agenthome')
    {
      Actions.home();
      return true;
    }
    Actions.pop();
    return true;
  }

  goToHomeScreen(){
    Actions.home();
  }

  render() {
    return (
      <View style={styles.mainContainer}>
        <TopStatusBar/>
        <View style={styles.listHeader}>
          <View style={styles.listHeaderTitle}>
            <Text style={styles.listHeaderTitleText}>Agent Home</Text>
          </View>
          <TouchableOpacity style={styles.homeHeaderButton} onPress={this.goToHomeScreen.bind(this)}>
            <Icon name="home" style={styles.homeHeaderButtonIcon} />
          </TouchableOpacity>
        </View>
        <View style={styles.profileNavs}>
          <View style={styles.variantsBoxes}>
            <TouchableOpacity activeOpacity={0.9} style={styles.variantsBoxRow} onPress={this.goToList.bind(this, 0)}>
                <View style={styles.variantsBoxCaption}>
                    <Text style={styles.variantsBoxCaptionText}>Total</Text>
                </View>
                <View style={styles.variantsBoxCaption}>
                    <Text style={styles.variantsBoxCaptionText}>{this.state.total}</Text>
                </View>
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.9} style={styles.variantsBoxRow} onPress={this.goToList.bind(this, 2)}>
                <View style={styles.variantsBoxCaption}>
                    <Text style={styles.variantsBoxCaptionText}>Unpoled</Text>
                </View>
                <View style={styles.variantsBoxCaption}>
                    <Text style={styles.variantsBoxCaptionText}>{this.state.unpoled}</Text>
                </View>
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.9} style={styles.variantsBoxRow} onPress={this.goToList.bind(this, 1)}>
                <View style={styles.variantsBoxCaption}>
                    <Text style={styles.variantsBoxCaptionText}>Poled</Text>
                </View>
                <View style={styles.variantsBoxCaption}>
                    <Text style={styles.variantsBoxCaptionText}>{this.state.poled}</Text>
                </View>
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.9} style={styles.variantsBoxRow} onPress={this.goToManageScreen.bind(this)}>
                <View style={styles.variantsBoxCaption}>
                    <Text style={styles.variantsBoxCaptionText}>Manage Voter</Text>
                </View>
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.9} style={styles.variantsBoxRow} >
                <View style={styles.variantsBoxCaption}>
                    <Text style={styles.variantsBoxCaptionText}>Feedback</Text>
                </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }

  goToManageScreen(){
    Actions.manage();
  }

  goToList(selectedIndex){
    AsyncStorage.setItem('selectedIndex', JSON.stringify(selectedIndex)).then(() => {
        Actions.agentdatalist()
    })
  }
}
