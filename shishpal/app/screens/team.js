import React from 'react';
import {
  Text,
  View,
  StatusBar,
  Image,
  TouchableOpacity,
  ListView,
  ScrollView,
  TextInput,
  TouchableHighlight,
  AsyncStorage,
  Modal,
  ToastAndroid,
  DatePickerAndroid,
  Switch,
  Picker,
  NetInfo,
  BackHandler
} from 'react-native';

var styles = require('../styles/teamStyle');
import TopStatusBar from '../extras/statusBar';

import Icon from 'react-native-vector-icons/FontAwesome';

import Spinner from 'react-native-loading-spinner-overlay';

import myConstants from '../constant/constant';

import { Actions } from 'react-native-router-flux';

import MyText from 'react-native-letter-spacing';

export default class Team extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      mobilePattern:/^[6789]\d{9}$/,
      isNameVerified:false,
      isFatherNameVerified:false,
      isMobileVerified:false,
      isDOBVerified:false,
      isGenderVerified:false,
      isAssemblyVerified:true,
      isVillageVerified:false,
      isAddressVerified:false,
      isTermVerified:true,
      btnDisable:true,
      otpBtnDisable:true,
      otpModal:false,
      id:'',
      loading:false,
      token_id:'',
      token:'',
      name:'',
      mobile:'',
      father_name:'',
      address:'',
      term:true,
      termsModal:false,
    }
  }

  componentDidMount(){
    AsyncStorage.getItem('user').then((data) => {
      data = JSON.parse(data);
      fetch(myConstants.USER.API_URL+"/assembly/"+data.assemblyID)
      .then((res) => res.json())
      .then((response) => {
        this.setState({villageData:JSON.stringify(response.data)})
      })
      .catch(error => {
      })
    })

    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick.bind(this));
  }

  handleBackButtonClick(){
      Actions.home();
      return true;
  }

  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick.bind(this));
  }

  async openDatepicker(){
    try {
      var today = new Date();
      var maxDate =  new Date(today.getFullYear()-18, today.getMonth(), today.getDate());
      const {action, year, month, day} = await DatePickerAndroid.open({
        date : maxDate,
        maxDate : maxDate
      });
      if (action !== DatePickerAndroid.dismissedAction) {
        var date = new Date(year, month, day);
        var month = date.getMonth() + 1;

        if(today.getTime() > date.getTime()){
          var validate = today.setFullYear(today.getFullYear()-18, today.getMonth(), today.getDate());
          if(validate > date.getTime()){
            this.setState({dob:date.getDate()+'-'+month+'-'+date.getFullYear()});
            this.setState({dobObject:date});
            this.setState({isDOBVerified:true})
            this.validateTeamJoin('dob');
          }
          else
          {
            ToastAndroid.show('Minimun age 18 years required', ToastAndroid.SHORT);
            this.setState({isDOBVerified:false})
            this.setState({dobObject:''});
            this.setState({dob:''});
            this.validateTeamJoin('dob');
          }
        }
        else {
            ToastAndroid.show('Date of birth must be lower than today date', ToastAndroid.SHORT);
            this.setState({isDOBVerified:false})
            this.setState({dobObject:''});
            this.setState({dob:''});
            this.validateTeamJoin('dob');
        }
      }
    }
    catch ({code, message}) {
    }
  }

  goToBack(){
    Actions.home();
  }

  render() {

    let villageItems = <Picker.Item key='0' value='0' label='Data Loading...' />;
    if(this.state.villageData){
      villageItems = JSON.parse(this.state.villageData).map(function(item){
          return <Picker.Item key={item.id} value={item.id} label={item.village} />
      })
      villageItems.unshift(<Picker.Item key='0' value='0' label='Select Village' />)
    }

    return (
      <View style={styles.writeMeContainer}>
        <TopStatusBar/>
        <Spinner visible={this.state.loading} color='#6B934B' overlayColor='rgba(107, 147, 75, 0.5)' />
        <Modal
          visible={this.state.termsModal}
          animationType={'slide'}
          transparent={true}
          onRequestClose={() => this.closeTermsModal()}
        >
          <View style={styles.modalContainer}>
            <View style={styles.innerContainer}>
                <TouchableHighlight style={styles.termsModalCloseButton} underlayColor='rgba(0,0,0,0)' onPress={this.closeTermsModal.bind(this)}>
                  <Icon name="times" size={16}/>
                </TouchableHighlight>

                <Text style={{margin:30, fontFamily:myConstants.FONTFAMILY, fontSize:myConstants.FONTSIZE14, lineHeight:23}}>
                  I am citizen of india. I am above 18 years. I here by give my consent in receiving any communication
                  from the Rajneeti Live in either writing Electronically and | or in any audio/video visual format via
                  phone including SMS, MMS and | or at my address.
                </Text>
            </View>
          </View>
        </Modal>
        <Modal
          onShow={() => { this.textInput.focus() }}
          style={{ zIndex: -10 }}
          visible={this.state.otpModal}
          animationType={'slide'}
          transparent={true}
          onRequestClose={() => this.closeModal()}
        >
          <View style={styles.modalContainer}>
            <View style={styles.innerContainer}>
              <View style={styles.modalHeader}>
                <Text style={styles.modalHeaderText}>Enter OTP</Text>
              </View>
              <View style={styles.modalHeaderSubTitle}>
                <Text style={styles.modalHeaderSubTitleText}>6 digits OTP received on your mobile</Text>
              </View>
              <View style={styles.modalBox}>
                <View style={styles.modalBoxInputBox}>
                  <TextInput underlineColorAndroid='transparent'
                    keyboardType = 'numeric'
                    ref={(input) => { this.textInput = input }}
                    onChangeText={this.fillEnteredToken.bind(this)}
                    style={styles.modalBoxInputBoxInput}
                    value={this.state.enteredToken}
                    maxLength={6}
                  />
                </View>
                <View style={styles.modalButtons}>
                  <View style={styles.modalButtonLeft}>
                  <TouchableHighlight underlayColor='rgba(138, 71, 221, 0.8)' style={styles.modalButtonLeftButton} onPress={this.cancelModal.bind(this)}>
                      <Text style={styles.modalButtonLeftButtonText}>Cancel</Text>
                    </TouchableHighlight>
                  </View>
                  <View style={styles.modalButtonRight}>
                  <TouchableHighlight underlayColor='rgba(138, 71, 221, 0.8)' style={[styles.modalButtonRightButton, {opacity:this.state.otpBtnDisable?0.4:1}]} disabled={this.state.otpBtnDisable}  onPress={this.submitModal.bind(this)}>
                      <Text style={styles.modalButtonRightButtonText}>Submit</Text>
                    </TouchableHighlight>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </Modal>
        <View style={styles.listHeader}>
          <TouchableOpacity style={styles.listHeaderButton} activeOpacity={0.9} onPress={this.goToBack.bind(this)}>
            <Icon name="chevron-left" style={styles.listHeaderButtonIcon} />
          </TouchableOpacity>
          <View style={styles.listHeaderTitle}>
            <Text style={styles.listHeaderTitleText}>Team Join</Text>
          </View>
        </View>
        <View style={{flex:1, flexDirection:'row', justifyContent:'center',paddingLeft:10, paddingRight:10, paddingTop:10}}>
          <ScrollView>
            <View style={styles.writeMeForm}>
              <View style={styles.writeMeRow}>
                <View style={styles.writeMeLabel}>
                  <Text style={styles.writeMeLabelText}>Name</Text>
                </View>
                <View style={styles.writeMeInput}>
                  <TextInput underlineColorAndroid='transparent'
                    onChangeText={this.nameChange.bind(this)}
                    onSubmitEditing={(event) => { this.refs.father.focus()}}
                    style={styles.writeMeInputBox}
                    value={this.state.name}
                    maxLength={40}
                  />
                </View>
              </View>
              <View style={styles.writeMeRow}>
                <View style={styles.writeMeLabel}>
                  <Text style={styles.writeMeLabelText}>Father/Husband Name</Text>
                </View>
                <View style={styles.writeMeInput}>
                  <TextInput underlineColorAndroid='transparent'
                    ref='father'
                    onChangeText={this.fatherNameChange.bind(this)}
                    onSubmitEditing={(event) => { this.refs.mobile.focus()}}
                    style={styles.writeMeInputBox}
                    value={this.state.father_name}
                    maxLength={40}
                  />
                </View>
              </View>
              <View style={styles.writeMeRow}>
                <View style={styles.writeMeLabel}>
                  <Text style={styles.writeMeLabelText}>Mobile</Text>
                </View>
                <View style={styles.writeMeInput}>
                  <TextInput underlineColorAndroid='transparent'
                    ref='mobile'
                    keyboardType = 'numeric'
                    onChangeText={this.mobileChange.bind(this)}
                    style={styles.writeMeInputBox}
                    value={this.state.mobile}
                    maxLength={10}
                  />
                </View>
              </View>
              <View style={styles.writeMeRow}>
                <View style={styles.writeMeLabel}>
                  <Text style={styles.writeMeLabelText}>Date of Birth</Text>
                </View>
                <View style={styles.writeMeInput}>
                  <TouchableHighlight underlayColor='rgba(0,0,0,0)' style={styles.datePicker}  onPress={this.openDatepicker.bind(this)}><Text style={styles.datePickerCaption}>{this.state.dob ? this.state.dob : 'Select Date of birth'}</Text></TouchableHighlight>
                </View>
              </View>
              <View style={styles.writeMeRow}>
                <View style={styles.writeMeLabel}>
                  <Text style={styles.writeMeLabelText}>Gender</Text>
                </View>
                <View style={styles.writeMeInput}>
                  <View style={styles.switchBox}>
                    <View style={styles.switchBoxLeft}>
                      <Switch
                          onValueChange={(value) => this.setState({isGender:value})}
                          onTintColor="rgba(138, 71, 221, 0.8)"
                          thumbTintColor="white"
                          tintColor="rgba(138, 71, 221, 0.2)"
                          value={this.state.isGender}
                      />
                    </View>
                    <View style={styles.switchBoxRight}>
                      <Text style={styles.switchBoxRightText}>{this.state.isGender ? 'Female' : 'Male'}</Text>
                    </View>
                  </View>
                </View>
              </View>
              <View style={styles.writeMeRow}>
                <View style={styles.writeMeLabel}>
                  <Text style={styles.writeMeLabelText}>Select Village</Text>
                </View>
                <View style={styles.writeMeInput}>
                  <View style={styles.pickerBox}>
                    <Picker style={styles.picker}
                      onValueChange={this.villageChange.bind(this)}
                      selectedValue={this.state.village}>
                      {villageItems}
                    </Picker>
                  </View>
                </View>
              </View>
              <View style={styles.writeMeRow}>
                <View style={styles.writeMeLabel}>
                  <Text style={styles.writeMeLabelText}>Voter Id (Optional)</Text>
                </View>
                <View style={styles.writeMeInput}>
                  <TextInput underlineColorAndroid='transparent'
                    onChangeText={this.voterIdChange.bind(this)}
                    onSubmitEditing={(event) => { this.refs.message.focus()}}
                    style={styles.writeMeInputBox}
                    value={this.state.voter_id}
                  />
                </View>
              </View>
              <View style={styles.writeMeRow}>
                <View style={styles.writeMeLabel}>
                  <Text style={styles.writeMeLabelText}>Address</Text>
                </View>
                <View style={styles.writeMeInput}>
                  <TextInput underlineColorAndroid='transparent'
                    ref='message'
                    multiline={true}
                    numberOfLines={10}
                    onChangeText={this.addressChange.bind(this)}
                    style={[styles.writeMeInputBox, {height:80, textAlignVertical: "top"}]}
                    value={this.state.address}
                    maxLength={500}
                  />
                </View>
              </View>
              <View style={styles.writeMeRow}>
                <TouchableHighlight underlayColor='rgba(138, 71, 221, 0.8)' style={[styles.submitWriteMe, {opacity:this.state.btnDisable?0.4:1}]} disabled={this.state.btnDisable} onPress={this.postTeamJoin.bind(this)}>
                  <Text style={styles.submitWriteMeText}>Submit</Text>
                </TouchableHighlight>
              </View>
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }

  showTerms(){
    this.setState({termsModal:true});
  }

  closeTermsModal(){
    this.setState({termsModal:false});
  }

  fillEnteredToken(val){
    this.setState({enteredToken:val});
    if(isNaN(val)===false && val.length===6){
      this.setState({otpBtnDisable:false})
    }
    else {
      this.setState({otpBtnDisable:true})
    }
  }

  closeModal(){
    return false;
  }

  cancelModal(){
    this.setState({otpModal:false});
  }



  nameChange(name){
    this.setState({name:name});
    if(name.length>2){
      this.setState({isNameVerified:true});
    }
    else {
      this.setState({isNameVerified:false});
    }
    this.validateTeamJoin('name')
  }

  fatherNameChange(father_name){
    this.setState({father_name:father_name});
    if(father_name.length>2){
      this.setState({isFatherNameVerified:true});
    }
    else {
      this.setState({isFatherNameVerified:false});
    }
    this.validateTeamJoin('father_name')
  }

  mobileChange(mobile){
    this.setState({mobile:mobile});
    if(this.state.mobilePattern.test(mobile)){
      this.setState({isMobileVerified:true});
    }
    else {
      this.setState({isMobileVerified:false});
    }
    this.validateTeamJoin('mobile')
  }

  villageChange(village){
    this.setState({village:village});
    if(village!=='0'){
      this.setState({isVillageVerified:true});
    }
    else {
      this.setState({isVillageVerified:false});
    }
    this.validateTeamJoin('village')
  }

  addressChange(address){
    this.setState({address:address});
    if(address.length>9)
    {
      this.setState({isAddressVerified:true});
      this.validateTeamJoin('address')
    }
    else {
      this.setState({isAddressVerified:false});
      this.validateTeamJoin('address')
    }
  }

  voterIdChange(voter_id){
    this.setState({voter_id:voter_id});
  }

  setTerm(term){
    this.setState({term:term})
    if(term){
      this.setState({isTermVerified:true});
    }
    else {
      this.setState({isTermVerified:false});
    }
    this.validateTeamJoin('term')
  }

  validateTeamJoin(type){

    switch (type) {
      case 'name':
        if(this.state.isFatherNameVerified &&
          this.state.isMobileVerified &&
          this.state.isDOBVerified &&
          this.state.isAddressVerified &&
          this.state.isVillageVerified)
        {
          this.setState({btnDisable:false})
        }
        else {
          this.setState({btnDisable:true})
        }
        break;
      case 'father_name':
        if(this.state.isNameVerified &&
          this.state.isMobileVerified &&
          this.state.isDOBVerified &&
          this.state.isVillageVerified &&
          this.state.isAddressVerified)
        {
          this.setState({btnDisable:false})
        }
        else {
          this.setState({btnDisable:true})
        }
        break;
      case 'mobile':
        if(this.state.isNameVerified &&
          this.state.isFatherNameVerified &&
          this.state.isDOBVerified &&
          this.state.isVillageVerified &&
          this.state.isAddressVerified)
        {
          this.setState({btnDisable:false})
        }
        else {
          this.setState({btnDisable:true})
        }
        break;
      case 'dob':
        if(this.state.isNameVerified &&
          this.state.isFatherNameVerified &&
          this.state.isMobileVerified &&
          this.state.isVillageVerified &&
          this.state.isAddressVerified)
        {
          this.setState({btnDisable:false})
        }
        else {
          this.setState({btnDisable:true})
        }
        break;

      case 'village':
        if(this.state.isNameVerified &&
          this.state.isFatherNameVerified &&
          this.state.isMobileVerified &&
          this.state.isDOBVerified &&
          this.state.isAddressVerified)
        {
          this.setState({btnDisable:false})
        }
        else {
          this.setState({btnDisable:true})
        }
        break;
      case 'address':
        if(this.state.isNameVerified &&
          this.state.isFatherNameVerified &&
          this.state.isMobileVerified &&
          this.state.isVillageVerified &&
          this.state.isDOBVerified &&
          this.state.isAddressVerified)
        {
          this.setState({btnDisable:false})
        }
        else {
          this.setState({btnDisable:true})
        }
        break;
      default:

    }
  }



  postTeamJoin(){
    NetInfo.isConnected.fetch().then(isConnected => {
      if(!isConnected){
        ToastAndroid.show('No Internet Connection', ToastAndroid.SHORT);
        return false;
      }
    });
    this.setState({loading:true});
    if(!this.state.isNameVerified &&
      !this.state.isFatherNameVerified &&
      !this.state.isMobileVerified &&
      !this.state.isDOBVerified &&
      !this.state.isAddressVerified &&
      !this.state.isVillageVerified)
    {
      this.setState({loading:false});
      ToastAndroid.show('Something is missing', ToastAndroid.SHORT);
      return false;
    }
    AsyncStorage.getItem('user').then((data) => {
      data = JSON.parse(data);

      var input = {
        name : this.state.name,
        father : this.state.father_name,
        gender : this.state.isGender ? 'female' : 'male',
        mobile : this.state.mobile,
        dob : this.state.dob,
        village : this.state.village,
        like : 'yes',
        address : this.state.address,
        agree : 1,
        voter_id : this.state.voter_id
      }
      fetch(myConstants.USER.API_URL+"/team-join/"+data.user_id, {
        method : 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body:JSON.stringify(input)
      })
      .then((response) => response.json())
      .then((responseData) => {
        if(responseData.error){
          this.setState({loading:false});
          ToastAndroid.show(responseData.errorMsg, ToastAndroid.SHORT);
          return false;
        }
        this.setState({id:responseData.id, token_id:responseData.token_id, token:responseData.token});
        this.setState({loading:false, otpModal:true});
        // this.refreshStates();
      })
      .catch((error) => {
        this.setState({loading:false});
      })
    })
  }

  submitModal(val){
    this.setState({otpModal:false});
    this.setState({loading:true});
    if(this.state.enteredToken==this.state.token){
      fetch(myConstants.USER.API_URL+"/team/verify", {
        method : 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body:JSON.stringify({
          id : this.state.token_id,
          token_id : this.state.token_id,
          token : this.state.token,
        })
      })
      .then((response) => response.json())
      .then((responseData) => {
        this.setState({loading:false, otpModal:false});
        this.refreshStates();
        ToastAndroid.show(responseData.message, ToastAndroid.SHORT);
      })
      .catch((error) => {
        this.setState({otpModal:true, loading:false});
      })
    }
    else {
      this.setState({loading:false, otpModal:true});
      ToastAndroid.show('OTP not matched!', ToastAndroid.SHORT);
    }
  }

  refreshStates(){
    this.setState({
      name:'',
      father_name:'',
      dob:'',
      mobile:'',
      address:'',
      village:'0',
      voter_id:'',
      btnDisable:true,
      isNameVerified:false,
      isFatherNameVerified:false,
      isMobileVerified:false,
      isDOBVerified:false,
      isAssemblyVerified:false,
      isVillageVerified:false,
      isAddressVerified:false,
      enteredToken:'',
      id:'',
      token_id:'',
      token:''
    })
  }
}
