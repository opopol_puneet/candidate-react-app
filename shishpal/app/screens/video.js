import React from 'react';
import { Text, View, StatusBar, Image, TouchableOpacity, Linking, ScrollView } from 'react-native';

var styles = require('../styles/videoStyle');
import TopStatusBar from '../extras/statusBar';

import Icon from 'react-native-vector-icons/FontAwesome';

import myConstants from '../constant/constant';

import { Actions } from 'react-native-router-flux';

export default class Video extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      names: [
         {
            id: 0,
            name: 'Develop your first react native app using youtube API and Play videos',
            videoId:'CvEnDmw9Nnc'
         },
         {
            id: 1,
            name: 'Swag Se Swagat Song | Tiger Zinda Hai | Salman Khan | Katrina Kaif | Vishal Dadlani | Neha Bhasin',
            videoId:'xmU0s2QtaEY'
         },
         {
            id: 2,
            name: 'Guru Randhawa: Lahore (Official Video) Bhushan Kumar | DirectorGifty | T-Series',
            videoId:'dZ0fwJojhrs'
         },
         {
            id: 3,
            name: 'SOHNEYA (Full Song) Guri Feat. Sukhe | Parmish Verma | latest Punjabi Songs 2017 | GEET MP3',
            videoId:'jaGZNgosNpk'
         },
         {
            id: 3,
            name: 'Palazzo (Full Video) | Kulwinder Billa & Shivjot | Aman | Himanshi | Latest Punjabi Song 2017',
            videoId:'9ND-Kmd60t8'
         },
         {
            id: 4,
            name: 'Cute Munda - Sharry Mann (Full Video Song) | Parmish Verma | Punjabi Songs 2017 | Lokdhun Punjabi',
            videoId:'PDlw1Tn-PVk'
         },
         {
            id: 5,
            name: 'Diljit Dosanjh - El Sueno ft. Tru-Skool (Official Video)',
            videoId:'u5szAqnRTwk'
         },
         {
            id: 6,
            name: 'Kangani | ( Full HD) | Rajvir Jawanda Ft. MixSingh | New Punjabi Songs 2017',
            videoId:'alVjh1nTHzE'
         },
         {
            id: 7,
            name: 'SUIT (Full Song) Nimrat Khaira Ft Mankirt Aulakh |Sukh Sanghera| Preet Hundal | Latest Punjabi Songs',
            videoId:'15KWUc1aLEU'
         },
         {
            id: 8,
            name: 'PATAKE (Full Video) || SUNANDA SHARMA || Latest Punjabi Songs 2016 || AMAR AUDIO',
            videoId:'Yk8PNl05EJE'
         },
         {
            id: 9,
            name: 'BILLI AKH (Full Video) | SUNANDA SHARMA | Latest Punjabi Songs 2016 || AMAR AUDIO',
            videoId:'JfMYoMG3Erw'
         }
      ]
    }
  }

  render() {
    return (
      <View style={styles.mainContainer}>
        <TopStatusBar/>
        <ScrollView>
        <View style={styles.videos}>
        {this.state.names.map((item, index) => (
          <TouchableOpacity key={item.id} style={styles.video} onPress={this.playThis.bind(this, item.videoId, item.name)}>
            <Text style={styles.videoText}>{item.name}</Text>
          </TouchableOpacity>
        ))}
        </View>
        </ScrollView>
      </View>
    );
  }

  playThis(videoId, name){
    Actions.playvideo({videoId:videoId, title:name});
  }
}
