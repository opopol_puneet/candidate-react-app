import React from 'react';
import { Text, View, StatusBar, Image, TouchableOpacity, ListView, ScrollView, ActivityIndicator, AsyncStorage, Linking, BackHandler } from 'react-native';

var styles = require('../styles/liveresultStyle');
import TopStatusBar from '../extras/statusBar';

import Icon from 'react-native-vector-icons/FontAwesome';

import myConstants from '../constant/constant';

import { Actions } from 'react-native-router-flux';

const ds = new ListView.DataSource({rowHasChanged:(row1, row2) => row1 != row2});

export default class LiveResult extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      dataSource: ds.cloneWithRows(['row 1', 'row 2']),
      loading:true,
      errorMsg:'',
      assembly:''
    }
  }

  componentDidMount(){
    AsyncStorage.getItem('user').then((data) => {
      data = JSON.parse(data);
      fetch(myConstants.USER.API_URL+"/results/nextmla/"+data.assemblyID+"?state="+myConstants.USER.STATE_ID)
      .then((response) => response.json())
      .then((data) => {
        this.setState({loading:false});
        if(data.error){
          this.setState({errorMsg:data.errorMsg})
        }
        else {
          this.setState({assembly:data.assembly});
          this.setState({dataSource:this.state.dataSource.cloneWithRows(data.result)});
        }
      })
      .catch((error) => {
        this.setState({errorMsg:'No Live Result'});
        this.setState({loading:false});
      })
    })

    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick.bind(this));
  }

  handleBackButtonClick(){
      Actions.home();
      return true;
  }

  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick.bind(this));
  }

  _renderRow(rowData){
    let candidateImage = myConstants.USER.IMAGE_URL+'/candidate/';
    let partyImage = myConstants.USER.IMAGE_URL+'/images/';
      return (
          <View style={styles.resultRow}>
            <View style={styles.candidateImageBox}>
              <Image source={{uri : rowData.image ? candidateImage+rowData.image : candidateImage+"/default.jpg"}} style={{height:50, width:50}} />
            </View>
            <View style={styles.resultDataBox}>
              <View style={styles.resultName}>
                <Text style={styles.resultNameText}>{rowData.mlaname}</Text>
              </View>
              <View style={styles.percentage}>
                <Text style={styles.percentageText}>{rowData.per}</Text>
              </View>
              <View style={styles.progress}>
                <View style={{backgroundColor:myConstants.PURPLE, flex:parseFloat(rowData.per), borderRadius:5}}/>
                <View style={{flex:100-parseFloat(rowData.per)}}/>
              </View>
            </View>
            <View style={styles.partyImageBox}>
              <Image source={{uri : partyImage+rowData.party_logo}} style={{height:50, width:50}} />
            </View>
          </View>
      )
  }

  goToBack(){
    Actions.home();
  }

  render() {
    return (
      <View style={styles.liveResultContainer}>
        <TopStatusBar/>
        <View style={styles.listHeader}>
          <TouchableOpacity style={styles.listHeaderButton} activeOpacity={0.9} onPress={this.goToBack.bind(this)}>
            <Icon name="chevron-left" style={styles.listHeaderButtonIcon} />
          </TouchableOpacity>
          <View style={styles.listHeaderTitle}>
            <Text style={styles.listHeaderTitleText}>MLA Results</Text>
          </View>
        </View>
        <ScrollView>
          <View style={styles.assemblyName}>
            <Text style={styles.assemblyNameText}>{this.state.assembly}</Text>
          </View>
          <View style={styles.liveResultFull}>
            {
              this.state.loading
              ?
              <View style={{marginTop:20}}><ActivityIndicator size="large" color={myConstants.PURPLE} /></View>
              :
              (
                this.state.errorMsg!=''
                ?
                <View style={{alignItems:'center', justifyContent:'center', marginTop:20}}>
                  <Text style={{fontSize:myConstants.FONTSIZE18, fontFamily:myConstants.FONTFAMILY, color:myConstants.BLACK}}>{this.state.errorMsg}</Text>
                </View>
                :
                <ListView dataSource={this.state.dataSource} renderRow={this._renderRow.bind(this)} />
              )
            }
          </View>
        </ScrollView>
        <TouchableOpacity style={styles.bottomBox} onPress={this.goToNextMla.bind(this)} activeOpacity={0.9}>
          <View style={styles.bottomBoxImageView}>
            <Image source={require('../../resources/RAJNEETI_LIVE.png')} style={styles.bottomBoxImage}/>
          </View>
          <View style={styles.bottomBoxTagView}>
            <Text style={styles.bottomBoxTagViewText}>
              Please give opinion at NextMLA app to vote and support for your favourite candidate.
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
  goToNextMla(){
      Linking.openURL(myConstants.SOCIAL.IFNEXTMLAAPPNOTAVAILABLE);
  }
}
