module.exports = {

  USER : {
    PARTY_BANNER : require('../../resources/INC.jpg'),
    PARTY_LOGO : require('../../resources/62215.png'),
    //PARTY_LOGO : require('../../resources/46990.png'),
    //CANDIDATE_IMAGE : require('../../resources/71248.png'),
    CANDIDATE_IMAGE : require('../../resources/shishpal.png'),
    BOOTH_AGENT_IMAGE : require('../../resources/default.jpg'),
    CANDIDATE_NAME : 'Shishpal Keharwala',
    ASSEMBLY : 'Kalanwali',
    USER_ID : 175,
    ID : 169,
    STATE_ID : 9,
    API_URL : 'http://nextmla.in/api',
    SOFT_API_URL : 'http://smapi.mdtpl.com',
    IMAGE_URL : 'http://nextmla.in',
    REFER_URL : 'http://nextmla.in',
    // API_URL : 'http://rajneetilive.com/api',
    // IMAGE_URL : 'http://rajneetilive.com',
    // REFER_URL : 'http://rajneetilive.com',
  },


  RESOURCES : {
    EVENTS : require('../../resources/EVENTS.png'),
    NEWS : require('../../resources/NEWS.png'),
    PROFILE : require('../../resources/CANDIDATE_PROFILE.png'),
    SOCIAL_MEDIA : require('../../resources/SOCIAL_MEDIA.png'),
    WRITE_ME : require('../../resources/WRITE_ME.png'),
    CANDIDATE_INTERVIEW : require('../../resources/CANDIDATE_INTERVIEW.png'),
    RAJNEETI_LIVE : require('../../resources/RAJNEETI_LIVE.png'),
    SHARE_PROFILE : require('../../resources/SHARE_PROFILE.png'),
    JOIN_TEAM : require('../../resources/JOIN_TEAM.png'),
  },

  GRAY : 'gray',
  LIGHTGRAY : '#d3d3d3',
  GREEN : "#6B934B",
  YELLOW : "#F4F92B",
  PURPLE : "#8a47dd",
  PURPLERBGA : "rgba(138,71,221, 0.5)",
  PURPLERBGA2 : "rgba(138,71,221, 0.4)",
  ORANGE : "#f26722",
  WHITE : "#FFFFFF",
  BLACK : "#000000",
  CONTAINER : "#F5F5F5",
  CONTAINER2 : "#696969",
  COLOR1 : "#f84524",
  COLOR2 : "#65cda8",
  COLOR3 : "#ffc500",
  COLOR4 : "#3a5a97",
  COLOR5 : "#3fbae3",
  COLOR6 : "#018947",
  COLOR7 : "#ba0100",
  COLOR8 : "#e0578b",
  COLOR9 : "#599ebf",
  INSTAGRAM : "#bc2a8d",
  NEWSBORDER : "#DCDCDC",
  NEWSBACKGROUND : "#F5F5F5",
  FACEBOOK : "#3b5998",
  TWITTER :  "#1DA1F2",
  WHATSAPP : "#128C7E",
  FONTSIZE29 : 29,
  FONTSIZE23 : 23,
  FONTSIZE18 : 18,
  FONTSIZE14 : 14,
  FONTSIZE16 : 16,
  FONTSIZE12 : 12,
  FONTFAMILY : 'WhitneyMedium',
  APIKEY:"AIzaSyAHsFgb1rfLEW575RHqz1qcpVDzwNV_g7s",

  SOCIAL : {
    IFFACEBOOKAPPAVAILABLE : "fb://facewebmodal/f?href=https://www.facebook.com/dudiindia",
    IFFACEBOOKAPPNOTAVAILABLE : "https://play.google.com/store/apps/details?id=com.facebook.katana",

    IFTWITTERAPPAVAILABLE : "twitter://user?screen_name=narenderdudi",
    IFTWITTERAPPNOTAVAILABLE : "https://play.google.com/store/apps/details?id=com.twitter.android",

    IFWHATSAPPAPPAVAILABLE : "whatsapp://send?text=Hello&phone=+918232877777",
    IFWHATSAPPAPPNOTAVAILABLE :  "https://play.google.com/store/apps/details?id=com.whatsapp",

    IFINSTAGRAMAPPAVAILABLE : "instagram://user?username=raman.dude",
    IFINSTAGRAMAPPNOTAVAILABLE :  "https://play.google.com/store/apps/details?id=com.instagram.android",

    IFNEXTMLAAPPAVAILABLE : "instagram://user?username=raman.dude",
    IFNEXTMLAAPPNOTAVAILABLE :  "https://play.google.com/store/apps/details?id=com.mdtpl.nextcm"
  },

  SHARE : {
    title : 'Candidate Profile',
    message : 'Candidate Profile',
    url : 'https://opopol.com',
    subject : 'Share Link'
  }
}
